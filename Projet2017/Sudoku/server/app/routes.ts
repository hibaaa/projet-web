import * as express from 'express';
import { SudokuGenerateur, Case } from './SudokuGenerateur';
import { MongoService } from './mongoService';

module Route {

  export class Index {

      mongoURL = "mongodb://Argon:Noble@ds157559.mlab.com:57559/projet_2_db";

    public home(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        console.log('hello world...');
    }

    public envoyerSudoku(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        console.log(Date());
        let sudoku : Case[][] = SudokuGenerateur.obtenirInstance().envoyerSudoku(req.params.estDifficile === "true");
        res.json({grille: sudoku, date: Date(), type: " DEMANDE SUDOKU ", description: req.connection.remoteAddress});
    }

    public genererSudoku(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        SudokuGenerateur.obtenirInstance().genererNouveauSudoku(req.params.estDifficile === "true");
        res.json({grille: undefined, date: Date(),
                  type: " GENERATION SUDOKU ",
                  description: req.connection.remoteAddress});
    }

    public validerSudoku(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let estDifficile : boolean = req.params.estDifficile === "true";
        let estValide_ : boolean = SudokuGenerateur.obtenirInstance().validerSudoku(req.body.grille);

        MongoService.obtenirInstance().ajouterMeilleurTemps(req.body.nomUtilisateur, req.body.temps, estDifficile);
        res.json({estValide : estValide_,
                  date: Date(), type: " VALIDER SUDOKU ",
                  description: req.connection.remoteAddress});
    }

    public validerUtilisateur(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let utilisateur = req.body.nomUtilisateur;
        MongoService.obtenirInstance().ajouterUtilisateur(utilisateur)
        .then(reponse =>
        {
            if (reponse !== null)
            {
                utilisateur = "";
            }
            res.json({nomUtilisateur : utilisateur,
                      date: Date(), type: " DEMANDE MONGODB ",
                      description: req.connection.remoteAddress});
        });
    }

    public envoyerMeilleursTemps(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let estDifficile : boolean = req.params.estDifficile === "true";

        MongoService.obtenirInstance().trouverMeilleurTemps(estDifficile)
        .then(reponse =>
        {
            let donnes : any[] = [];
            let compteur = 0;
            reponse.forEach(element =>
            {
                donnes[compteur] = { nomUtilisateur : element.nom, temps : element.temps };
                compteur++;
            });
            res.json({meilleursTemps : donnes,
                      date: Date(), type: " DEMANDE MONGODB ",
                      description: req.connection.remoteAddress});
        });
    }
  }
}

export = Route;
