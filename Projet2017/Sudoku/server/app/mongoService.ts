
import * as mongoose from 'mongoose';
import Utilisateur from './utilisateur';
import { IUtilisateur } from './utilisateur';
import MeilleurTemps from './meilleurTemps';
import { IMeilleurTemps } from './meilleurTemps';

export class MongoService
{
    private static instance : MongoService;

    static obtenirInstance()
    {
        if (!MongoService.instance) {
            MongoService.instance = new MongoService();
        }
        return MongoService.instance;
    }

    connecter()
    {
        mongoose.connect("mongodb://equipe18:nouvelleequipe@ds143330.mlab.com:43330/db_inf2990", () => {
        console.log('connected to Mongo...');
        });
    }

    ajouterUtilisateur(nomUtilisateur : string) : Promise<IUtilisateur>
    {
        return this.trouverUtilisateur(nomUtilisateur)
        .then(reponse =>
        {
            if (reponse === null)
            {
                let nouvelUtilisateur = new Utilisateur({nom : nomUtilisateur,
                                                         estDifficile : true });
                nouvelUtilisateur.save();
            }
            return reponse;
        });
    }

    trouverUtilisateur(nomUtilisateur : string) : Promise<IUtilisateur>
    {
        return Utilisateur.findOne({ 'nom': nomUtilisateur }, function(erreur, utilisateurs)
        {
            if (erreur)
            {
                return console.error(erreur);
            }
        }).exec();
    }

    ajouterMeilleurTemps(nomUtilisateur : string, temps_ : string, estDifficile_ : boolean) : Promise<IMeilleurTemps[]>
    {
        return this.trouverMeilleurTemps(estDifficile_)
        .then(reponse =>
        {
            let compteur = 0;
            reponse.forEach(element => {
                if (element.temps < temps_)
                {
                    compteur++;
                }
            });
            if (compteur < 3)
            {
                reponse.forEach(element =>
                {
                    if (element.position >= compteur)
                    {
                        element.position++;
                        element.save();
                    }
                    if (element.position === 3)
                    {
                        element.remove();
                    }
                });
                let nouveauMeilleurTemps = new MeilleurTemps(
                    {nom : nomUtilisateur, temps : temps_, position : compteur, estDifficile : estDifficile_});
                nouveauMeilleurTemps.save();
            }
            return reponse;
        });
    }

    trouverMeilleurTemps(estDifficile_ : boolean) : Promise<IMeilleurTemps[]>
    {
        return MeilleurTemps.find(function(erreur, meilleursTemps)
        {
            if (erreur)
            {
                return console.error(erreur);
            }
        }).where('estDifficile', estDifficile_).sort({position : 1}).exec();
    }
}
