
import * as mongoose from 'mongoose';

export interface IUtilisateur extends mongoose.Document
{
    nom: string;
}

let utilisateurSchema = new mongoose.Schema(
{
    nom : String
});

export default mongoose.model<IUtilisateur>("UtilisateurSudoku", utilisateurSchema);
