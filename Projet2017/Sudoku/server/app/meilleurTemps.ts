
import * as mongoose from 'mongoose';

export interface IMeilleurTemps extends mongoose.Document
{
    nom : string;
    temps : string;
    position : number;
    estDifficile : number;
}

let meilleurTempsSchema = new mongoose.Schema(
{
    nom : String,
    temps : String,
    position : Number,
    estDifficile : Boolean
});

export default mongoose.model<IMeilleurTemps>("MeilleurTemps", meilleurTempsSchema);
