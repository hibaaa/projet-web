import { SudokuGenerateur, Case } from './SudokuGenerateur';
import { expect } from 'chai';

const SUDOKU_TAILLE = 9;
const MIN_DIFFERENCES = 10;
const INDICES_VIDES = 8;
const CASES_VIDES_FACILE_MIN = 35;
const CASES_VIDES_FACILE_MAX = 40;
const CASES_VIDES_DIFFICILE_MIN = 50;
const CASES_VIDES_DIFFICILE_MAX = 55;

let sudoku : Case[][] = [
[{valeur : 6, ligne : 0, colonne : 0, estFixe : true}, {valeur : 3, ligne : 0, colonne : 1, estFixe : true},
{valeur : 2, ligne : 0, colonne : 2, estFixe : true}, {valeur : 7, ligne : 0, colonne : 3, estFixe : true},
{valeur : 8, ligne : 0, colonne : 4, estFixe : true}, {valeur : 1, ligne : 0, colonne : 5, estFixe : true},
{valeur : 9, ligne : 0, colonne : 6, estFixe : true}, {valeur : 4, ligne : 0, colonne : 7, estFixe : true},
{valeur : 5, ligne : 0, colonne : 8, estFixe : true}],
[{valeur : 4, ligne : 1, colonne : 0, estFixe : true}, {valeur : 8, ligne : 1, colonne : 1, estFixe : true},
{valeur : 7, ligne : 1, colonne : 2, estFixe : true}, {valeur : 5, ligne : 1, colonne : 3, estFixe : true},
{valeur : 9, ligne : 1, colonne : 4, estFixe : true}, {valeur : 6, ligne : 1, colonne : 5, estFixe : true},
{valeur : 2, ligne : 1, colonne : 6, estFixe : true}, {valeur : 1, ligne : 1, colonne : 7, estFixe : true},
{valeur : 3, ligne : 1, colonne : 8, estFixe : true}],
[{valeur : 5, ligne : 2, colonne : 0, estFixe : true}, {valeur : 1, ligne : 2, colonne : 1, estFixe : true},
{valeur : 9, ligne : 2, colonne : 2, estFixe : true}, {valeur : 2, ligne : 2, colonne : 3, estFixe : true},
{valeur : 4, ligne : 2, colonne : 4, estFixe : true}, {valeur : 3, ligne : 2, colonne : 5, estFixe : true},
{valeur : 8, ligne : 2, colonne : 6, estFixe : true}, {valeur : 7, ligne : 2, colonne : 7, estFixe : true},
{valeur : 6, ligne : 2, colonne : 8, estFixe : true}],
[{valeur : 8, ligne : 3, colonne : 0, estFixe : true}, {valeur : 6, ligne : 3, colonne : 1, estFixe : true},
{valeur : 4, ligne : 3, colonne : 2, estFixe : true}, {valeur : 3, ligne : 3, colonne : 3, estFixe : true},
{valeur : 5, ligne : 3, colonne : 4, estFixe : true}, {valeur : 2, ligne : 3, colonne : 5, estFixe : true},
{valeur : 7, ligne : 3, colonne : 6, estFixe : true}, {valeur : 9, ligne : 3, colonne : 7, estFixe : true},
{valeur : 1, ligne : 3, colonne : 8, estFixe : true}],
[{valeur : 7, ligne : 4, colonne : 0, estFixe : true}, {valeur : 5, ligne : 4, colonne : 1, estFixe : true},
{valeur : 1, ligne : 4, colonne : 2, estFixe : true}, {valeur : 9, ligne : 4, colonne : 3, estFixe : true},
{valeur : 6, ligne : 4, colonne : 4, estFixe : true}, {valeur : 8, ligne : 4, colonne : 5, estFixe : true},
{valeur : 3, ligne : 4, colonne : 6, estFixe : true}, {valeur : 2, ligne : 4, colonne : 7, estFixe : true},
{valeur : 4, ligne : 4, colonne : 8, estFixe : true}],
[{valeur : 2, ligne : 5, colonne : 0, estFixe : true}, {valeur : 9, ligne : 5, colonne : 1, estFixe : true},
{valeur : 3, ligne : 5, colonne : 2, estFixe : true}, {valeur : 1, ligne : 5, colonne : 3, estFixe : true},
{valeur : 7, ligne : 5, colonne : 4, estFixe : true}, {valeur : 4, ligne : 5, colonne : 5, estFixe : true},
{valeur : 6, ligne : 5, colonne : 6, estFixe : true}, {valeur : 5, ligne : 5, colonne : 7, estFixe : true},
{valeur : 8, ligne : 5, colonne : 8, estFixe : true}],
[{valeur : 9, ligne : 6, colonne : 0, estFixe : true}, {valeur : 4, ligne : 6, colonne : 1, estFixe : true},
{valeur : 5, ligne : 6, colonne : 2, estFixe : true}, {valeur : 6, ligne : 6, colonne : 3, estFixe : true},
{valeur : 3, ligne : 6, colonne : 4, estFixe : true}, {valeur : 7, ligne : 6, colonne : 5, estFixe : true},
{valeur : 1, ligne : 6, colonne : 6, estFixe : true}, {valeur : 8, ligne : 6, colonne : 7, estFixe : true},
{valeur : 2, ligne : 6, colonne : 8, estFixe : true}],
[{valeur : 1, ligne : 7, colonne : 0, estFixe : true}, {valeur : 7, ligne : 7, colonne : 1, estFixe : true},
{valeur : 6, ligne : 7, colonne : 2, estFixe : true}, {valeur : 8, ligne : 7, colonne : 3, estFixe : true},
{valeur : 2, ligne : 7, colonne : 4, estFixe : true}, {valeur : 5, ligne : 7, colonne : 5, estFixe : true},
{valeur : 4, ligne : 7, colonne : 6, estFixe : true}, {valeur : 3, ligne : 7, colonne : 7, estFixe : true},
{valeur : 9, ligne : 7, colonne : 8, estFixe : true}],
[{valeur : 3, ligne : 8, colonne : 0, estFixe : true}, {valeur : 2, ligne : 8, colonne : 1, estFixe : true},
{valeur : 8, ligne : 8, colonne : 2, estFixe : true}, {valeur : 4, ligne : 8, colonne : 3, estFixe : true},
{valeur : 1, ligne : 8, colonne : 4, estFixe : true}, {valeur : 9, ligne : 8, colonne : 5, estFixe : true},
{valeur : 5, ligne : 8, colonne : 6, estFixe : true}, {valeur : 6, ligne : 8, colonne : 7, estFixe : true},
{valeur : 7, ligne : 8, colonne : 8, estFixe : true}]];

let sudokuAvecVides : Case[][] = [
[{valeur : undefined, ligne : 0, colonne : 0, estFixe : false}, {valeur : 3, ligne : 0, colonne : 1, estFixe : true},
{valeur : 2, ligne : 0, colonne : 2, estFixe : true}, {valeur : 7, ligne : 0, colonne : 3, estFixe : true},
{valeur : 8, ligne : 0, colonne : 4, estFixe : true}, {valeur : 1, ligne : 0, colonne : 5, estFixe : true},
{valeur : 9, ligne : 0, colonne : 6, estFixe : true}, {valeur : 4, ligne : 0, colonne : 7, estFixe : true},
{valeur : 5, ligne : 0, colonne : 8, estFixe : true}],
[{valeur : 4, ligne : 1, colonne : 0, estFixe : true}, {valeur : 8, ligne : 1, colonne : 1, estFixe : true},
{valeur : undefined, ligne : 1, colonne : 2, estFixe : false}, {valeur : 5, ligne : 1, colonne : 3, estFixe : true},
{valeur : 9, ligne : 1, colonne : 4, estFixe : true}, {valeur : undefined, ligne : 1, colonne : 5, estFixe : false},
{valeur : 2, ligne : 1, colonne : 6, estFixe : true}, {valeur : 1, ligne : 1, colonne : 7, estFixe : true},
{valeur : 3, ligne : 1, colonne : 8, estFixe : true}],
[{valeur : 5, ligne : 2, colonne : 0, estFixe : true}, {valeur : 1, ligne : 2, colonne : 1, estFixe : true},
{valeur : 9, ligne : 2, colonne : 2, estFixe : true}, {valeur : 2, ligne : 2, colonne : 3, estFixe : true},
{valeur : 4, ligne : 2, colonne : 4, estFixe : true}, {valeur : 3, ligne : 2, colonne : 5, estFixe : true},
{valeur : 8, ligne : 2, colonne : 6, estFixe : true}, {valeur : 7, ligne : 2, colonne : 7, estFixe : true},
{valeur : 6, ligne : 2, colonne : 8, estFixe : true}],
[{valeur : 8, ligne : 3, colonne : 0, estFixe : true}, {valeur : undefined, ligne : 3, colonne : 1, estFixe : false},
{valeur : 4, ligne : 3, colonne : 2, estFixe : true}, {valeur : 3, ligne : 3, colonne : 3, estFixe : true},
{valeur : 5, ligne : 3, colonne : 4, estFixe : true}, {valeur : 2, ligne : 3, colonne : 5, estFixe : true},
{valeur : 7, ligne : 3, colonne : 6, estFixe : true}, {valeur : 9, ligne : 3, colonne : 7, estFixe : true},
{valeur : 1, ligne : 3, colonne : 8, estFixe : true}],
[{valeur : 7, ligne : 4, colonne : 0, estFixe : true}, {valeur : 5, ligne : 4, colonne : 1, estFixe : true},
{valeur : 1, ligne : 4, colonne : 2, estFixe : true}, {valeur : 9, ligne : 4, colonne : 3, estFixe : true},
{valeur : 6, ligne : 4, colonne : 4, estFixe : true}, {valeur : 8, ligne : 4, colonne : 5, estFixe : true},
{valeur : 3, ligne : 4, colonne : 6, estFixe : true}, {valeur : 2, ligne : 4, colonne : 7, estFixe : true},
{valeur : 4, ligne : 4, colonne : 8, estFixe : true}],
[{valeur : 2, ligne : 5, colonne : 0, estFixe : true}, {valeur : 9, ligne : 5, colonne : 1, estFixe : true},
{valeur : 3, ligne : 5, colonne : 2, estFixe : true}, {valeur : 1, ligne : 5, colonne : 3, estFixe : true},
{valeur : undefined, ligne : 5, colonne : 4, estFixe : false}, {valeur : 4, ligne : 5, colonne : 5, estFixe : true},
{valeur : 6, ligne : 5, colonne : 6, estFixe : true}, {valeur : 5, ligne : 5, colonne : 7, estFixe : true},
{valeur : 8, ligne : 5, colonne : 8, estFixe : true}],
[{valeur : 9, ligne : 6, colonne : 0, estFixe : true}, {valeur : 4, ligne : 6, colonne : 1, estFixe : true},
{valeur : 5, ligne : 6, colonne : 2, estFixe : true}, {valeur : undefined, ligne : 6, colonne : 3, estFixe : false},
{valeur : 3, ligne : 6, colonne : 4, estFixe : true}, {valeur : 7, ligne : 6, colonne : 5, estFixe : true},
{valeur : 1, ligne : 6, colonne : 6, estFixe : true}, {valeur : undefined, ligne : 6, colonne : 7, estFixe : false},
{valeur : 2, ligne : 6, colonne : 8, estFixe : true}],
[{valeur : 1, ligne : 7, colonne : 0, estFixe : true}, {valeur : 7, ligne : 7, colonne : 1, estFixe : true},
{valeur : 6, ligne : 7, colonne : 2, estFixe : true}, {valeur : 8, ligne : 7, colonne : 3, estFixe : true},
{valeur : 2, ligne : 7, colonne : 4, estFixe : true}, {valeur : 5, ligne : 7, colonne : 5, estFixe : true},
{valeur : 4, ligne : 7, colonne : 6, estFixe : true}, {valeur : 3, ligne : 7, colonne : 7, estFixe : true},
{valeur : 9, ligne : 7, colonne : 8, estFixe : true}],
[{valeur : undefined, ligne : 8, colonne : 0, estFixe : false}, {valeur : 2, ligne : 8, colonne : 1, estFixe : true},
{valeur : 8, ligne : 8, colonne : 2, estFixe : true}, {valeur : 4, ligne : 8, colonne : 3, estFixe : true},
{valeur : 1, ligne : 8, colonne : 4, estFixe : true}, {valeur : 9, ligne : 8, colonne : 5, estFixe : true},
{valeur : 5, ligne : 8, colonne : 6, estFixe : true}, {valeur : 6, ligne : 8, colonne : 7, estFixe : true},
{valeur : 7, ligne : 8, colonne : 8, estFixe : true}]];


let indicesVides : Case[] = [{valeur : undefined, ligne : 0, colonne : 0, estFixe : true},
{valeur : undefined, ligne : 1, colonne : 2, estFixe : true},
{valeur : undefined, ligne : 1, colonne : 5, estFixe : true},
{valeur : undefined, ligne : 3, colonne : 1, estFixe : true},
{valeur : undefined, ligne : 5, colonne : 4, estFixe : true},
{valeur : undefined, ligne : 6, colonne : 3, estFixe : true},
{valeur : undefined, ligne : 6, colonne : 7, estFixe : true},
{valeur : undefined, ligne : 8, colonne : 0, estFixe : true}];

describe("tests SudokuGenerateur", function()
{
    let sudokuGen : SudokuGenerateur;

    beforeEach(() => {
        sudokuGen = new SudokuGenerateur();
    });


    it("test construction", done =>
    {
        expect(sudokuGen).exist;
        done();
    });

    it("test échanger des colonnes", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.echangerColonne(sudokuAutre);

        let colA : number;
        let colB : number;
        let zone : number;
        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                {
                    if (colA === undefined || colA === j)
                    {
                        colA = j;
                        zone = SudokuGenerateur.determinerZone(i);
                    }
                    else if (zone === SudokuGenerateur.determinerZone(i) &&
                    (colB === undefined || colB === j))
                    {
                        colB = j;
                    }
                    else
                    {
                        resultat = false;
                    }
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test échanger des lignes", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.echangerLigne(sudokuAutre);

        let ligneA : number;
        let ligneB : number;
        let zone : number;
        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                {
                    if (ligneA === undefined || ligneA === i)
                    {
                        ligneA = i;
                        zone = SudokuGenerateur.determinerZone(i);
                    }
                    else if (zone === SudokuGenerateur.determinerZone(i) &&
                            (ligneB === undefined || ligneB === i))
                    {
                        ligneB = i;
                    }
                    else
                    {
                        resultat = false;
                   }
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test inversion colonne", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.inversionColonne(sudokuAutre);

        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[i][SUDOKU_TAILLE - j - 1].valeur)
                {
                    resultat = false;
                    break;
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test inversion ligne", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.inversionLigne(sudokuAutre);

        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[SUDOKU_TAILLE - i - 1][j].valeur)
                {
                    resultat = false;
                    break;
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test inversion diagonale identité (\\)", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.inversionDiagonalIdentite(sudokuAutre);

        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[j][i].valeur)
                {
                    resultat = false;
                    break;
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test inversion diagonale contre identité (/)", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.inversionDiagonalContreIdentite(sudokuAutre);

        let resultat = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                // chiffre sur la contre diagonal
                if (i + j === SUDOKU_TAILLE - 1)
                {
                    if (sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                    {
                        resultat = false;
                        break;
                    }
                }
                else if (sudoku[i][j].valeur !== sudokuAutre[SUDOKU_TAILLE - j - 1][SUDOKU_TAILLE - i - 1].valeur)
                {
                    resultat = false;
                    break;
                }
            }
        }

        expect(resultat).equal(true);
        done();
    });

    it("test generation grille", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.genererSudokuComplet(sudokuGen.copierSudoku(sudoku));
        let nbDifferences = 0;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                {
                    ++nbDifferences;
                }
            }
        }
        expect(nbDifferences).above(MIN_DIFFERENCES);
        done();
    });

    it("test copie profonde", done =>
    {
        let sudokuAutre : Case[][] = sudokuGen.copierSudoku(sudoku);
        sudokuGen.inversionDiagonalIdentite(sudokuAutre);

        let resultat = false;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                {
                    resultat = true;
                    break;
                }
            }
        }
        expect(resultat).equal(true);
        done();
    });

    it("test trouver prochaine case vide", done =>
    {
        let sudokuVide = sudokuGen.copierSudoku(sudokuAvecVides);
        sudokuGen.modifierSudokuAResoudre(sudokuVide);

        for (let i = 0; i < INDICES_VIDES; i++)
        {
            expect(sudokuGen.trouverProchaineCaseVide(sudokuGen.obtenirSudokuAResoudre(), true)[0]
            === indicesVides[i].ligne);
            expect(sudokuGen.trouverProchaineCaseVide(sudokuGen.obtenirSudokuAResoudre(), true)[1]
            === indicesVides[i].colonne);

            sudokuVide[indicesVides[i].ligne][indicesVides[i].colonne].valeur = 5;
            sudokuGen.modifierSudokuAResoudre(sudokuVide);
        }
        for (let i = 0; i < INDICES_VIDES; i++)
        {
            sudokuVide[indicesVides[i].ligne][indicesVides[i].colonne].valeur = undefined;
        }
        done();
    });

    it("test resolution sudoku", done =>
    {
        sudokuGen.modifierSudokuAResoudre(sudokuAvecVides);
        let temp : boolean = sudokuGen.resoudreSudoku(true, true);
        expect(temp).equal(true);

        //compter le nombre de cases vides
        let compteur = 0;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuAResoudre()[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.equal(0);

        done();
    });

    it("test resolution sudoku dans le sens inverse", done =>
    {
        sudokuGen.modifierSudokuAResoudre(sudokuAvecVides);
        let temp : boolean = sudokuGen.resoudreSudoku(false, true);
        expect(temp).equal(true);

        //compter le nombre de cases vides
        let compteur = 0;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuAResoudre()[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.equal(0);

        done();
    });

    it("test resolution sudoku avec possibilites descendant", done =>
    {
        sudokuGen.modifierSudokuAResoudre(sudokuAvecVides);
        let temp : boolean = sudokuGen.resoudreSudoku(true, false);
        expect(temp).equal(true);

        //compter le nombre de cases vides
        let compteur = 0;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuAResoudre()[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.equal(0);

        done();
    });

    it("test resolution sudoku dans le sens inverse avec possibilites descendant", done =>
    {
        sudokuGen.modifierSudokuAResoudre(sudokuAvecVides);
        let temp : boolean = sudokuGen.resoudreSudoku(true, false);
        expect(temp).equal(true);

        //compter le nombre de cases vides
        let compteur = 0;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuAResoudre()[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuAResoudre()[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.equal(0);

        done();
    });

    it("test validite solution", done =>
    {
        sudokuGen.modifierSudokuComplet(sudoku);

        expect(sudokuGen.verifierValiditeSolution(sudokuAvecVides, true, true)).to.equal(true);
        expect(sudokuGen.verifierValiditeSolution(sudokuAvecVides, true, false)).to.equal(true);
        expect(sudokuGen.verifierValiditeSolution(sudokuAvecVides, false, true)).to.equal(true);
        expect(sudokuGen.verifierValiditeSolution(sudokuAvecVides, false, false)).to.equal(true);

        done();
    });

    it("test resolution solution unique", done =>
    {
        sudokuGen.modifierSudokuComplet(sudoku);

        expect(sudokuGen.verifierSolutionUnique(sudokuAvecVides)).to.equal(true);

        done();
    });

    it("test comparer 2 sudoku", done =>
    {
        let identique : boolean = sudokuGen.comparer2Sudoku(sudoku, sudokuAvecVides);
        expect(identique).to.equal (false);
        identique = sudokuGen.comparer2Sudoku(sudokuAvecVides, sudokuAvecVides);
        expect (identique).to.equal(true);
        done();
    });

    it("test effacer prochaine case", done =>
    {
        let compteur = 0;
        sudokuGen.modifierSudokuComplet(sudoku);
        let sudoku2 = sudokuGen.copierSudoku(sudoku);
        for (let i = 0; i < INDICES_VIDES; i++)
        {
            sudoku2 = sudokuGen.effacerProchaineCase(sudoku2);
        }
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku2[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudoku2[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudoku2[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect (compteur).to.equal(INDICES_VIDES);
        done();
    });

    it("test generer indice aleatoire", done =>
    {
        let indiceAleatoire : number = sudokuGen.genererIndiceAleatoire();
        expect(indiceAleatoire).to.gte(0);
        expect(indiceAleatoire).to.lessThan(SUDOKU_TAILLE);
        done();
    });

    it("test generer nombre de cases a enlever aleatoire", done =>
    {
        let casesAEnleverFacile : number = sudokuGen.calculerNombreDeCasesAEnlever(false);
        expect(casesAEnleverFacile).to.gte(CASES_VIDES_FACILE_MIN);
        expect(casesAEnleverFacile).to.lte(CASES_VIDES_FACILE_MAX);

        let casesAEnleverDifficile : number = sudokuGen.calculerNombreDeCasesAEnlever(true);
        expect(casesAEnleverDifficile).to.gte(CASES_VIDES_DIFFICILE_MIN);
        expect(casesAEnleverDifficile).to.lte(CASES_VIDES_DIFFICILE_MAX);
        done();
    });

    it("test generer sudoku jouable", done =>
    {
        let compteur1 = 0;
        let compteur2 = 0;
        let sudoku1 = sudokuGen.genererSudokuJouable(false);
        let sudoku2 = sudokuGen.genererSudokuJouable(true);
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku1[i][j].valeur === undefined)
                {
                    compteur1++;
                    expect(sudoku1[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudoku1[i][j].estFixe).to.equal(true);
                }
                if (sudoku2[i][j].valeur === undefined)
                {
                    compteur2++;
                    expect(sudoku2[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudoku2[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur1).to.gte(CASES_VIDES_FACILE_MIN);
        expect(compteur1).to.lte(CASES_VIDES_FACILE_MAX);
        expect(compteur2).to.gte(CASES_VIDES_DIFFICILE_MIN);
        expect(compteur2).to.lte(CASES_VIDES_DIFFICILE_MAX);
        done();
    });

    it("test generer buffers sudokus", done =>
    {
        sudokuGen.initialiser();

        for (let index = 0; index < 3; index++)
        {
            let compteur = 0;

            for (let i = 0; i < SUDOKU_TAILLE; i++)
            {
                for (let j = 0; j < SUDOKU_TAILLE; j++)
                {
                    if (sudokuGen.obtenirSudokuFacile(index)[i][j].valeur === undefined)
                    {
                        compteur++;
                        expect(sudokuGen.obtenirSudokuFacile(index)[i][j].estFixe).to.equal(false);
                    }
                    else
                    {
                        expect(sudokuGen.obtenirSudokuFacile(index)[i][j].estFixe).to.equal(true);
                    }
                }
            }
            expect(compteur).to.gte(CASES_VIDES_FACILE_MIN);
            expect(compteur).to.lte(CASES_VIDES_FACILE_MAX);
        }
        for (let index = 0; index < 3; index++)
        {
            let compteur = 0;

            for (let i = 0; i < SUDOKU_TAILLE; i++)
            {
                for (let j = 0; j < SUDOKU_TAILLE; j++)
                {
                    if (sudokuGen.obtenirSudokuDifficile(index)[i][j].valeur === undefined)
                    {
                        compteur++;
                        expect(sudokuGen.obtenirSudokuDifficile(index)[i][j].estFixe).to.equal(false);
                    }
                    else
                    {
                        expect(sudokuGen.obtenirSudokuDifficile(index)[i][j].estFixe).to.equal(true);
                    }
                }
            }
            expect(compteur).to.gte(CASES_VIDES_DIFFICILE_MIN);
            expect(compteur).to.lte(CASES_VIDES_DIFFICILE_MAX);
        }
        done();
    });

    it("test generer nouveau sudoku facile", done =>
    {
        sudokuGen.initialiser();

        let sudokuFacile2 : Case[][] = sudokuGen.obtenirSudokuFacile(1);
        let sudokuFacile3 : Case[][] = sudokuGen.obtenirSudokuFacile(2);

        sudokuGen.genererNouveauSudoku(false);

        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuFacile(0), sudokuFacile2)).to.equal(true);
        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuFacile(1), sudokuFacile3)).to.equal(true);

        let compteur = 0;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuFacile(2)[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuFacile(2)[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuFacile(2)[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.gte(CASES_VIDES_FACILE_MIN);
        expect(compteur).to.lte(CASES_VIDES_FACILE_MAX);

        done();
    });

    it("test generer nouveau sudoku difficile", done =>
    {
        sudokuGen.initialiser();

        let sudokuDifficile2 : Case[][] = sudokuGen.obtenirSudokuDifficile(1);
        let sudokuDifficile3 : Case[][] = sudokuGen.obtenirSudokuDifficile(2);

        sudokuGen.genererNouveauSudoku(true);

        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuDifficile(0), sudokuDifficile2)).to.equal(true);
        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuDifficile(1), sudokuDifficile3)).to.equal(true);

        let compteur = 0;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuGen.obtenirSudokuDifficile(2)[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuGen.obtenirSudokuDifficile(2)[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuGen.obtenirSudokuDifficile(2)[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(compteur).to.gte(CASES_VIDES_DIFFICILE_MIN);
        expect(compteur).to.lte(CASES_VIDES_DIFFICILE_MAX);

        done();
    });

    it("test envoi sudoku facile", done =>
    {
        sudokuGen.initialiser();

        let sudokuFacile : Case[][] = sudokuGen.envoyerSudoku(false);

        let compteur = 0;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuFacile[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuFacile[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuFacile[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuJoue(), sudokuFacile)).to.equal(true);
        expect(compteur).to.gte(CASES_VIDES_FACILE_MIN);
        expect(compteur).to.lte(CASES_VIDES_FACILE_MAX);

        done();
    });

    it("test envoi sudoku difficile", done =>
    {
        sudokuGen.initialiser();

        let sudokuDifficile : Case[][] = sudokuGen.envoyerSudoku(true);

        let compteur = 0;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuDifficile[i][j].valeur === undefined)
                {
                    compteur++;
                    expect(sudokuDifficile[i][j].estFixe).to.equal(false);
                }
                else
                {
                    expect(sudokuDifficile[i][j].estFixe).to.equal(true);
                }
            }
        }
        expect(sudokuGen.comparer2Sudoku(sudokuGen.obtenirSudokuJoue(), sudokuDifficile)).to.equal(true);
        expect(compteur).to.gte(CASES_VIDES_DIFFICILE_MIN);
        expect(compteur).to.lte(CASES_VIDES_DIFFICILE_MAX);

        done();
    });

    it("test valider sudoku", done =>
    {
        sudokuGen.modifierSudokuJoue(sudokuAvecVides);
        expect(sudokuGen.validerSudoku(sudokuAvecVides)).to.equal(false);
        expect(sudokuGen.validerSudoku(sudoku)).to.equal(true);

        done();
    });

});
