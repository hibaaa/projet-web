
const SUDOKU_TAILLE = 9;
const SUDOKU_TAILLE_ZONE = 3;
const SUDOKU_TRANSORMATION_MAX = 20;
const SUDOKU_NB_TRANSFORMATIONS = 6;
const CASES_VIDES_FACILE_MIN = 35;
const CASES_VIDES_FACILE_MAX = 40;
const CASES_VIDES_DIFFICILE_MIN = 50;
const CASES_VIDES_DIFFICILE_MAX = 55;

export class Case
{
    valeur : number;
    ligne : number;
    colonne : number;
    estFixe : boolean;
}


export class SudokuGenerateur
{
    private static instance : SudokuGenerateur;

    private sudokuBase : Case[][] = [
[{valeur : 6, ligne : 0, colonne : 0, estFixe : true}, {valeur : 3, ligne : 0, colonne : 1, estFixe : true},
{valeur : 2, ligne : 0, colonne : 2, estFixe : true}, {valeur : 7, ligne : 0, colonne : 3, estFixe : true},
{valeur : 8, ligne : 0, colonne : 4, estFixe : true}, {valeur : 1, ligne : 0, colonne : 5, estFixe : true},
{valeur : 9, ligne : 0, colonne : 6, estFixe : true}, {valeur : 4, ligne : 0, colonne : 7, estFixe : true},
{valeur : 5, ligne : 0, colonne : 8, estFixe : true}],
[{valeur : 4, ligne : 1, colonne : 0, estFixe : true}, {valeur : 8, ligne : 1, colonne : 1, estFixe : true},
{valeur : 7, ligne : 1, colonne : 2, estFixe : true}, {valeur : 5, ligne : 1, colonne : 3, estFixe : true},
{valeur : 9, ligne : 1, colonne : 4, estFixe : true}, {valeur : 6, ligne : 1, colonne : 5, estFixe : true},
{valeur : 2, ligne : 1, colonne : 6, estFixe : true}, {valeur : 1, ligne : 1, colonne : 7, estFixe : true},
{valeur : 3, ligne : 1, colonne : 8, estFixe : true}],
[{valeur : 5, ligne : 2, colonne : 0, estFixe : true}, {valeur : 1, ligne : 2, colonne : 1, estFixe : true},
{valeur : 9, ligne : 2, colonne : 2, estFixe : true}, {valeur : 2, ligne : 2, colonne : 3, estFixe : true},
{valeur : 4, ligne : 2, colonne : 4, estFixe : true}, {valeur : 3, ligne : 2, colonne : 5, estFixe : true},
{valeur : 8, ligne : 2, colonne : 6, estFixe : true}, {valeur : 7, ligne : 2, colonne : 7, estFixe : true},
{valeur : 6, ligne : 2, colonne : 8, estFixe : true}],
[{valeur : 8, ligne : 3, colonne : 0, estFixe : true}, {valeur : 6, ligne : 3, colonne : 1, estFixe : true},
{valeur : 4, ligne : 3, colonne : 2, estFixe : true}, {valeur : 3, ligne : 3, colonne : 3, estFixe : true},
{valeur : 5, ligne : 3, colonne : 4, estFixe : true}, {valeur : 2, ligne : 3, colonne : 5, estFixe : true},
{valeur : 7, ligne : 3, colonne : 6, estFixe : true}, {valeur : 9, ligne : 3, colonne : 7, estFixe : true},
{valeur : 1, ligne : 3, colonne : 8, estFixe : true}],
[{valeur : 7, ligne : 4, colonne : 0, estFixe : true}, {valeur : 5, ligne : 4, colonne : 1, estFixe : true},
{valeur : 1, ligne : 4, colonne : 2, estFixe : true}, {valeur : 9, ligne : 4, colonne : 3, estFixe : true},
{valeur : 6, ligne : 4, colonne : 4, estFixe : true}, {valeur : 8, ligne : 4, colonne : 5, estFixe : true},
{valeur : 3, ligne : 4, colonne : 6, estFixe : true}, {valeur : 2, ligne : 4, colonne : 7, estFixe : true},
{valeur : 4, ligne : 4, colonne : 8, estFixe : true}],
[{valeur : 2, ligne : 5, colonne : 0, estFixe : true}, {valeur : 9, ligne : 5, colonne : 1, estFixe : true},
{valeur : 3, ligne : 5, colonne : 2, estFixe : true}, {valeur : 1, ligne : 5, colonne : 3, estFixe : true},
{valeur : 7, ligne : 5, colonne : 4, estFixe : true}, {valeur : 4, ligne : 5, colonne : 5, estFixe : true},
{valeur : 6, ligne : 5, colonne : 6, estFixe : true}, {valeur : 5, ligne : 5, colonne : 7, estFixe : true},
{valeur : 8, ligne : 5, colonne : 8, estFixe : true}],
[{valeur : 9, ligne : 6, colonne : 0, estFixe : true}, {valeur : 4, ligne : 6, colonne : 1, estFixe : true},
{valeur : 5, ligne : 6, colonne : 2, estFixe : true}, {valeur : 6, ligne : 6, colonne : 3, estFixe : true},
{valeur : 3, ligne : 6, colonne : 4, estFixe : true}, {valeur : 7, ligne : 6, colonne : 5, estFixe : true},
{valeur : 1, ligne : 6, colonne : 6, estFixe : true}, {valeur : 8, ligne : 6, colonne : 7, estFixe : true},
{valeur : 2, ligne : 6, colonne : 8, estFixe : true}],
[{valeur : 1, ligne : 7, colonne : 0, estFixe : true}, {valeur : 7, ligne : 7, colonne : 1, estFixe : true},
{valeur : 6, ligne : 7, colonne : 2, estFixe : true}, {valeur : 8, ligne : 7, colonne : 3, estFixe : true},
{valeur : 2, ligne : 7, colonne : 4, estFixe : true}, {valeur : 5, ligne : 7, colonne : 5, estFixe : true},
{valeur : 4, ligne : 7, colonne : 6, estFixe : true}, {valeur : 3, ligne : 7, colonne : 7, estFixe : true},
{valeur : 9, ligne : 7, colonne : 8, estFixe : true}],
[{valeur : 3, ligne : 8, colonne : 0, estFixe : true}, {valeur : 2, ligne : 8, colonne : 1, estFixe : true},
{valeur : 8, ligne : 8, colonne : 2, estFixe : true}, {valeur : 4, ligne : 8, colonne : 3, estFixe : true},
{valeur : 1, ligne : 8, colonne : 4, estFixe : true}, {valeur : 9, ligne : 8, colonne : 5, estFixe : true},
{valeur : 5, ligne : 8, colonne : 6, estFixe : true}, {valeur : 6, ligne : 8, colonne : 7, estFixe : true},
{valeur : 7, ligne : 8, colonne : 8, estFixe : true}]];

    private sudokuComplet : Case[][];
    private sudokuAResoudre : Case[][];
    private sudokuJoue : Case[][];
    private troisSudokuFaciles : Case[][][] = [ [[]], [[]], [[]]];
    private troisSudokuDifficiles : Case[][][] = [[[]], [[]], [[]]];

    static obtenirInstance()
    {
        if (!SudokuGenerateur.instance) {
            SudokuGenerateur.instance = new SudokuGenerateur();
            SudokuGenerateur.instance.initialiser();
        }
        return SudokuGenerateur.instance;
    }

    static determinerZone (index : number) : number
    {
        return Math.floor(index / SUDOKU_TAILLE_ZONE) * SUDOKU_TAILLE_ZONE;
    }

    initialiser() : void
    {
        this.troisSudokuFaciles[0] = this.genererSudokuJouable(false);
        this.troisSudokuFaciles[1] = this.genererSudokuJouable(false);
        this.troisSudokuFaciles[2] = this.genererSudokuJouable(false);
        this.troisSudokuDifficiles[0] = this.genererSudokuJouable(true);
        this.troisSudokuDifficiles[1] = this.genererSudokuJouable(true);
        this.troisSudokuDifficiles[2] = this.genererSudokuJouable(true);
    }

    obtenirSudokuJoue () : Case[][]
    {
        return this.sudokuJoue;
    }

    obtenirSudokuAResoudre () : Case[][]
    {
        return this.sudokuAResoudre;
    }

    obtenirSudokuBase () : Case[][]
    {
        return this.sudokuBase;
    }

    obtenirSudokuFacile (index : number) : Case[][]
    {
        return this.troisSudokuFaciles[index];
    }

    obtenirSudokuDifficile (index : number) : Case[][]
    {
        return this.troisSudokuDifficiles[index];
    }

    modifierSudokuAResoudre(sudoku : Case[][])
    {
        this.sudokuAResoudre = this.copierSudoku(sudoku);
    }

    modifierSudokuComplet(sudoku : Case[][])
    {
        this.sudokuComplet = this.copierSudoku(sudoku);
    }

    modifierSudokuJoue(sudoku : Case[][])
    {
        this.sudokuJoue = this.copierSudoku(sudoku);
    }

    echangerColonne (sudoku : Case[][]) : void
    {
        let temp : number;
        let colA : number = Math.floor(Math.random() * SUDOKU_TAILLE);
        let colB : number = Math.floor(Math.random() * SUDOKU_TAILLE);
        while (colB === colA || SudokuGenerateur.determinerZone(colA) !== SudokuGenerateur.determinerZone(colB))
        {
            colB = Math.floor(Math.random() * SUDOKU_TAILLE);
        }

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            temp = sudoku[i][colA].valeur;
            sudoku[i][colA].valeur = sudoku[i][colB].valeur;
            sudoku[i][colB].valeur = temp;
        }
    }

    echangerLigne (sudoku : Case[][]) : void
    {
        let ligneTemp : number;
        let ligneA : number = Math.floor(Math.random() * SUDOKU_TAILLE);
        let ligneB : number = Math.floor(Math.random() * SUDOKU_TAILLE);
        while (ligneA === ligneB || SudokuGenerateur.determinerZone(ligneA) !== SudokuGenerateur.determinerZone(ligneB))
        {
            ligneB = Math.floor(Math.random() * SUDOKU_TAILLE);
        }

        for (let j = 0; j < SUDOKU_TAILLE; j++)
        {
            ligneTemp = sudoku[ligneA][j].valeur;
            sudoku[ligneA][j].valeur = sudoku[ligneB][j].valeur;
            sudoku[ligneB][j].valeur = ligneTemp;
        }
    }

    inversionColonne (sudoku : Case[][]) : void
    {
        let temp : number;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            // besoin d'inverser seulement la moitié de la table, car on traite les éléments par paire
            for (let j = 0; j < Math.floor(SUDOKU_TAILLE / 2); j++)
            {
                temp = sudoku[i][j].valeur;
                sudoku[i][j].valeur = sudoku[i][SUDOKU_TAILLE - j - 1].valeur;
                sudoku[i][SUDOKU_TAILLE - j - 1].valeur = temp;
            }
        }
    }

    inversionLigne (sudoku : Case[][]) : void
    {
        let temp : number;

        // besoin d'inverser seulement la moitié de la table, car on traite les éléments par paire
        for (let i = 0; i < Math.floor(SUDOKU_TAILLE / 2); i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                temp = sudoku[i][j].valeur;
                sudoku[i][j].valeur = sudoku[SUDOKU_TAILLE - i - 1][j].valeur;
                sudoku[SUDOKU_TAILLE - i - 1][j].valeur = temp;
            }
        }
    }

    inversionDiagonalIdentite (sudoku : Case[][]) : void
    {
        let temp : number;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            // besoin d'inverser seulement le triangle inférieur du sudoku
            for (let j = 0; j < i; j++)
            {
                temp = sudoku[i][j].valeur;
                sudoku[i][j].valeur = sudoku[j][i].valeur;
                sudoku[j][i].valeur = temp;
            }
        }
    }

    inversionDiagonalContreIdentite (sudoku : Case[][]) : void
    {
        let temp : number;

        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            // besoin d'inverser seulement le triangle supérieur du sudoku
            for (let j = 0; j < SUDOKU_TAILLE - i - 1; j++)
            {
                temp = sudoku[i][j].valeur;
                sudoku[i][j].valeur = sudoku[SUDOKU_TAILLE - j - 1][SUDOKU_TAILLE - i - 1].valeur;
                sudoku[SUDOKU_TAILLE - j - 1][SUDOKU_TAILLE - i - 1].valeur = temp;
            }
        }
    }

    genererSudokuComplet (sudoku : Case[][]) : Case[][]
    {
        let operationSuivante : number;
        for (let i = 0; i < SUDOKU_TRANSORMATION_MAX; i++)
        {
            operationSuivante = Math.floor(Math.random() * SUDOKU_NB_TRANSFORMATIONS);
            switch (operationSuivante)
            {
                case 0:
                this.echangerColonne(sudoku);
                break;

                case 1:
                this.echangerLigne(sudoku);
                break;

                case 2:
                this.inversionColonne(sudoku);
                break;

                case 3:
                this.inversionLigne(sudoku);
                break;

                case 4:
                this.inversionDiagonalIdentite(sudoku);
                break;

                case 5:
                this.inversionDiagonalContreIdentite(sudoku);
                break;
            }
        }
        this.sudokuComplet = this.copierSudoku(sudoku);
        return this.copierSudoku(sudoku);
    }

    copierSudoku (sudokuACopier : Case[][]) : Case[][]
    {
        let copieSudoku : Case[][] = [];
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            copieSudoku[i] = [];
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                copieSudoku[i][j] = new Case();
                copieSudoku[i][j].valeur = sudokuACopier[i][j].valeur;
                copieSudoku[i][j].ligne = sudokuACopier[i][j].ligne;
                copieSudoku[i][j].colonne = sudokuACopier[i][j].colonne;
                copieSudoku[i][j].estFixe = sudokuACopier[i][j].estFixe;
            }
        }
        return copieSudoku;
    }

    trouverProchaineCaseVide(sudoku : Case[][], estAscendant : boolean) : number[]
    {
        let indices : number[];
        if (estAscendant)
        {
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8];
        }
        else
        {
            indices = [8, 7, 6, 5, 4, 3, 2, 1, 0];
        }
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku[indices[i]][indices[j]].valeur === undefined)
                {
                    return [indices[i], indices[j]];
                }
            }
        }
        return null;
    }

    estEntreeValideZone (chiffre : number, indices : number[]) : boolean
    {
        let zoneLigne : number = SudokuGenerateur.determinerZone(indices[0]);
        let zoneColonne : number = SudokuGenerateur.determinerZone(indices[1]);

        for (let i : number = zoneLigne; i < (zoneLigne + SUDOKU_TAILLE_ZONE); i++)
        {
            for (let j : number = zoneColonne; j < (zoneColonne + SUDOKU_TAILLE_ZONE); j++)
            {
                if (this.sudokuAResoudre[i][j].valeur === chiffre)
                {
                    return false;
                }
            }
        }
        return true;
    }

    estEntreeValideLigne (entree : number, indices : number[]) : boolean
    {
        for (let j = 0; j < SUDOKU_TAILLE; j++)
        {
            if (this.sudokuAResoudre[indices[0]][j].valeur === entree)
            {
                return false;
            }
        }
        return true;
    }

    estEntreeValideColonne (entree : number, indices : number[]) : boolean
    {
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            if (this.sudokuAResoudre[i][indices[1]].valeur === entree)
            {
                return false;
            }
        }
        return true;
    }

    emplacementEstPossible (entree : number, indices : number[]) : boolean
    {
        return entree > 0 &&
               this.estEntreeValideColonne(entree, indices) &&
               this.estEntreeValideLigne(entree, indices) &&
               this.estEntreeValideZone(entree, indices);
    }

    nombreEmplacementPossible (indices : number[], estAscendant : boolean) : number[]
    {
        let possibilites : number[] = [];
        let nombresPossibles : number[];
        if (estAscendant)
        {
            nombresPossibles = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        }
        else
        {
            nombresPossibles = [9, 8, 7, 6, 5, 4, 3, 2, 1];
        }
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            if (this.emplacementEstPossible(nombresPossibles[i], indices))
            {
                possibilites.push(nombresPossibles[i]);
            }
        }
        return possibilites;
    }

    resoudreSudoku(sensRegulier : boolean, estAscendant : boolean) : boolean
    {
        let indices : number[] = this.trouverProchaineCaseVide(this.sudokuAResoudre, sensRegulier);
        let possibilites : number[] = null;
        if (indices === null)
        {
                return true;
        }
        else
        {
            possibilites = this.nombreEmplacementPossible(indices, estAscendant);
            if (possibilites.length === 0)
            {
                return false;
            }
            else
            {
                for (let i = 0; i < possibilites.length; i++)
                {
                    this.sudokuAResoudre[indices[0]][indices[1]].valeur = possibilites[i];
                    if (this.resoudreSudoku(sensRegulier, estAscendant))
                    {
                        this.sudokuAResoudre[indices[0]][indices[1]].estFixe = true;
                        return true;
                    }
                    this.sudokuAResoudre[indices[0]][indices[1]].valeur = undefined;
                }
            }
        }
        return false;
    }

    verifierSolutionUnique (sudoku : Case[][]) : boolean
    {
        let valide = true;

        valide = valide && this.verifierValiditeSolution(sudoku, true, true);
        valide = valide && this.verifierValiditeSolution(sudoku, true, false);
        valide = valide && this.verifierValiditeSolution(sudoku, false, true);
        valide = valide && this.verifierValiditeSolution(sudoku, false, false);

        return valide;
    }

    verifierValiditeSolution (sudoku : Case[][], sensRegulier : boolean, estAscendant : boolean) : boolean
    {
        this.modifierSudokuAResoudre(this.copierSudoku(sudoku));
        let valide : boolean = this.resoudreSudoku(sensRegulier, estAscendant);
        return valide && this.comparer2Sudoku(this.sudokuAResoudre, this.sudokuComplet);
    }

    comparer2Sudoku(sudoku1 : Case[][], sudoku2 : Case[][]) : boolean
    {
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudoku1[i][j].valeur != sudoku2[i][j].valeur)
                {
                    return false;
                }
            }
        }
        return true;
    }

    effacerProchaineCase(sudoku : Case[][]) : Case[][]
    {
        let indiceLigne : number;
        let indiceColonne : number;
        let count = 10000;
        while (count > 0)
        {
            indiceLigne = this.genererIndiceAleatoire();
            indiceColonne = this.genererIndiceAleatoire();
            if (sudoku[indiceLigne][indiceColonne].valeur !== undefined)
            {
                sudoku[indiceLigne][indiceColonne].valeur = undefined;
                if (this.verifierSolutionUnique(sudoku))
                {
                    sudoku[indiceLigne][indiceColonne].estFixe = false;
                    break;
                }
                else
                {
                    sudoku[indiceLigne][indiceColonne].valeur =
                    this.sudokuComplet[indiceLigne][indiceColonne].valeur;
                }
            }
            count--;
        }
        return sudoku;
    }

    genererIndiceAleatoire() : number
    {
        return Math.floor(Math.random() * SUDOKU_TAILLE);
    }

    genererSudokuJouable(difficile : boolean) : Case[][]
    {
        let sudoku : Case[][] = this.genererSudokuComplet(this.sudokuBase);
        let casesAEffacer = this.calculerNombreDeCasesAEnlever(difficile);
        for (let i = 0; i < casesAEffacer; i++)
        {
            sudoku = this.effacerProchaineCase(sudoku);
        }
        return sudoku;
    }

    calculerNombreDeCasesAEnlever(difficile : boolean) : number
    {
        if (difficile)
        {
            return Math.floor(Math.random() * (CASES_VIDES_DIFFICILE_MAX - CASES_VIDES_DIFFICILE_MIN))
            + CASES_VIDES_DIFFICILE_MIN;
        }
        else
        {
            return Math.floor(Math.random() * (CASES_VIDES_FACILE_MAX - CASES_VIDES_FACILE_MIN))
            + CASES_VIDES_FACILE_MIN;
        }
    }

    genererNouveauSudoku (estDifficile : boolean) : void
    {
        if(estDifficile)
        {
            this.troisSudokuDifficiles[0] = this.copierSudoku(this.troisSudokuDifficiles[1]);
            this.troisSudokuDifficiles[1] = this.copierSudoku(this.troisSudokuDifficiles[2]);
            this.troisSudokuDifficiles[2] = this.genererSudokuJouable(true);
        }
        else
        {
            this.troisSudokuFaciles[0] = this.copierSudoku(this.troisSudokuFaciles[1]);
            this.troisSudokuFaciles[1] = this.copierSudoku(this.troisSudokuFaciles[2]);
            this.troisSudokuFaciles[2] = this.genererSudokuJouable(false);
        }
    }

    envoyerSudoku (estDifficile : boolean) : Case[][]
    {
        let sudoku : Case[][] = [];
        if(estDifficile)
        {
            sudoku = this.obtenirSudokuDifficile(0);
        }
        else
        {
            sudoku = this.obtenirSudokuFacile(0);   
        }
        this.sudokuJoue = this.copierSudoku(sudoku);
        return sudoku;
    }

    validerSudoku (sudoku : Case[][]) : boolean
    {
        this.modifierSudokuAResoudre(this.sudokuJoue);
        this.resoudreSudoku(true, true);
        return this.comparer2Sudoku(this.sudokuAResoudre, sudoku);
    }

}
