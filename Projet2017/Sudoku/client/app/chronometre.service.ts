import { Injectable } from '@angular/core';

const MAX_SECONDES = 59;
const MAX_MINUTES = 59;
const MAX_HEURES = 23;
const SECONDE_MS = 1000;

@Injectable()
export class ChronometreService
{
    public tempsSecondes : number;
    public tempsMinutes : number;
    public tempsHeures : number;
    public tempsJours : number;
    public temps : string;
    id : NodeJS.Timer;
    public estCommence : boolean;
    public estAffiche : boolean;
    public affichage : string;

    constructor()
    {
        this.tempsSecondes = 0;
        this.tempsMinutes = 0;
        this.tempsHeures = 0;
        this.tempsJours = 0;
        this.temps = "00:00:00:00";
        this.id = null;
        this.estCommence = false;
        this.estAffiche = true;
        this.affichage = "MASQUER";
    }

    public masquer() : void
    {
        this.estAffiche = false;
        this.rafraichirTemps();
        this.affichage = "AFFICHER";
    }

    public afficher() : void
    {
        this.estAffiche = true;
        this.rafraichirTemps();
        this.affichage = "MASQUER";
    }

    public obtenirMinutes() : number
    {
        return this.tempsMinutes;
    }

    public obtenirSecondes() : number
    {
        return this.tempsSecondes;
    }

    public obtenirHeures() : number
    {
        return this.tempsHeures;
    }

    public obtenirJours() : number
    {
        return this.tempsJours;
    }

    public rafraichirTemps() : void
    {
        this.temps = "";
        if (this.estAffiche)
        {
            if (this.tempsJours < 10)
            {
                this.temps += 0;
            }
            this.temps += this.tempsJours + ':';
            if (this.tempsHeures < 10)
            {
                this.temps += 0;
            }
            this.temps += this.tempsHeures + ':';
            if (this.tempsMinutes < 10)
            {
                this.temps += 0;
            }
            this.temps += this.tempsMinutes + ':';
            if (this.tempsSecondes < 10)
            {
                this.temps += 0;
            }
            this.temps += this.tempsSecondes;
        }
    }

    public commencerChronometre() : void
    {
        if (!this.estCommence)
        {
            this.id = setInterval(() => {
            if (this.tempsSecondes === MAX_SECONDES)
            {
                this.tempsSecondes = 0;
                if (this.tempsMinutes === MAX_MINUTES)
                {
                    this.tempsMinutes = 0;
                    if (this.tempsHeures === MAX_HEURES)
                    {
                        this.tempsHeures = 0;
                        this.tempsJours++;
                    }
                    else
                    {
                    this.tempsHeures++;
                    }
                }
                else
                {
                    this.tempsMinutes++;
                }
            }
            else
            {
                this.tempsSecondes++;
            }
            this.rafraichirTemps();
            }, SECONDE_MS);
            this.estCommence = true;
        }
    }

    public arreterChronometre() : void
    {
        clearInterval(this.id);
        this.id = null;
        this.estCommence = false;
    }

    public reinitialiserChronometre() : void
    {
        if (this.id != null || !this.estCommence)
        {
            this.arreterChronometre();
        }
        this.tempsSecondes = 0;
        this.tempsMinutes = 0;
        this.tempsHeures = 0;
        this.tempsJours = 0;
        this.temps = "00:00:00:00";
        this.commencerChronometre();
    }

    public reinitialiserChronometreArrete() : void
    {
        if (this.id != null || !this.estCommence)
        {
            this.arreterChronometre();
        }
        this.tempsSecondes = 0;
        this.tempsMinutes = 0;
        this.tempsHeures = 0;
        this.tempsJours = 0;
        this.temps = "00:00:00:00";
    }
}
