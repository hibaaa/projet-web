import { Component } from '@angular/core';

@Component({
  selector: 'interfaceJeu',
  templateUrl: 'app/interfaceJeu.component.html',
})

export class InterfaceJeuComponent
{
  public titre = ['S', 'U', 'D', 'O', 'K', 'U'];
}
