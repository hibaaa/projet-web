import { AppComponent } from './app.component';
import { Router } from '@angular/router';

import { expect } from 'chai';

describe('AppComponent', function () {
  let comp: AppComponent;
  let router: Router;

  beforeEach(() => {
    comp = new AppComponent(router);
  });

  it('test creation du component', done =>
  {
    expect(comp).to.not.be.undefined;
    done();
  });

});
