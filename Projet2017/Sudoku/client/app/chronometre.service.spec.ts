import { ChronometreService } from './chronometre.service';

import { expect } from 'chai';

const MAX_SECONDES = 59;
const MAX_MINUTES = 59;
const MAX_HEURES = 23;
const SECONDE_MS = 1000;

describe('test ChronometreService', function ()
{
  let chronometre : ChronometreService;

  beforeEach(() => {
    chronometre = new ChronometreService();
  });

  it('test demarrage du chronometre', done =>
  {
    chronometre.commencerChronometre();
    expect(chronometre.estCommence).to.equal(true);
    done();
  });

  it('test masquer chronometre', done =>
  {
    chronometre.masquer();
    expect(chronometre.estAffiche).to.equal(false);
    expect(chronometre.temps).to.equal("");
    expect(chronometre.affichage).to.equal("AFFICHER");
    done();
  });

  it('test afficher chronometre', done =>
  {
    chronometre.masquer();
    expect(chronometre.estAffiche).to.equal(false);
    expect(chronometre.temps).to.equal("");
    expect(chronometre.affichage).to.equal("AFFICHER");
    chronometre.afficher();
    expect(chronometre.estAffiche).to.equal(true);
    expect(chronometre.temps).to.not.equal("");
    expect(chronometre.affichage).to.equal("MASQUER");
    done();
  });

  it('test obtention secondes', done =>
  {
    expect(chronometre.obtenirSecondes()).to.equal(chronometre.tempsSecondes);
    done();
  });

  it('test obtention minutes', done =>
  {
    expect(chronometre.obtenirMinutes()).to.equal(chronometre.tempsMinutes);
    done();
  });

  it('test obtention heures', done =>
  {
    expect(chronometre.obtenirHeures()).to.equal(chronometre.tempsHeures);
    done();
  });

  it('test obtention jours', done =>
  {
    expect(chronometre.obtenirJours()).to.equal(chronometre.tempsJours);
    done();
  });

  it('test arret du chronometre', done =>
  {
    chronometre.arreterChronometre();
    expect(chronometre.estCommence).to.equal(false);
    expect(chronometre.id).to.equal(null);
    done();
  });

  it('test reinitialisation du chronometre et demarrage', done =>
  {
    chronometre.reinitialiserChronometre();
    expect(chronometre.id).to.not.equal(null);
    expect(chronometre.estCommence).to.equal(true);
    expect(chronometre.tempsJours).to.equal(0);
    expect(chronometre.tempsHeures).to.equal(0);
    expect(chronometre.tempsMinutes).to.equal(0);
    expect(chronometre.tempsSecondes).to.equal(0);
    expect(chronometre.temps).to.equal("00:00:00:00");
    done();
  });

  it('test reinitialisation du chronometre arrete', done =>
  {
    chronometre.reinitialiserChronometreArrete();
    expect(chronometre.id).to.equal(null);
    expect(chronometre.estCommence).to.equal(false);
    expect(chronometre.tempsJours).to.equal(0);
    expect(chronometre.tempsHeures).to.equal(0);
    expect(chronometre.tempsMinutes).to.equal(0);
    expect(chronometre.tempsSecondes).to.equal(0);
    expect(chronometre.temps).to.equal("00:00:00:00");
    done();
  });

  it('test multiplicite chronometre', done => {
    chronometre.commencerChronometre();
    let id : any = chronometre.id;
    chronometre.commencerChronometre();
    expect(chronometre.id).to.equal(id);
    done();
  });

  it('test mise a jour le temps complet', done => {
    chronometre.tempsHeures = 1;
    chronometre.tempsJours = 1;
    chronometre.tempsMinutes = 1;
    chronometre.tempsSecondes = 1;
    chronometre.rafraichirTemps();
    expect(chronometre.temps).to.equal('01:01:01:01');
    done();
  });

  it('test incrementer secondes', done =>
  {
    this.timeout(20000);
    chronometre.tempsSecondes = 0;
    chronometre.tempsMinutes = 0;
    chronometre.tempsHeures = 0;
    chronometre.tempsJours = 0;
    chronometre.commencerChronometre();
    setTimeout(() =>
    {
      expect(chronometre.tempsSecondes).to.equal(1);
      expect(chronometre.tempsMinutes).to.equal(0);
      expect(chronometre.tempsHeures).to.equal(0);
      expect(chronometre.tempsJours).to.equal(0);
      done();
    }, 1000);
  });

  it('test incrementer minutes', done =>
  {
    this.timeout(20000);
    chronometre.tempsSecondes = MAX_SECONDES;
    chronometre.tempsMinutes = 0;
    chronometre.tempsHeures = 0;
    chronometre.tempsJours = 0;
    chronometre.commencerChronometre();
    setTimeout(() =>
    {
      expect(chronometre.tempsSecondes).to.equal(0);
      expect(chronometre.tempsMinutes).to.equal(1);
      expect(chronometre.tempsHeures).to.equal(0);
      expect(chronometre.tempsJours).to.equal(0);
      done();
    }, SECONDE_MS);
  });

  it('test incrementer heures', done =>
  {
    this.timeout(20000);
    chronometre.tempsSecondes = MAX_SECONDES;
    chronometre.tempsMinutes = MAX_MINUTES;
    chronometre.tempsHeures = 0;
    chronometre.tempsJours = 0;
    chronometre.commencerChronometre();
    setTimeout(() =>
    {
      expect(chronometre.tempsSecondes).to.equal(0);
      expect(chronometre.tempsMinutes).to.equal(0);
      expect(chronometre.tempsHeures).to.equal(1);
      expect(chronometre.tempsJours).to.equal(0);
      done();
    }, SECONDE_MS);
  });

  it('test incrementer jours', done =>
  {
    this.timeout(20000);
    chronometre.tempsSecondes = MAX_SECONDES;
    chronometre.tempsMinutes = MAX_MINUTES;
    chronometre.tempsHeures = MAX_HEURES;
    chronometre.tempsJours = 0;
    chronometre.commencerChronometre();
    setTimeout(() =>
    {
      expect(chronometre.tempsSecondes).to.equal(0);
      expect(chronometre.tempsMinutes).to.equal(0);
      expect(chronometre.tempsHeures).to.equal(0);
      expect(chronometre.tempsJours).to.equal(1);
      done();
    }, SECONDE_MS);
  });

});
