import { Component } from '@angular/core';

import { ChronometreService } from './chronometre.service';

@Component({
    selector : 'mon-chronometre',
    templateUrl : 'app/chronometre.component.html',
    providers : [ ]
})

export class ChronometreComponent
{

    constructor ( private chronometre : ChronometreService)
    {
    }

    public alternerAffichage() : void
    {
        if (this.chronometre.estAffiche)
        {
            this.chronometre.masquer();
        }
        else
        {
            this.chronometre.afficher();
        }
    }
}
