
import { Component } from '@angular/core';
import { SudokuService } from './sudoku.service';
import { ChronometreService } from './chronometre.service';
import { PanneauService } from './panneauDeControle.service';
import { TableauBordService } from './tableauDeBord.service';


@Component({
  selector: 'panneau-controle',
  templateUrl: 'app/panneauDeControle.component.html'
})

export class PanneauComponent {
  afficher = false;
  message = "";
  constructor(private sudokuService : SudokuService,
              private chronometreService : ChronometreService,
              private panneauService : PanneauService,
              private tableauDeBordService : TableauBordService)
  {
  }

  public obtenirNouveauSudoku() : void
  {
    this.afficher = true;
    document.getElementById("inputJoueur").style.background = "transparent";
    if (!this.panneauService.obtenirEstConnecte())
    {
      this.message = "Veuillez choisir un nom!";
      return;
    }

    this.sudokuService.chargerSudoku(this.panneauService.obtenirDifficulte())
                    .then(
                      reponse => { this.tableauDeBordService.ajouterLigneRegistre(reponse);
                                   this.chronometreService.reinitialiserChronometre();
                              this.tableauDeBordService.decrementerNbSudoku(this.panneauService.obtenirDifficulte()); },
                      error => console.log(error));

    this.sudokuService.genererSudoku(this.panneauService.obtenirDifficulte())
                    .then(
                      reponse => { this.tableauDeBordService.ajouterLigneRegistre(reponse);
                             this.tableauDeBordService.incrementerNbSudoku(this.panneauService.obtenirDifficulte()); },
                      error => console.log(error));
    this.sudokuService.estFini = false;
  }

  public reinitialiserPartie() : void
  {
    if (this.sudokuService.sudoku !== undefined)
    {
      this.sudokuService.reinitialiserSudoku();
      this.chronometreService.reinitialiserChronometre();
    }
  }

  public validerJoueur(nomUtilisateur : string) : void
  {
    let regexp = new RegExp('^[a-z]+$', 'i');
    if (!this.panneauService.nomUtilisateurEstVide())
    {
      if (regexp.test(nomUtilisateur))
      {
        this.panneauService.validerUtilisateur()
                    .then(
                       reponse => this.tableauDeBordService.ajouterLigneRegistre(reponse),
                       error => console.log(error));
      }
      else
      {
        this.message = "Nom de joueur invalide";
      }
    }
    else
    {
      this.message = "Entrer un nom de joueur pour jouer!";
    }
  }

  public choisirDifficulte(estDifficile : boolean) : void
  {
    if (estDifficile === this.panneauService.obtenirDifficulte())
    {
      return;
    }
    this.sudokuService.sudoku = undefined;
    this.chronometreService.reinitialiserChronometreArrete();
    this.panneauService.modifierDifficulte(estDifficile);
    this.panneauService.obtenirMeilleurstemps(estDifficile)
                    .then(
                      reponse => this.tableauDeBordService.ajouterLigneRegistre(reponse),
                      error => console.log(error));
  }
}
