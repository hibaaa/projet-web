import { Component } from '@angular/core';
import { SudokuService, Case } from './sudoku.service';
import { ChronometreService } from './chronometre.service';
import { PanneauService } from './panneauDeControle.service';

@Component(
    {
    selector: 'mon-sudoku',
    templateUrl: 'app/sudoku.component.html',

    styles: [`
    input:focus {
        background-color: rgba(255, 255, 0, 0.6);
	}`]
})

export class SudokuComponent
{
    constructor(private sudokuService : SudokuService,
                private chronometreService : ChronometreService, private panneauService : PanneauService)
    {
    }

    public gererEntree (uneCase : Case, unEvenement : any) : void
    {
        if (isNaN(Number(unEvenement.key)) || unEvenement.key === ' ' || uneCase.estFixe ||
        !this.sudokuService.estEntreeValide(Number(unEvenement.key), uneCase))
        {
            unEvenement.preventDefault();

            if ((unEvenement.key === 'Delete' || unEvenement.key === 'Backspace') && !uneCase.estFixe)
            {
                uneCase.valeur = undefined;
            }

            this.sudokuService.gererDeplacement(uneCase, unEvenement);
        }
        else if (this.sudokuService.estDerniereCase(uneCase))
        {
            this.chronometreService.arreterChronometre();
            this.sudokuService.sudoku[uneCase.ligne][uneCase.colonne].valeur = unEvenement.key;
            this.sudokuService.validerSudokuComplet()
            .then(reponse => {
                  this.panneauService.obtenirMeilleurstemps(this.panneauService.obtenirDifficulte());
                  console.log("get"); });
        }
    }

    public determinerCouleur(i: number, j: number) : boolean
    {
        return (Math.floor(i / 3) + Math.floor(j / 3)) % 2 === 1;
    }
}
