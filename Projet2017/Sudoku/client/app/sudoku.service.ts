
const SUDOKU_TAILLE = 9;
const SUDOKU_TAILLE_ZONE = 3;

import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { PanneauService } from './panneauDeControle.service';
import { ChronometreService } from './chronometre.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

export class Case
{
    public valeur: number;
    public ligne: number;
    public colonne: number;
    public estFixe: boolean;
}

@Injectable()
export class SudokuService
{
    public sudoku : Case[][];
    private serveurUrl : string;
    public estFini : boolean;

    public static determinerZone (index : number) : number
    {
        // retourne l'index de début du sous grillage, soit(0,3,6)
        return Math.floor(index / SUDOKU_TAILLE_ZONE) * SUDOKU_TAILLE_ZONE;
    }

    public static calculerID (uneCase : Case, deltaLigne : number, deltaColonne : number) : string
    {
        return ((uneCase.ligne + deltaLigne + SUDOKU_TAILLE) % SUDOKU_TAILLE).toString() +
               ((uneCase.colonne + deltaColonne + SUDOKU_TAILLE) % SUDOKU_TAILLE).toString();
    }

    constructor(private http : Http,
                    private chronometreService : ChronometreService, private panneauService : PanneauService )
    {
        this.serveurUrl = 'http://localhost:3002/';
    }

    public validerSudokuComplet () : Promise<string>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serveurUrl + 'validerSudoku/' + (this.panneauService.obtenirDifficulte()),
                       { grille : this.sudoku,
                         nomUtilisateur : this.panneauService.obtenirNomUtilisateur(),
                         temps : this.chronometreService.temps }, options)
               .toPromise()
               .then(reponse =>
               {
                   this.estFini = reponse.json().estValide;
                   return this.extraireJsonSudoku(reponse);
               })
               .catch(this.gererErreur);
    }

    public chargerSudoku(estDifficile : boolean) : Promise<string>
    {
        return this.http.get(this.serveurUrl + 'envoyerSudoku/' + estDifficile)
               .toPromise()
               .then(reponse => this.extraireJsonSudoku(reponse))
               .catch(this.gererErreur);
    }

    public genererSudoku (estDifficile : boolean) : Promise<string>
    {
        return this.http.get(this.serveurUrl + 'genererSudoku/' + estDifficile)
               .toPromise()
               .then(reponse => this.extraireJsonSudoku(reponse))
               .catch(this.gererErreur);
    }

    public extraireJsonSudoku (res: Response) : string
    {
        let donnes = res.json();

        if (donnes.grille !== undefined)
        {
            this.sudoku = [];
            for (let ligne = 0; ligne < SUDOKU_TAILLE; ligne++)
            {
                this.sudoku[ligne] = [];
                for (let colonne = 0; colonne < SUDOKU_TAILLE; colonne++)
                {
                    this.sudoku[ligne][colonne] = donnes.grille[ligne][colonne];
                }
            }
        }
        return donnes.date + " -- " + donnes.type + " -- " + donnes.description;
    }

    // handleError du tutorial de angular renommé en français pour les normes du projet
    private gererErreur (erreur: Response | any)
    {
        console.log("ERROR");
        let errMsg: string;
        if (erreur instanceof Response)
        {
            const body = erreur.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${erreur.status} - ${erreur.statusText || ''} ${err}`;
        }
        else
        {
            errMsg = erreur.message ? erreur.message : erreur.toString();
        }
        console.log(errMsg);
        return Promise.reject(errMsg);
    }

    estDerniereCase (derniereCase : Case) : boolean
    {
        for (let ligne = 0; ligne < SUDOKU_TAILLE; ligne++)
        {
           for (let colonne = 0; colonne < SUDOKU_TAILLE; colonne++)
           {
                if (this.sudoku[ligne][colonne].valeur === undefined &&
                    (ligne !== derniereCase.ligne || colonne !== derniereCase.colonne))
                {
                    return false;
                }
           }
        }
        return true;
    }

    estEntreeValideZone (chiffre : number, uneCase : Case) : boolean
    {
        let zoneLigne : number = SudokuService.determinerZone(uneCase.ligne);
        let zoneColonne : number = SudokuService.determinerZone(uneCase.colonne);

        for (let ligne = zoneLigne; ligne < zoneLigne + SUDOKU_TAILLE_ZONE; ligne++)
        {
            for (let colonne = zoneColonne; colonne < zoneColonne + SUDOKU_TAILLE_ZONE; colonne++)
            {
                if (this.sudoku[ligne][colonne].valeur === chiffre)
                {
                    return false;
                }
            }
        }
        return true;
    }

    estEntreeValideLigne (entree : number, uneCase : Case) : boolean
    {
        for (let colonne = 0; colonne < SUDOKU_TAILLE; colonne++)
        {
            if (this.sudoku[uneCase.ligne][colonne].valeur === entree)
            {
                return false;
            }
        }
        return true;
    }

    estEntreeValideColonne (entree : number, uneCase : Case) : boolean
    {
        for (let ligne = 0; ligne < SUDOKU_TAILLE; ligne++)
        {
            if (this.sudoku[ligne][uneCase.colonne].valeur === entree)
            {
                return false;
            }
        }
        return true;
    }

    estEntreeValide (entree : number, uneCase : Case) : boolean
    {
        return entree > 0 &&
               this.estEntreeValideColonne(entree, uneCase) &&
               this.estEntreeValideLigne(entree, uneCase) &&
               this.estEntreeValideZone(entree, uneCase);
    }

    copierSudoku (sudokuACopier : Case[][]) : Case[][]
    {
        let copieSudoku : Case[][] = [];
        for (let ligne = 0; ligne < SUDOKU_TAILLE; ligne++)
        {
            copieSudoku[ligne] = [];
            for (let colonne = 0; colonne < SUDOKU_TAILLE; colonne++)
            {
                copieSudoku[ligne][colonne] = new Case();
                copieSudoku[ligne][colonne].valeur = sudokuACopier[ligne][colonne].valeur;
                copieSudoku[ligne][colonne].ligne = sudokuACopier[ligne][colonne].ligne;
                copieSudoku[ligne][colonne].colonne = sudokuACopier[ligne][colonne].colonne;
            }
        }
        return copieSudoku;
    }

    reinitialiserSudoku () : void
    {
        for (let ligne = 0; ligne < SUDOKU_TAILLE; ligne++)
        {
            for (let colonne = 0; colonne < SUDOKU_TAILLE; colonne++)
            {
                if (!this.sudoku[ligne][colonne].estFixe)
                {
                    this.sudoku[ligne][colonne].valeur = undefined;
                }
            }
        }
    }

    gererDeplacement(uneCase : Case, unEvenement : any) : void
    {
        let deltaLigne = 0;
        let deltaColonne = 0;

        if (unEvenement.key === 'ArrowLeft')
        {
            --deltaColonne;
        }

        if (unEvenement.key === 'ArrowRight')
        {
            ++deltaColonne;
        }

        if (unEvenement.key === 'ArrowUp')
        {
            --deltaLigne;
        }

        if (unEvenement.key === 'ArrowDown')
        {
            ++deltaLigne;
        }
        document.getElementById(SudokuService.calculerID(uneCase, deltaLigne, deltaColonne)).focus();
    }
}
