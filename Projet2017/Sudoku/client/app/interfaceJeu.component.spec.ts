import { InterfaceJeuComponent } from './interfaceJeu.component';

import { expect } from 'chai';

describe('test ChronometreComponent', function ()
{
  let interfaceJeu : InterfaceJeuComponent;

  beforeEach(() => {
    interfaceJeu = new InterfaceJeuComponent();
  });

  it('test creation du component', done =>
  {
    expect(interfaceJeu).to.not.be.undefined;
    done();
  });

});
