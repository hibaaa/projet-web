import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { PanneauComponent } from './panneauDeControle.component';
import { PanneauService } from './panneauDeControle.service';

import { TableauDeBordComponent } from './tableauDeBord.component';
import { TableauBordService } from './tableauDeBord.service';

import { ChronometreComponent } from './chronometre.component';
import { ChronometreService } from './chronometre.service';

import { SudokuComponent } from './sudoku.component';
import { SudokuService } from './sudoku.service';

import { InterfaceJeuComponent } from './interfaceJeu.component';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpModule, JsonpModule,
  RouterModule.forRoot([
    { path: '', component: InterfaceJeuComponent },
    { path: 'sudoku', component: InterfaceJeuComponent },
    { path: 'tableauDeBord', component: TableauDeBordComponent}
  ]) ],
  declarations: [ AppComponent, SudokuComponent, ChronometreComponent,
  PanneauComponent, TableauDeBordComponent, InterfaceJeuComponent],
  providers: [ SudokuService, ChronometreService, PanneauService, TableauBordService ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
