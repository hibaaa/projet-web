import { TableauBordService } from './tableauDeBord.service';

import { expect } from 'chai';

describe('test TableauDeBordService', function ()
{
  let tableauDeBord : TableauBordService;

  beforeEach(() => {
    tableauDeBord = new TableauBordService();
  });

  it('test creation du component', done =>
  {
    expect(tableauDeBord).to.not.be.undefined;
    done();
  });

  it('test ajouter des lignes ', done =>
  {
    tableauDeBord.ajouterLigneRegistre("ligne");
    expect("ligne").equal(tableauDeBord.obtenirRegistre()[0]);
    done();
  });

  it('test obtenir des sudokus ', done =>
  {
    expect(tableauDeBord.obtenirNbSudoku(false)).equal(3);
    done();
  });

  it('test incrementer nb de sudokus ', done =>
  {
    tableauDeBord.incrementerNbSudoku(false);
    expect(tableauDeBord.obtenirNbSudoku(false)).equal(4);
    done();
  });

  it('test decrementer nb de sudokus ', done =>
  {
    tableauDeBord.decrementerNbSudoku(false);
    expect(tableauDeBord.obtenirNbSudoku(false)).equal(2);
    done();
  });

});
