import { SudokuService, Case } from './sudoku.service';
import { Http, Response, ResponseOptions } from '@angular/http';
import { expect } from 'chai';
import { PanneauService } from './panneauDeControle.service';
import { ChronometreService } from './chronometre.service';

const SUDOKU_TAILLE = 9;

describe("tests SudokuService", function()
{
    let sudokuService : SudokuService;
    let http : Http;
    let sudokuComplet : Case[][];
    let panneauService : PanneauService;
    let chronometreService : ChronometreService;

     beforeEach(() => {
         sudokuService = new SudokuService(http, chronometreService, panneauService);
    sudokuComplet = [
[{valeur : 6, ligne : 0, colonne : 0, estFixe : false}, {valeur : 3, ligne : 0, colonne : 1, estFixe : false},
{valeur : 2, ligne : 0, colonne : 2, estFixe : false}, {valeur : 7, ligne : 0, colonne : 3, estFixe : false},
{valeur : 8, ligne : 0, colonne : 4, estFixe : false}, {valeur : 1, ligne : 0, colonne : 5, estFixe : false},
{valeur : 9, ligne : 0, colonne : 6, estFixe : false}, {valeur : 4, ligne : 0, colonne : 7, estFixe : false},
{valeur : 5, ligne : 0, colonne : 8, estFixe : false}],
[{valeur : 4, ligne : 1, colonne : 0, estFixe : false}, {valeur : 8, ligne : 1, colonne : 1, estFixe : false},
{valeur : 7, ligne : 1, colonne : 2, estFixe : false}, {valeur : 5, ligne : 1, colonne : 3, estFixe : false},
{valeur : 9, ligne : 1, colonne : 4, estFixe : false}, {valeur : 6, ligne : 1, colonne : 5, estFixe : false},
{valeur : 2, ligne : 1, colonne : 6, estFixe : false}, {valeur : 1, ligne : 1, colonne : 7, estFixe : false},
{valeur : 3, ligne : 1, colonne : 8, estFixe : false}],
[{valeur : 5, ligne : 2, colonne : 0, estFixe : false}, {valeur : 1, ligne : 2, colonne : 1, estFixe : false},
{valeur : 9, ligne : 2, colonne : 2, estFixe : false}, {valeur : 2, ligne : 2, colonne : 3, estFixe : false},
{valeur : 4, ligne : 2, colonne : 4, estFixe : false}, {valeur : 3, ligne : 2, colonne : 5, estFixe : false},
{valeur : 8, ligne : 2, colonne : 6, estFixe : false}, {valeur : 7, ligne : 2, colonne : 7, estFixe : false},
{valeur : 6, ligne : 2, colonne : 8, estFixe : false}],
[{valeur : 8, ligne : 3, colonne : 0, estFixe : false}, {valeur : 6, ligne : 3, colonne : 1, estFixe : false},
{valeur : 4, ligne : 3, colonne : 2, estFixe : false}, {valeur : 3, ligne : 3, colonne : 3, estFixe : false},
{valeur : 5, ligne : 3, colonne : 4, estFixe : false}, {valeur : 2, ligne : 3, colonne : 5, estFixe : false},
{valeur : 7, ligne : 3, colonne : 6, estFixe : false}, {valeur : 9, ligne : 3, colonne : 7, estFixe : false},
{valeur : 1, ligne : 3, colonne : 8, estFixe : false}],
[{valeur : 7, ligne : 4, colonne : 0, estFixe : false}, {valeur : 5, ligne : 4, colonne : 1, estFixe : false},
{valeur : 1, ligne : 4, colonne : 2, estFixe : false}, {valeur : 9, ligne : 4, colonne : 3, estFixe : false},
{valeur : 6, ligne : 4, colonne : 4, estFixe : false}, {valeur : 8, ligne : 4, colonne : 5, estFixe : false},
{valeur : 3, ligne : 4, colonne : 6, estFixe : false}, {valeur : 2, ligne : 4, colonne : 7, estFixe : false},
{valeur : 4, ligne : 4, colonne : 8, estFixe : false}],
[{valeur : 2, ligne : 5, colonne : 0, estFixe : false}, {valeur : 9, ligne : 5, colonne : 1, estFixe : false},
{valeur : 3, ligne : 5, colonne : 2, estFixe : false}, {valeur : 1, ligne : 5, colonne : 3, estFixe : false},
{valeur : 7, ligne : 5, colonne : 4, estFixe : false}, {valeur : 4, ligne : 5, colonne : 5, estFixe : false},
{valeur : 6, ligne : 5, colonne : 6, estFixe : false}, {valeur : 5, ligne : 5, colonne : 7, estFixe : false},
{valeur : 8, ligne : 5, colonne : 8, estFixe : false}],
[{valeur : 9, ligne : 6, colonne : 0, estFixe : false}, {valeur : 4, ligne : 6, colonne : 1, estFixe : false},
{valeur : 5, ligne : 6, colonne : 2, estFixe : false}, {valeur : 6, ligne : 6, colonne : 3, estFixe : false},
{valeur : 3, ligne : 6, colonne : 4, estFixe : false}, {valeur : 7, ligne : 6, colonne : 5, estFixe : false},
{valeur : 1, ligne : 6, colonne : 6, estFixe : false}, {valeur : 8, ligne : 6, colonne : 7, estFixe : false},
{valeur : 2, ligne : 6, colonne : 8, estFixe : false}],
[{valeur : 1, ligne : 7, colonne : 0, estFixe : false}, {valeur : 7, ligne : 7, colonne : 1, estFixe : false},
{valeur : 6, ligne : 7, colonne : 2, estFixe : false}, {valeur : 8, ligne : 7, colonne : 3, estFixe : false},
{valeur : 2, ligne : 7, colonne : 4, estFixe : false}, {valeur : 5, ligne : 7, colonne : 5, estFixe : false},
{valeur : 4, ligne : 7, colonne : 6, estFixe : false}, {valeur : 3, ligne : 7, colonne : 7, estFixe : false},
{valeur : 9, ligne : 7, colonne : 8, estFixe : false}],
[{valeur : 3, ligne : 8, colonne : 0, estFixe : false}, {valeur : 2, ligne : 8, colonne : 1, estFixe : false},
{valeur : 8, ligne : 8, colonne : 2, estFixe : false}, {valeur : 4, ligne : 8, colonne : 3, estFixe : false},
{valeur : 1, ligne : 7, colonne : 4, estFixe : false}, {valeur : 9, ligne : 8, colonne : 5, estFixe : false},
{valeur : 5, ligne : 8, colonne : 6, estFixe : false}, {valeur : 6, ligne : 8, colonne : 7, estFixe : false},
{valeur : 7, ligne : 8, colonne : 8, estFixe : false}]];

  });

    it("test construction", done =>
    {
        sudokuService.sudoku = sudokuComplet;

        expect(sudokuService).exist;
        done();
    });

    it("test determiner une zone", done =>
    {
        expect(SudokuService.determinerZone(0)).equal(0);
        expect(SudokuService.determinerZone(2)).equal(0);
        expect(SudokuService.determinerZone(3)).equal(3);
        expect(SudokuService.determinerZone(5)).equal(3);
        expect(SudokuService.determinerZone(7)).equal(6);
        done();
    });

    it("test calculer id", done =>
    {

        expect(SudokuService.calculerID({valeur : 5, ligne : 0, colonne : 0, estFixe : false}, 0, 1)).equal("01");
        expect(SudokuService.calculerID({valeur : 5, ligne : 0, colonne : 0, estFixe : false}, -1, 1)).equal("81");
        done();
    });

    it("test extraire JSON", done =>
    {
        sudokuService.sudoku = sudokuComplet;
        let sudokuAutre : Case[][] = sudokuService.copierSudoku(sudokuComplet);
        let reponse = new Response(new ResponseOptions(
            {body : {grille : undefined, date : "date", type : "type", description : "desc"}}));

        expect (sudokuService.extraireJsonSudoku(reponse)).equal("date -- type -- desc");

        let estidentique = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuService.sudoku[i][j].valeur !== sudokuAutre[i][j].valeur)
                {
                    estidentique = false;
                    break;
                }
            }
        }
        expect(estidentique).equal(true);

        sudokuAutre[0][0].valeur = (sudokuAutre[0][0].valeur + 1) % SUDOKU_TAILLE;
        reponse = new Response(new ResponseOptions(
            {body : {grille : sudokuAutre, date : "date1", type : "type1", description : "desc1"}}));

        expect (sudokuService.extraireJsonSudoku(reponse)).equal("date1 -- type1 -- desc1");
        expect (sudokuService.sudoku[0][0].valeur).equal(sudokuAutre[0][0].valeur);

        done();
    });

    it("test est la derniere case", done =>
    {
        let sudokuAutre : Case[][] = sudokuService.copierSudoku(sudokuComplet);
        sudokuAutre[0][0].valeur = undefined;
        sudokuService.sudoku = sudokuAutre;
        let uneCase : Case = {valeur : 6, ligne : 0, colonne : 0, estFixe : false};

        expect(sudokuService.estDerniereCase(uneCase)).equal(true);
        done();
    });

    it("test est une entrée valide dans la zone", done =>
    {
        sudokuService.sudoku = sudokuComplet;
        sudokuService.sudoku[0][0].valeur = undefined;

        expect(sudokuService.estEntreeValideZone(6, sudokuComplet[0][0])).equal(true);

        sudokuService.sudoku[0][0].valeur = 6;
        expect(sudokuService.estEntreeValideZone(6, sudokuComplet[0][0])).equal(false);
        expect(sudokuService.estEntreeValideZone(1, sudokuComplet[0][0])).equal(false);

        done();
    });

    it("test est une entrée valide dans la ligne", done =>
    {
        sudokuService.sudoku = sudokuComplet;
        sudokuService.sudoku[0][0].valeur = undefined;

        expect(sudokuService.estEntreeValideLigne(6, sudokuComplet[0][0])).equal(true);

        sudokuService.sudoku[0][0].valeur = 6;
        expect(sudokuService.estEntreeValideLigne(6, sudokuComplet[0][0])).equal(false);
        expect(sudokuService.estEntreeValideLigne(1, sudokuComplet[0][0])).equal(false);

        done();
    });

    it("test est une entrée valide dans la colonne", done =>
    {
        sudokuService.sudoku = sudokuComplet;

        sudokuService.sudoku[0][0].valeur = undefined;
        expect(sudokuService.estEntreeValideColonne(6, sudokuComplet[0][0])).equal(true);

        sudokuService.sudoku[0][0].valeur = 6;
        expect(sudokuService.estEntreeValideColonne(6, sudokuComplet[0][0])).equal(false);
        expect(sudokuService.estEntreeValideColonne(1, sudokuComplet[0][0])).equal(false);

        done();
    });

    it("test copie profonde", done =>
    {
        sudokuService.sudoku = sudokuComplet;
        let sudokuAutre : Case[][] = sudokuService.copierSudoku(sudokuComplet);

        sudokuAutre[0][0].valeur = undefined;
        let resultat : boolean = sudokuComplet[0][0].valeur !== undefined;

        expect(resultat).equal(true);
        done();
    });

    it("test réinitialiser", done =>
    {
        sudokuService.sudoku = sudokuService.copierSudoku(sudokuComplet);
        sudokuService.sudoku[0][0].estFixe = true;
        sudokuService.reinitialiserSudoku();

        expect(sudokuService.sudoku[0][0].valeur).not.undefined;

        let estValide = true;
        for (let i = 0; i < SUDOKU_TAILLE; i++)
        {
            for (let j = 0; j < SUDOKU_TAILLE; j++)
            {
                if (sudokuService.sudoku[i][j].valeur !== undefined)
                {
                    if (i !== 0 || j !== 0)
                    {
                        estValide = false;
                        break;
                    }
                }
            }
        }
        expect(estValide).equal(true);
        done();
    });
});
