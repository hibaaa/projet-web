import { SudokuComponent } from './sudoku.component';
import { SudokuService } from './sudoku.service';
import { ChronometreService } from './chronometre.service';
import { PanneauService } from './panneauDeControle.service';

import { expect } from 'chai';

describe('test SudokuComponent', function () {
  let sudokuComp : SudokuComponent;
  let sudokuService : SudokuService;
  let chrono : ChronometreService;
  let panneauService : PanneauService;

  beforeEach(() => {
    sudokuComp = new SudokuComponent(sudokuService, chrono, panneauService);
  });

  it('test creation du component', done =>
  {
    expect(sudokuComp).to.not.be.undefined;
    done();
  });

  it('test determiner couleur', done =>
  {
    expect(sudokuComp.determinerCouleur(0, 0)).equal(false);
    expect(sudokuComp.determinerCouleur(3, 3)).equal(false);
    expect(sudokuComp.determinerCouleur(0, 3)).equal(true);
    done();
  });

});

