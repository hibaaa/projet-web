
import { Injectable } from '@angular/core';

@Injectable()
export class TableauBordService
{
    private registreServeur : string[];
    private nbSudokuFacile : number;
    private nbSudokuDifficile : number;

    constructor()
    {
        this.registreServeur = Array<string>();
        this.nbSudokuDifficile = 3;
        this.nbSudokuFacile = 3;
    }

    public ajouterLigneRegistre(entree : string) : void
    {
        this.registreServeur.push(entree);
    }

    public obtenirRegistre() : string[]
    {
        return this.registreServeur;
    }

    public obtenirNbSudoku(estDifficile : boolean) : number
    {
        if (estDifficile)
        {
            return this.nbSudokuDifficile;
        }
        else
        {
            return this.nbSudokuFacile;
        }
    }

    public incrementerNbSudoku(estDifficile : boolean) : void
    {
        if (estDifficile)
        {
            ++this.nbSudokuDifficile;
        }
        else
        {
            ++this.nbSudokuFacile;
        }
    }

    public decrementerNbSudoku(estDifficile : boolean) : void
    {
        if (estDifficile)
        {
            --this.nbSudokuDifficile;
        }
        else
        {
            --this.nbSudokuFacile;
        }
    }

}
