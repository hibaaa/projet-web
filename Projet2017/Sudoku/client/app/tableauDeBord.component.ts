
import { Component } from '@angular/core';
import { TableauBordService } from './tableauDeBord.service';


@Component({
  selector: 'mon-tableauDeBord',
  templateUrl: 'app/tableauDeBord.component.html',
  providers : [ ]
})

export class TableauDeBordComponent
{
  public titre = ['T', 'A', 'B', 'L', 'E', 'A', 'U', ' ', 'D', 'E', ' ', 'B', 'O', 'R', 'D'];

  constructor(private tableauDeBordService : TableauBordService)
  {
  }
}
