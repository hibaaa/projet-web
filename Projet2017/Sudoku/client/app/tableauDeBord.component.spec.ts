
import { TableauDeBordComponent } from './tableauDeBord.component';
import { TableauBordService } from './tableauDeBord.service';

import { expect } from 'chai';

describe('test TableauDeBordComponent', function () {
  let tableauDeBordComponent : TableauDeBordComponent;
  let tableauDeBordService : TableauBordService;

  beforeEach(() => {
    tableauDeBordComponent = new TableauDeBordComponent(tableauDeBordService);
  });

  it('test creation du component', done =>
  {
    expect(tableauDeBordComponent).to.not.be.undefined;
    done();
  });
});
