import { PanneauComponent } from './panneauDeControle.component';
import { SudokuService } from './sudoku.service';
import { ChronometreService } from './chronometre.service';
import { PanneauService } from './panneauDeControle.service';
import { TableauBordService } from './tableauDeBord.service';

import { expect } from 'chai';

describe('test PanneauComponent', function () {
  let panneauDeControle : PanneauComponent;
  let sudoku : SudokuService;
  let chrono : ChronometreService;
  let panneauDeControleService : PanneauService;
  let tableauDeBordService : TableauBordService;

  beforeEach(() => {
    panneauDeControle = new PanneauComponent(sudoku, chrono, panneauDeControleService, tableauDeBordService);
  });

  it('test creation du component', done =>
  {
    expect(panneauDeControle).to.not.be.undefined;
    done();
  });

});
