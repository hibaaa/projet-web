
import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

export class MeilleurTemps
{
    public nomUtilisateur : string;
    public temps : string;
}

@Injectable()

export class PanneauService
{
    private difficulte : string;
    private estDifficile : boolean;
    private nomUtilisateur : string;
    private serveurUrl : string;
    private estConnecte : boolean;
    public meilleursTemps : MeilleurTemps[];

    constructor(private http : Http)
    {
        this.difficulte = "";
        this.estDifficile = undefined;
        this.nomUtilisateur = "";
        this.serveurUrl = 'http://localhost:3002/';
        this.estConnecte = false;
        this.meilleursTemps = undefined;
    }
    public obtenirNomUtilisateur() : string
    {
        return this.nomUtilisateur;
    }

    public obtenirEstConnecte() : boolean
    {
        return this.estConnecte;
    }

    public obtenirDifficulte() : boolean
    {
        return this.estDifficile;
    }

    public modifierDifficulte(estDifficile : boolean) : void
    {
        if (estDifficile)
        {
            this.difficulte = "Difficile";
        }
        else
        {
            this.difficulte = "Facile";
        }
        this.estDifficile = estDifficile;
    }

    public nomUtilisateurEstVide () : Boolean
    {
        return this.nomUtilisateur === "";
    }

    public validerUtilisateur () : Promise<string>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serveurUrl + 'validerUtilisateur',
        { nomUtilisateur : this.nomUtilisateur}, options)
               .toPromise()
               .then(reponse => this.gererUtilisateur(reponse))
               .catch(this.gererErreur);
    }

    public obtenirMeilleurstemps(estDifficile : boolean) : Promise<string>
    {
        return this.http.get(this.serveurUrl + 'meilleursTemps/' + estDifficile)
               .toPromise()
               .then(reponse => this.extraireJsonMeilleursTemps(reponse))
               .catch(this.gererErreur);
    }

    public gererUtilisateur (reponse : Response) : string
    {
        let donnes = reponse.json();
        this.nomUtilisateur = donnes.nomUtilisateur.toString();
        if (this.nomUtilisateur === "")
        {
            alert("login Fail");
            //document.getElementById("message").innerHTML = "Veuillez choisir un nom!";
        }
        else
        {
            this.estConnecte = true;
            alert("login success");
        }
        return donnes.date + " -- " + donnes.type + " -- " + donnes.description;
    }

    public extraireJsonMeilleursTemps (res: Response) : string
    {
        let donnes = res.json();
        this.meilleursTemps = donnes.meilleursTemps;
        return donnes.date + " -- " + donnes.type + " -- " + donnes.description;
    }

    // handleError du tutorial de angular renommé en français pour les normes du projet
    private gererErreur (erreur: Response | any) : Promise<string>
    {
        console.log("ERROR");
        let errMsg: string;
        if (erreur instanceof Response)
        {
            const body = erreur.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${erreur.status} - ${erreur.statusText || ''} ${err}`;
        }
        else
        {
            errMsg = erreur.message ? erreur.message : erreur.toString();
        }
        console.log(errMsg);
        return Promise.reject(errMsg);
    }
}
