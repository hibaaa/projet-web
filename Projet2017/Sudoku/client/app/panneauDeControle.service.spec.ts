import { PanneauService, MeilleurTemps } from './panneauDeControle.service';
import { Http, Response, ResponseOptions } from '@angular/http';
import { expect } from 'chai';

describe("tests PanneauService", function()
{
    let panneau : PanneauService;
    let http : Http;

    beforeEach(() => {
        panneau = new PanneauService(http);
    });

    it("test construction", done =>
    {
        expect(panneau).exist;
        done();
    });

    it("test modifier la difficulte", done =>
    {
        panneau.modifierDifficulte(true);
        expect(panneau.obtenirDifficulte()).equal(true);
        done();
    });

    it("test sont champs remplies", done =>
    {
        expect(panneau.nomUtilisateurEstVide()).equal(true);
        done();
    });

    it("test gérer Utilisateur", done =>
    {
        let reponse = new Response(new ResponseOptions(
            {body : { nomUtilisateur : "", date: "date", type: "MONGO", description: "ip client"}}));
        panneau.gererUtilisateur(reponse);
        expect(panneau.obtenirNomUtilisateur()).equal("");
        reponse = new Response(new ResponseOptions(
            {body : { nomUtilisateur : "bob", date: "date", type: "MONGO", description: "ip client"}}));
        expect(panneau.gererUtilisateur(reponse)).equal("date -- MONGO -- ip client");
        panneau.gererUtilisateur(reponse);
        expect(panneau.obtenirNomUtilisateur()).equal("bob");
        done();
    });

    it("test extraire json meilleur temps", done =>
    {
        let tableau: MeilleurTemps[] = [{nomUtilisateur : "McFly-kun", temps : "00:00:10:00"},
                                     {nomUtilisateur : "McFly-kun", temps : "00:00:10:00"},
                                     {nomUtilisateur : "McFly-kun", temps : "00:00:10:00"}];
        let reponse = new Response(new ResponseOptions
                        ({body : {meilleursTemps : tableau, date: "date", type: "MONGO", description: "ip client"}}));
        panneau.modifierDifficulte(false);
        expect(panneau.extraireJsonMeilleursTemps(reponse)).equal("date -- MONGO -- ip client");
        expect(panneau.meilleursTemps[0].nomUtilisateur).equal(tableau[0].nomUtilisateur);
        expect(panneau.meilleursTemps[0].temps).equal(tableau[0].temps);
        done();
    });
});
