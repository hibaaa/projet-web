import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as socketIo from 'socket.io-client';
import { PanneauService } from './panneauInformatif.service';
import { ChevaletService , Case } from './chevalet.service';
import { LoginService } from './login.service';
import { PlateauService } from './plateauJeu.service';
import { Lettre } from './lettre';
import { Message } from './message';
import { Router } from '@angular/router';

const MAX_SCRABBLE = 15;
const CENTRE = 7;
let SERVER_URL = 'http://localhost:3002';

@Injectable()
export class SocketService
{
    private socket : any;
    private idPartie: number;
    private partieLancee = false;
    private nomInvalide = false;
    private premierTour = false;

    constructor(private chevaletService : ChevaletService,
                private panneauService: PanneauService,
                private login: LoginService,
                private plateau : PlateauService,
                private router: Router)
    {
        this.initSocket();
    }

    private initSocket(): void
    {
        this.socket = socketIo(SERVER_URL);
        this.seConnecter();
    }

    public envoyerMessage(message: Message): void
    {
        this.socket.emit('message', {message, idPartie: this.idPartie});
    }

    public echangerLettre(lettres: string): boolean
    {
        let nomUtilisateur = this.login.obtenirNomUtilisateur();
        let echange = this.verifierMot(lettres);
        if (echange)
        {
            this.socket.emit('echangeDeLettres', { lettres , nomUtilisateur , idPartie: this.idPartie});
        }
        return echange;
    }

    public placerMot(mot: Array<string> , position : Array<number>) : boolean
    {
        let nomUtilisateur = this.login.obtenirNomUtilisateur();
        let caseCentraleOccupee = false;
        if (this.premierTour)
        {
             if ((mot[1] === "H" && position[0] === CENTRE && mot[0].length + position[1] > CENTRE)
             || (mot[1] === "V" && position[1] === CENTRE && mot[0].length + position[0] > CENTRE))
             {
                 caseCentraleOccupee = true;
             }
             this.premierTour = false;
        }
        else
        {
            caseCentraleOccupee = true;
        }

        let placer = this.verifierMotPlace(mot[1], mot[0], position);
        if (placer && caseCentraleOccupee)
        {
            this.socket.emit('placerMot', {mot : mot , position : position , nom : nomUtilisateur ,
            idPartie: this.idPartie});
        }
        return placer;
    }

    public finirTour(): void
    {
        this.socket.emit('TourFini', {id: this.idPartie , nom: this.login.obtenirNomUtilisateur()});
    }


    public validerUtilisateur(nomUtilisateur: string) : void
    {
        //RegEx pour verifier qu'un string contient seuleument des lettres
        let regexp = new RegExp('^[a-z]+$', 'i');
        if (regexp.test(nomUtilisateur))
        {
            this.login.modifierRechercheEnCours();
            this.socket.emit('validerUtilisateur', nomUtilisateur);
        }
        else
        {
            this.login.modifierNomJoueurInvalide();
            this.login.erreurAuthentification = "Nom de joueur invalide";
        }
    }


    public chercherPartie(): void
    {
        let nomUtilisateur = this.login.obtenirNomUtilisateur();
        let nbJoueurs = this.login.obtenirNombreJoueurs();
        this.socket.emit('chercherPartie', { nbJoueurs , nomUtilisateur });
    }

    private seConnecter() : void
    {
        this.socket.on('partietrouve', (id: number) => {
            this.idPartie = id;
        });

        this.socket.on('partieLancee', (data: any) => {
            this.partieLancee = true;
            this.panneauService.modifierTailleReserve(data.taille);
            this.panneauService.modifierNomUtilisateur(this.login.obtenirNomUtilisateur());
            this.socket.emit('InitialisationPartie', {nom: this.login.obtenirNomUtilisateur(),
                 id: this.idPartie});
            this.router.navigate(['/partieScrabble']);
            this.login.rechercheEnCours = false;
        });

        this.socket.on('ActualisationDuChevalet', (data: any) => {
            if (data.echange !== undefined && !data.echange)
            {
                alert("L'Echange de lettres n'a pu se faire"
                        + " car la reserve ne contient pas assez de lettres");
            }
            this.actualiserChevalet(data.nouvelleLettres);
        });

        this.socket.on('utilisateurExistant' , () => {
            this.login.modifierNomJoueurInvalide();
            this.login.modifierRechercheEnCours();
            this.login.erreurAuthentification = "Nom de joueur est déja pris!";
            this.nomInvalide = true;
        });

        this.socket.on('prochainTour' , () => {
            this.finirTour();
        });

        this.socket.on('utilisateurAjoute' , () => {
            this.chercherPartie();
        });

        this.socket.on('partieFini', (data: any) => {
            if (this.login.obtenirNomUtilisateur() === data.nom)
            {
                this.panneauService.modifierScore(data.score);
                this.actualiserChevalet(data.lettresFinale);
                this.panneauService.modifierTailleChevalet(data.tailleChevalet);
            }
            this.panneauService.terminerPartie(data.nomGagnant, data.scoreGagnant);
            this.panneauService.rendreInactif();
            this.actualiserGrille(data.grille);
        });

        this.socket.on('JoueurActif', (estActif: boolean) => {
            if (estActif)
            {
                this.premierTour = true;
                this.panneauService.rendreActif();
                alert("C'est ton tour!");
            }
        });

        this.socket.on('EstCeMonTour' , (data : any) => {
            this.panneauService.modifierTailleReserve(data.taille);
            if (this.login.obtenirNomUtilisateur() === data.nouveauActif)
            {
                this.panneauService.rendreActif();
                alert("C'est ton tour");
            }
            else if (this.login.obtenirNomUtilisateur() === data.ancienActif)
            {
                this.socket.emit('ActualisationChevalet', {nom: this.login.obtenirNomUtilisateur(),
                 id: this.idPartie});
                this.panneauService.modifierScore(data.score);
                this.panneauService.rendreInactif();
            }
            this.actualiserGrille(data.grille);
        });
    }

    public actualiserGrille(data: any) : void
    {
        let grille: Array<Array<Lettre>> = [];
        for ( let i = 0 ; i < MAX_SCRABBLE ; i++)
        {
            grille[i] = [];
            for (let j = 0; j < MAX_SCRABBLE; j++)
            {
                if (data[i][j].lettre.idLettre !== undefined)
                {
                    let lettre = data[i][j].lettre.idLettre;
                    let valeur = data[i][j].lettre.valeur;
                    grille[i][j] = new Lettre(lettre, valeur);
                }
                else
                {
                    grille[i][j] = new Lettre (undefined, undefined);
                }
            }
        }
        this.plateau.modifierGrille(grille);
    }

    public actualiserChevalet(data: any): void
    {
        let chevalet: Array<Case> = [];
        for (let i = 0 ; i < data.length ; i++)
        {
            let lettre = data[i].idLettre;
            let valeur = data[i].valeur;
            let nouvelleLettre = new Lettre(lettre, valeur);
            let nouvelleCase = new Case(nouvelleLettre, i);
            chevalet.push(nouvelleCase);
        }
        this.chevaletService.modifierTableLettres(chevalet);
    }

    public get()
    {
        let observable = new Observable((observer: any) => {
            this.socket.on('message', (data: any) => {
                console.log("SOCKET SERVICE: " + data.nom + " " + data.message);
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        });
        return observable;
    }

    public verifierMot(mot : string): boolean
    {
        let valide = true;
        for (let i = 0; i < mot.length; i++)
        {
            if (!this.chevaletService.trouverLettre(mot[i].toUpperCase()))
            {
                valide = false;
            }
        }
        return valide;
    }

    public verifierMotPlace(direction: string, mot : string , position: number[]): boolean
    {
        let valide = true;
        let nbreAsterisque = this.chevaletService.trouverNombreAsterisque();
        let grille = this.plateau.obtenirGrille();
        let debordementHorizontal = (direction === "H" && position[1] + mot.length > 15);
        let debordementVertical = (direction === "V" && position[0] + mot.length > 15);
        if (!debordementVertical && !debordementHorizontal)
        {
            for (let i = 0; i < mot.length; i++)
            {
                let lettreSurGrilleH = false;
                let lettreSurGrilleV = false;
                let lettreSurChevalet = this.chevaletService.trouverLettre(mot[i].toUpperCase());

                if (direction === "H" && grille[position[0]][position[1] + i].lettre !== undefined)
                {
                    lettreSurGrilleH =
                            this.plateau.obtenirGrille()[position[0]][position[1] + i].lettre === mot[i].toUpperCase();
                }
                else if (direction === "V" && grille[position[0] + i][position[1]].lettre !== undefined)
                {
                    lettreSurGrilleV =
                        this.plateau.obtenirGrille()[position[0] + i][position[1]].lettre === mot[i].toUpperCase();
                }
                if (!lettreSurChevalet && !lettreSurGrilleH && !lettreSurGrilleV && nbreAsterisque > 0)
                {
                    nbreAsterisque --;
                }
                else if (!lettreSurChevalet && !lettreSurGrilleH && !lettreSurGrilleV && nbreAsterisque === 0)
                {
                    valide = false;
                }
            }
        }
        else
        {
            valide = false;
        }
        return valide;
    }

    public partieEstLancee(): boolean
    {
        return this.partieLancee;
    }

    public estNomInvalide(): boolean
    {
        return this.nomInvalide;
    }
}
