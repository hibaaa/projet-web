import { Component, HostListener } from '@angular/core';
import { ChevaletService } from './chevalet.service';

@Component({
  selector: 'mon-chevalet',
  templateUrl: 'app/chevalet.component.html',
  providers: []
})

export class ChevaletComponent {

  @HostListener('document:keydown', ['$event'])

  quandTouchePressee(ev: KeyboardEvent)
  {
    if (document.activeElement.id !== "chatBox")
    {
      this.chevaletService.intervenirSurLettresChevalet(ev);
    }
  }

  constructor(private chevaletService: ChevaletService)
  {
  }
}
