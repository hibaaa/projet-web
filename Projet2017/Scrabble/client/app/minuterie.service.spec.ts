import { MinuterieService } from './minuterie.service';
import { expect } from 'chai';

describe('test MinuterieService', function()
{
    let minuterie : MinuterieService;

    beforeEach(() =>
    {
        minuterie = new MinuterieService();
    });

    it('test demarrage de la minuterie', done =>
    {
        minuterie.commencerMinuterie();
        expect(minuterie.estCommence).to.equal(true);
        done();
    });

    it('test arret de la minuterie', done =>
    {
        minuterie.arreterMinuterie();
        expect(minuterie.estCommence).to.equal(false);
        done();
    });

    it('test decrementer secondes et minutes', done => {
        this.timeout(20000);
        minuterie.commencerMinuterie();
        setTimeout(() =>
        {
            expect(minuterie.tempsMinutes).to.equal(4);
            expect(minuterie.tempsSecondes).to.equal(59);
            done();
        }, 1000);
    });
});
