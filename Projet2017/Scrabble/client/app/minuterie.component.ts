import { Component , EventEmitter , Output , OnInit } from '@angular/core';
import { MinuterieService } from './minuterie.service';

@Component({
  selector: 'mon-timer',
  templateUrl: 'app/minuterie.component.html',
  providers : []
})

export class MinuterieComponent implements OnInit
{
   @Output() minuterieComponent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor( private minuterie : MinuterieService)
  {
  }

  ngOnInit()
  {
    this.minuterie.relierEvenement(this.minuterieComponent);
  }
}
