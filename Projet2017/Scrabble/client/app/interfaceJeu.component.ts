import { Component, OnInit } from '@angular/core';
import { BoiteDeCommunicationService } from './boiteDeCommunication.service';
import { ChevaletService } from './chevalet.service';

@Component({
  selector: 'interface-jeu',
  templateUrl: 'app/interfaceJeu.component.html',
  providers: []
})

export class InterfaceJeuComponent implements OnInit
{

  constructor(private boiteCommunicationService: BoiteDeCommunicationService, private chevaletService: ChevaletService)
  {}

  ngOnInit()
  {
    document.getElementById("chatBox").focus();
  }
}
