import { expect } from 'chai';
import { LoginService } from './login.service';

describe('test LoginService', function()
{
    let login : LoginService;

    beforeEach(() =>
    {
        login = new LoginService();
    });

    it('Test service de login', done =>
    {
        expect(login).to.not.be.undefined;
        done();
    });
});
