import { LoginComponent } from './login.component';
import { expect } from 'chai';
import { LoginService } from './login.service';
import { SocketService } from './socket.service';
import { Router } from '@angular/router';

describe('LoginComponent', function () {
  let login : LoginComponent;
  let loginService : LoginService;
  let socketService : SocketService;
  let router: Router;

  beforeEach(() => {
    login = new LoginComponent(router, loginService, socketService);
  });

  it('test creation de boite de login', done =>
  {
    expect(login).to.not.be.undefined;
    done();
  });
});
