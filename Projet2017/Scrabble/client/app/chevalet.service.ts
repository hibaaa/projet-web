import { Injectable } from "@angular/core";
import { Lettre } from './lettre';

const DEBUT_ASCII = 96;
const FIN_ASCII = 123;
const MAX_CHEVALET = 7;
const ASTERISQUE = 42;

export class Case
{
    public lettre: Lettre;
    public indice: number;
    constructor(lettre_: Lettre, indice_: number)
    {
        this.lettre = lettre_;
        this.indice = indice_;
    }

    public obtenirLettre(): Lettre{
        return this.lettre;
    }
}

@Injectable()
export class ChevaletService
{
    public lettres: Array<Case>;
    private indiceCourantChevalet: number;
    private indicePrecedentChevalet: number;
    private trouve: boolean;
    private chevaletSelectionne: boolean;

    constructor()
    {
        this.lettres = new Array<Case>();
        this.indiceCourantChevalet = 0;
        this.indicePrecedentChevalet = 0;
        this.trouve = true;
        this.chevaletSelectionne = false;
    }

    public changerSelectionChevalet(): void
    {
        this.chevaletSelectionne = !this.chevaletSelectionne;
    }

    public obtenirChevaletSelectionne(): boolean
    {
        return this.chevaletSelectionne;
    }

    public obtenirChevalet(): Array<Case>
    {
        return this.lettres;
    }

    public obtenirIndiceCourant(): number
    {
        return this.indiceCourantChevalet;
    }

    public modifierTableLettres(lettres_: Array<Case>): void
    {
        this.lettres = [];
        lettres_.forEach(element => {
            this.lettres.push(element);
        });
    }

    public intervenirSurLettresChevalet(unEvenement : any): void
    {
        let entree : string = unEvenement.key;
        this.indicePrecedentChevalet = this.indiceCourantChevalet;
        if (this.lettres.length !== 0)
        {
            if (entree.charCodeAt(0) < FIN_ASCII && entree.charCodeAt(0) > DEBUT_ASCII ||
            entree.charCodeAt(0) === ASTERISQUE)
            {
                this.selectionnerLettresChevalet(entree);
            }
            else if (entree === 'ArrowRight' || entree === 'ArrowLeft')
            {
                this.echangerLettresChevalet(entree);
            }
            if (this.indiceCourantChevalet !== undefined && this.trouve)
            {
                let selectionPrecedente = document.getElementById(this.indicePrecedentChevalet.toString());
                selectionPrecedente.style.borderColor = "transparent";
                let selection = document.getElementById(this.indiceCourantChevalet.toString());
                selection.style.borderColor = "#FFE4E1";
            }
        }
    }

    public selectionnerLettresChevalet(entree : String): void
    {
        entree = entree.toUpperCase();
        this.trouve = false;
        for (let i = this.indiceCourantChevalet + 1; i < MAX_CHEVALET; i++)
        {
            if (this.lettres[i].lettre.lettre === entree && !this.trouve)
            {
                this.trouve = true;
                this.indiceCourantChevalet = i;
            }
        }

        if (!this.trouve)
        {
            for (let i = 0; i < this.indiceCourantChevalet; i++)
            {
                if (this.lettres[i].lettre.lettre === entree && !this.trouve)
                {
                    this.trouve = true;
                    this.indiceCourantChevalet = i;
                }
            }
            if (!this.trouve)
            {
                let selectionPrecedente = document.getElementById(this.indicePrecedentChevalet.toString());
                selectionPrecedente.style.borderColor = "transparent";
                let selection = document.getElementById(this.indiceCourantChevalet.toString());
                selection.style.borderColor = "transparent";
                this.indiceCourantChevalet = 0;
            }
        }
    }

    public identifierCases(): void
    {
        for (let i = 0; i < this.lettres.length; i++)
        {
            this.lettres[i].indice = i;
        }
    }

    public echangerLettresChevalet (entree : String): void
    {
        let indice: number = this.indiceCourantChevalet;
        if (entree === 'ArrowRight')
        {
            let indiceCaseDroite = indice + 1;
            if (indiceCaseDroite >= MAX_CHEVALET)
            {
                this.retourExtremiteDroite();
                this.indiceCourantChevalet = 0;
            }

            else
            {
            let temp: Lettre = this.lettres[indice].lettre;
            this.lettres[indice].lettre = this.lettres[indiceCaseDroite].lettre;
            this.lettres[indiceCaseDroite].lettre = temp;
            this.indiceCourantChevalet = indiceCaseDroite;
            }
        }

        else
        {
            let indiceCaseGauche = indice - 1;
            if (indiceCaseGauche < 0)
            {
                this.retourExtremiteGauche();
                this.indiceCourantChevalet = MAX_CHEVALET - 1;
            }

            else
            {
            let temp: Lettre = this.lettres[indice].lettre;
            this.lettres[indice].lettre = this.lettres[indiceCaseGauche].lettre;
            this.lettres[indiceCaseGauche].lettre = temp;
            this.indiceCourantChevalet = indiceCaseGauche;
            }
        }
        this.trouve = true;
    }

    public trouverNombreAsterisque(): number
    {
        let asterisque = 0;
        this.lettres.forEach(element => {
            if ( element.obtenirLettre().obtenirLettre() === "*"){
                asterisque ++;
            }
        });
        return asterisque;
    }

    public trouverLettre(lettre: string) : boolean
    {
        let estPresent = false;
        this.lettres.forEach(element => {
            if ( element.obtenirLettre().obtenirLettre() === lettre){
                estPresent = true;
            }
        });

        return estPresent;
    }

    public retourExtremiteDroite(): void
    {
        let temp: Lettre = this.lettres[MAX_CHEVALET - 1].lettre;
        for (let i = MAX_CHEVALET - 2; i >= 0; i--)
        {
            this.lettres[i + 1].lettre = this.lettres[i].lettre;
        }
        this.lettres[0].lettre = temp;
    }

    public retourExtremiteGauche(): void
    {
        let temp: Lettre = this.lettres[0].lettre;
        for (let i = 1; i < MAX_CHEVALET; i++)
        {
            this.lettres[i - 1].lettre = this.lettres[i].lettre;
        }
        this.lettres[MAX_CHEVALET - 1].lettre = temp;
    }

}
