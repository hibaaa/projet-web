import { Component } from '@angular/core';
import { PlateauService } from './plateauJeu.service';

const CENTRE = 7;

@Component({
  selector: 'plateau-jeu',
  templateUrl: 'app/plateauJeu.component.html'
})

export class PlateauJeuComponent {
  indices = ['', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];
  constructor(private plateauService : PlateauService){}

  public obtenirTypeCase(ligne: number, colonne: number): string
  {
    if (this.plateauService.verifierCaseMotTriple(ligne, colonne))
    {
      return "motX3";
    }
    else if (this.plateauService.verifierCaseMotDouble(ligne, colonne))
    {
      return "motX2";
    }
    else if (this.plateauService.verifierCaseLettreTriple(ligne, colonne))
    {
      return "lettreX3";
    }
    else if (this.plateauService.verifierCaseLettreDouble(ligne, colonne))
    {
      return "lettreX2";
    }
    else if (ligne === CENTRE && colonne === CENTRE)
    {
      return "centre";
    }
    else
    {
      return "";
    }
  }
}
