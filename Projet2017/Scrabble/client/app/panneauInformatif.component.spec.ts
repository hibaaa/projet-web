import { PanneauInformatifComponent } from './panneauInformatif.component';
import { PanneauService } from './panneauInformatif.service';
import { SocketService } from './socket.service';
import { expect } from 'chai';
import { LoginService } from './login.service';

describe('PanneauInformatifComponent', function () {
  let panneauInformatif: PanneauInformatifComponent;
  let panneauService: PanneauService;
  let socketService: SocketService;
  let loginService: LoginService;


  beforeEach(() => {
    panneauInformatif = new PanneauInformatifComponent(panneauService, socketService, loginService);
  });

  it('test creation de panneau informatif', done =>
  {
    expect(panneauInformatif).to.not.be.undefined;
    done();
  });
});
