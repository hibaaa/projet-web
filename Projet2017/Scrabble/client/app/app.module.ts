import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { PanneauInformatifComponent } from './panneauInformatif.component';
import { PanneauService } from './panneauInformatif.service';
import { ChevaletComponent } from './chevalet.component';
import { BoiteDeCommunicationComponent } from './boiteDeCommunication.component';
import { MinuterieComponent } from './minuterie.component';
import { MinuterieService } from './minuterie.service';
import { SocketService } from './socket.service';
import { ChevaletService } from './chevalet.service';
import { BoiteDeCommunicationService } from './boiteDeCommunication.service';
import { PlateauService } from './plateauJeu.service';
import { PlateauJeuComponent } from './plateauJeu.component';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { InterfaceJeuComponent } from './interfaceJeu.component';
import { CanActivateGuard } from './loginGuard';

@NgModule({
  imports: [ BrowserModule ,
             HttpModule ,
             FormsModule ,
             JsonpModule,
             RouterModule,
             AppRoutingModule],
  declarations: [ AppComponent,
                  PanneauInformatifComponent,
                  ChevaletComponent,
                  MinuterieComponent,
                  BoiteDeCommunicationComponent,
                  PlateauJeuComponent,
                  LoginComponent,
                  InterfaceJeuComponent],
  providers: [ PanneauService,
               MinuterieService,
               SocketService,
               ChevaletService,
               BoiteDeCommunicationService,
               LoginService,
               PlateauService,
               CanActivateGuard],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
