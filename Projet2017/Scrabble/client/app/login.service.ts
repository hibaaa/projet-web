import { Injectable } from "@angular/core";

@Injectable()
export class LoginService{

    private nomUtilisateur : string;
    private nbJoueurs : number;
    public nomJoueurInvalide = false;
    public rechercheEnCours = false;
    public erreurAuthentification: string;

    constructor()
    {
        this.nomUtilisateur = "";
        this.nbJoueurs = 2;
    }

    public reinitiliaserNomUtilisateur(): void
    {
        this.nomUtilisateur = "";
    }

    public obtenirNomUtilisateur(): string
    {
        return this.nomUtilisateur;
    }

    public obtenirNombreJoueurs(): number
    {
        return this.nbJoueurs;
    }

    public nomUtilisateurEstVide(): boolean
    {
        return (this.nomUtilisateur === "");
    }

    public modifierNbJoueurs(nbJoueurs_ : any): void
    {
        this.nbJoueurs = nbJoueurs_;
    }

    public modifierNomJoueurInvalide(): void
    {
        this.nomJoueurInvalide = !this.nomJoueurInvalide;
    }

    public modifierRechercheEnCours(): void
    {
        this.rechercheEnCours = !this.rechercheEnCours;
    }
}
