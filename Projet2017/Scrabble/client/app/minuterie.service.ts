import { Injectable , EventEmitter } from "@angular/core";

const MAX_SECONDES = 59;
const MAX_MINUTES = 5;

@Injectable()
export class MinuterieService
{

    public temps : string;
    public tempsSecondes : number;
    public tempsMinutes : number;
    id : any;
    public estCommence : boolean;
    public minuterieFini: EventEmitter<boolean>;

    constructor()
    {
        this.tempsMinutes = MAX_MINUTES;
        this.tempsSecondes = 0;
        this.temps = "05:00";
        this.id = null;
        this.estCommence = false;
    }

    public relierEvenement(evenement : EventEmitter<boolean>) : void
    {
        this.minuterieFini = evenement;
    }


    public rafraichirMinuterie() : void
    {
        this.temps = "";
        this.temps += 0 + this.tempsMinutes + ':';
        if (this.tempsSecondes < 10)
        {
            this.temps += 0;
        }
        this.temps += this.tempsSecondes;
        if (this.temps === "0:00")
        {
            this.minuterieFini.emit(true);
        }
    }

    public commencerMinuterie() : void
    {
        if (!this.estCommence)
        {
            this.id = setInterval( () =>
            {
                if (this.tempsMinutes !== 0 && this.tempsSecondes === 0)
                {
                    this.tempsMinutes --;
                    this.tempsSecondes = MAX_SECONDES;
                }
                else if (this.tempsSecondes !== 0)
                {
                    this.tempsSecondes--;
                }
                else if (this.tempsMinutes === 0 && this.tempsSecondes === 0)
                {
                    this.arreterMinuterie();
                }
                this.rafraichirMinuterie();
            }, 1000);
            this.estCommence = true;
        }
    }

    public arreterMinuterie(): void
    {
        clearInterval(this.id);
        this.id = null;
        this.estCommence = false;
    }

    public reinitialiserMinuterie(): void
    {
        this.arreterMinuterie();
        this.tempsSecondes = 0;
        this.tempsMinutes = 5;
        this.temps = "05:00";
    }
}
