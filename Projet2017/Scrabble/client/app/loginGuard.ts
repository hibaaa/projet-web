import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { SocketService } from './socket.service';

@Injectable()
export class CanActivateGuard implements CanActivate
{
    constructor (private socket: SocketService) {}

    canActivate()
    {
        return this.socket.partieEstLancee();
    }
}
