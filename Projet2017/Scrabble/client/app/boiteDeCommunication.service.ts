import { Injectable } from "@angular/core";
import { Message } from './message';
import { SocketService } from './socket.service';
import { PanneauService } from './panneauInformatif.service';

const MAX_SCRABBLE = 15;
const NOMBRE_PARAMETRES = 4;

@Injectable()
export class BoiteDeCommunicationService
{
    private ioConnection: any;
    private chatBoxSelectionnee: boolean;
    private messages: Array<Message>;
    private messageContent: string;
    private afficherAide: boolean;

    constructor(private socketService: SocketService, private panneauService: PanneauService)
    {
        this.messageContent = null;
        this.afficherAide = false;
        this.messages = new Array<Message>();
        this.chatBoxSelectionnee = true;
        this.ioConnection = this.socketService.get().subscribe((data: any) => {
            let message = new Message(data.nom, data.message, data.estCommande);
            this.messages.push(message);
        });
    }

    public obtenirMessageCourant(): string
    {
        return this.messageContent;
    }

    public obtenirChatBoxSelectionnee(): boolean
    {
        return this.chatBoxSelectionnee;
    }

    public changerChatBoxSelectionnee(): void
    {
        this.chatBoxSelectionnee = !this.chatBoxSelectionnee;
    }

    public aideEstAffiche(): boolean
    {
        return this.afficherAide;
    }

    public envoyerMessage(): void
    {
        if (this.messageContent !== null)
        {
            let commandeChanger = (this.messageContent.includes('changer') && this.messageContent[0] === '!');
            let commandePasser = (this.messageContent.includes('passer') && this.messageContent[0] === '!');
            let commandePlacer = (this.messageContent.includes('placer') && this.messageContent[0] === '!');
            if (this.messageContent === 'aide')
            {
                this.afficherAide = true;
                this.messageContent = null;
            }
            else if ( commandeChanger && this.panneauService.estActif())
            {
                this.socketService.envoyerMessage(new Message(this.panneauService.obtenirNomUtilisateur(),
                                                    this.messageContent, true));
                this.echanger();
            }
            else if ( commandePasser && this.panneauService.estActif())
            {
                this.socketService.envoyerMessage(new Message(this.panneauService.obtenirNomUtilisateur(),
                                                           this.messageContent, true));
                this.socketService.finirTour();
                this.messageContent = null;
            }
            else if (commandePlacer && this.panneauService.estActif())
            {
                this.socketService.envoyerMessage(new Message(this.panneauService.obtenirNomUtilisateur(),
                                                    this.messageContent, true));
                this.placer();
            }
            else if (this.messageContent[0] === '!' && !(commandeChanger || commandePasser || commandePlacer))
            {
                this.messageContent = null;
                this.messages.push(new Message("ALERTE",
                                                    "COMMANDE INCONNUE! Vérifier l'orthographe ou"
                                                     + "bien tapez aide pour la liste de commandes possibles", false));
            }
            else if ( (commandeChanger || commandePasser || commandePlacer) && !this.panneauService.estActif())
            {
                this.messageContent = null;
                this.messages.push(new Message("ALERTE",
                                                    "Veuillez attendre votre tour", true));
            }
            else
            {
                this.afficherAide = false;
                this.socketService.envoyerMessage(new Message(this.panneauService.obtenirNomUtilisateur(),
                                                    this.messageContent, false));
                this.messageContent = null;
            }
        }
    }

    public echanger(): void
    {
        let lettresAechanger;
        lettresAechanger = this.messageContent.split(' ')[1];
        this.messageContent = null;
        if (lettresAechanger !== undefined)
        {
            this.socketService.echangerLettre(lettresAechanger);
        }
        else
        {
        this.messages.push(new Message("ALERTE",
                                            "Échange impossible!", true));
        }
    }

    public placer(): void
    {
        let positionHorizontal , positionVertical , direction , placerParametres;
        placerParametres = this.messageContent.split(' ')[1];
        if (placerParametres !== undefined)
        {

            if (placerParametres.length === NOMBRE_PARAMETRES - 1)
            {
                positionHorizontal = placerParametres[0].toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0);
                positionVertical = Number(placerParametres[1]);
                direction = placerParametres[2];
            }
            else if (placerParametres.length === NOMBRE_PARAMETRES)
            {
                positionHorizontal = placerParametres[0].toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0);
                positionVertical = Number(placerParametres.substr(1, 2));
                direction = placerParametres[NOMBRE_PARAMETRES - 1];
            }
        }
        let motAPlacer = this.messageContent.split(' ')[2];
        let parametresInvalides = (positionHorizontal < 0 || positionHorizontal > MAX_SCRABBLE - 1)
        || (positionVertical < 1 || positionVertical > MAX_SCRABBLE)
        || (direction !== "h" && direction !== "v") || (motAPlacer === undefined)
        || (positionVertical === undefined) || (positionVertical === undefined) || isNaN(positionVertical)
        || (direction === undefined);
        if (!parametresInvalides)
        {
            let mot : Array<string> = new Array<string>();
            let position : Array<number> = new Array<number>();
            mot.push(motAPlacer.toUpperCase());
            mot.push(direction.toUpperCase());
            position.push(positionHorizontal);
            position.push(positionVertical - 1);
            if (!this.socketService.placerMot(mot, position))
            {
                this.messages.push(new Message("ALERTE",
                            "Mot invalide!", true));
            }
        }
        else
        {
            this.messages.push(new Message("ALERTE",
                        "Paramètres invalides dans la commande placer!", true));
        }
        this.messageContent = null;
    }
}
