import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterfaceJeuComponent } from './interfaceJeu.component';
import { LoginComponent } from './login.component';
import { CanActivateGuard } from './loginGuard';

const appRoutes: Routes = [
    {path: 'loginScrabble', component: LoginComponent},
    {path: 'partieScrabble', component: InterfaceJeuComponent, canActivate: [CanActivateGuard]},
    {path: '', redirectTo: '/loginScrabble', pathMatch: 'full'}

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
