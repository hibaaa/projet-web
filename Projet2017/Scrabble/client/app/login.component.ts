import { Component } from '@angular/core';
import { LoginService } from './login.service';
import { SocketService } from './socket.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fenetre-login',
  templateUrl: 'app/login.component.html'
})

export class LoginComponent {

  constructor(private router: Router, private login: LoginService, private socketService : SocketService)
  {
  }

  chercherPartie(){
    if (this.login.nomUtilisateurEstVide())
    {
      this.login.nomJoueurInvalide = true;
      this.login.erreurAuthentification = "Veuillez choisir un nom!";
    }
    else
    {
      this.login.nomJoueurInvalide = false;
      this.login.erreurAuthentification = "";
      this.socketService.validerUtilisateur(this.login.obtenirNomUtilisateur());
    }
  }
}
