import { AppComponent } from './app.component';
import { expect } from 'chai';
import { Router } from '@angular/router';

describe('AppComponent', function () {
  let comp: AppComponent;
  let router: Router;

  beforeEach(() => {
    comp = new AppComponent(router);
  });

  it('test creation du Appcomponent', done =>
  {
    expect(comp).to.not.be.undefined;
    done();
  });

});
