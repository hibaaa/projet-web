import { Component } from '@angular/core';
import { PanneauService } from './panneauInformatif.service';
import { SocketService } from './socket.service';
import { LoginService } from './login.service';

@Component({
  selector: 'panneau-informatif',
  templateUrl: 'app/panneauInformatif.component.html'
})

export class PanneauInformatifComponent
{
    constructor(private panneauService : PanneauService,
                private socketService : SocketService,
                private loginService: LoginService){}

    minuterieFini(fini: boolean)
    {
        if (fini)
        {
            this.socketService.finirTour();
        }
    }
}
