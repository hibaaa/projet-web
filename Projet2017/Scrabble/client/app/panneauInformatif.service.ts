import { Injectable } from "@angular/core";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { MinuterieService } from './minuterie.service';

@Injectable()
export class PanneauService{

    private nomUtilisateur : string;
    private nbJoueurs : number;
    private tailleReserve : number;
    private tailleChevalet : number;
    private actif: boolean;
    private score: number;
    private partieFini: boolean;

    constructor(private minuterieService : MinuterieService)
    {
        this.actif = false;
        this.nomUtilisateur = "";
        this.tailleReserve = 102;
        this.nbJoueurs = 2;
        this.score = 0;
        this.partieFini = false;
    }

    public estActif(): boolean
    {
        return this.actif;
    }

    public obtenirScore(): number
    {
        return this.score;
    }

    public obtenirNomUtilisateur(): string
    {
        return this.nomUtilisateur;
    }

    public obtenirNombreJoueurs(): number
    {
        return this.nbJoueurs;
    }

    public nomUtilisateurEstVide(): boolean
    {
        return (this.nomUtilisateur === "");
    }

    public modifierTailleReserve(nouvelleTaille: number): void
    {
        this.tailleReserve = nouvelleTaille;
    }

    public modifierNbJoueurs(nbJoueurs_ : any): void
    {
        this.nbJoueurs = nbJoueurs_;
    }

    public modifierTailleChevalet(taille: number): void
    {
        this.tailleChevalet = taille;
    }

    public modifierNomUtilisateur(nom : string): void
    {
        this.nomUtilisateur = nom;
    }

    public modifierScore(nouveauScore: number): void
    {
        this.score = nouveauScore;
    }

    public rendreActif(): void
    {
        this.actif = true;
        this.minuterieService.commencerMinuterie();
    }

    public rendreInactif(): void
    {
        this.actif = false;
        this.minuterieService.reinitialiserMinuterie();
    }

    public terminerPartie(nomGagnant: string, scoreGagnant: number): void
    {
        this.partieFini = true;
        document.getElementById("gagnant").innerHTML = "GAGNANT : " + nomGagnant + " SCORE: " + scoreGagnant;
        document.getElementById("panneauInformatif").style.height = "280px";
    }
}
