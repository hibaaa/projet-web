export class Lettre
{
    public lettre: string;
    public valeur: number;

    constructor(lettre: string, valeur: number)
    {
        this.lettre = lettre;
        this.valeur = valeur;
    }

    public obtenirLettre(): string
    {
        return this.lettre;
    }

    public obtenirValeur(): number
    {
        return this.valeur;
    }
}
