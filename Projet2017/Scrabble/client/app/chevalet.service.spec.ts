import { ChevaletService , Case } from './chevalet.service';
import { Lettre } from './lettre';
import { expect } from 'chai';

let a: Lettre = new Lettre('A', 1);
let b: Lettre = new Lettre('B', 1);
let y: Lettre = new Lettre('Y', 1);
let s: Lettre = new Lettre('S', 1);
let o: Lettre = new Lettre('O', 1);
let case0: Case = new Case(a, 0);
let case1: Case = new Case(b, 1);
let case2: Case = new Case(y, 2);
let case3: Case = new Case(a, 3);
let case4: Case = new Case(s, 4);
let case5: Case = new Case(o, 5);
let case6: Case = new Case(s, 6);

let chevalet: Array<Case> = Array<Case>();
chevalet.push(case0, case1, case2, case3, case4, case5, case6);

describe("tests ChevaletService", function()
{
    it ("test de selection de lettre", done =>
    {
      let service = new ChevaletService();
      service.modifierTableLettres(chevalet);
      service.selectionnerLettresChevalet('o');
      expect (service.obtenirIndiceCourant()).to.equal(5);
      service.selectionnerLettresChevalet('s');
      expect (service.obtenirIndiceCourant()).to.equal(6);
      service.selectionnerLettresChevalet('s');
      expect (service.obtenirIndiceCourant()).to.equal(4);
      done();
    });

   it("test d'echange de lettres", done =>
    {
        let service = new ChevaletService();
        service.modifierTableLettres(chevalet);
        service.echangerLettresChevalet('ArrowRight');
        expect (service.obtenirChevalet()[service.obtenirIndiceCourant()].lettre.lettre).to.equal('A');
        expect (service.obtenirIndiceCourant()).to.equal(1);
        service.echangerLettresChevalet('ArrowLeft');
        expect (service.obtenirChevalet()[service.obtenirIndiceCourant()].lettre.lettre).to.equal('A');
        service.echangerLettresChevalet('ArrowLeft');
        expect (service.obtenirChevalet()[service.obtenirIndiceCourant()].lettre.lettre).to.equal('A');
        expect (service.obtenirIndiceCourant()).to.equal(6);
        expect (service.obtenirChevalet()[5].lettre.lettre).to.equal('S');
        expect (service.obtenirChevalet()[1].lettre.lettre).to.equal('Y');
        service.echangerLettresChevalet('ArrowLeft');
        expect (service.obtenirChevalet()[service.obtenirIndiceCourant()].lettre.lettre).to.equal('A');
        expect (service.obtenirIndiceCourant()).to.equal(5);
        expect (service.obtenirChevalet()[6].lettre.lettre).to.equal('S');
        done();
    });

    it("test retour droite", done =>
    {
        let service = new ChevaletService();
        service.modifierTableLettres(chevalet);
        service.retourExtremiteDroite();
        expect (service.obtenirChevalet()[0].lettre.lettre).to.equal('S');
        done();
    });

    it("test retour gauche", done =>
    {
        let service = new ChevaletService();
        service.modifierTableLettres(chevalet);
        service.retourExtremiteGauche();
        expect (service.obtenirChevalet()[6].lettre.lettre).to.equal('S');
        done();
    });
});
