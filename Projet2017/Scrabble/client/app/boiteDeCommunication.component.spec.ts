import { BoiteDeCommunicationComponent } from './boiteDeCommunication.component';
import { expect } from 'chai';

import { BoiteDeCommunicationService } from './boiteDeCommunication.service';

describe('BoiteDeCommunicationComponent', function () {
  let boiteDeCommunication: BoiteDeCommunicationComponent;
  let boiteDeCommunicationService: BoiteDeCommunicationService;

  beforeEach(() => {
    boiteDeCommunication = new BoiteDeCommunicationComponent(boiteDeCommunicationService);
  });

  it('test creation de la boite de communication', done =>
  {
    expect(boiteDeCommunication).to.not.be.undefined;
    done();
  });

});
