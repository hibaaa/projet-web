import { Component, AfterViewChecked } from '@angular/core';
import { BoiteDeCommunicationService } from './boiteDeCommunication.service';

@Component({
  selector: 'boite-communication',
  templateUrl: 'app/boiteDeCommunication.component.html'
})

export class BoiteDeCommunicationComponent implements AfterViewChecked {

  titre = 'DISCUSSION';

  constructor(private boiteDeCommunicationService: BoiteDeCommunicationService)
  {}

  ngAfterViewChecked()
  {
    let element = document.querySelector('affichageMessages');
    if (element)
    {
      element.scrollTop = element.scrollHeight;
    }
  }
}
