import { expect} from 'chai';
import { Joueur } from './joueur';
import { Lettre } from './lettre';

const NOMBRE_LETTRES_CHEVALET = 7;

describe('Test de le classe Joueur ', () => {
    let joueur = new Joueur("joueur");
    let chevaletJoueur : Array<Lettre>;
    let lettre : Lettre;
    it('Test recherche des lettres sur le chevalet d"un joueur', done => {
        chevaletJoueur = [];
        for (let i = 0; i < NOMBRE_LETTRES_CHEVALET; i++)
        {
            lettre = new Lettre();
            lettre.idLettre = 'ABCDEFG'.charAt(i);
            chevaletJoueur.push(lettre);
        }
        joueur.lettres = chevaletJoueur;
        expect(joueur.obtenirNombreLettres()).to.equal(NOMBRE_LETTRES_CHEVALET);
        expect(joueur.trouverLettre("A")).to.equal(true);
        expect(joueur.trouverLettre("B")).to.equal(true);
        expect(joueur.trouverLettre("C")).to.equal(true);
        expect(joueur.trouverLettre("D")).to.equal(true);
        expect(joueur.trouverLettre("E")).to.equal(true);
        expect(joueur.trouverLettre("F")).to.equal(true);
        expect(joueur.trouverLettre("G")).to.equal(true);
        expect(joueur.trouverLettre("H")).to.equal(false);
        expect(joueur.trouverLettre("I")).to.equal(false);
        expect(joueur.trouverLettre("J")).to.equal(false);
        expect(joueur.trouverLettre("K")).to.equal(false);
        expect(joueur.trouverLettre("L")).to.equal(false);
        expect(joueur.trouverLettre("M")).to.equal(false);
        expect(joueur.trouverLettre("N")).to.equal(false);
        expect(joueur.trouverLettre("O")).to.equal(false);
        expect(joueur.trouverLettre("P")).to.equal(false);
        expect(joueur.trouverLettre("Q")).to.equal(false);
        expect(joueur.trouverLettre("R")).to.equal(false);
        expect(joueur.trouverLettre("S")).to.equal(false);
        expect(joueur.trouverLettre("T")).to.equal(false);
        expect(joueur.trouverLettre("U")).to.equal(false);
        expect(joueur.trouverLettre("V")).to.equal(false);
        expect(joueur.trouverLettre("W")).to.equal(false);
        expect(joueur.trouverLettre("X")).to.equal(false);
        expect(joueur.trouverLettre("Y")).to.equal(false);
        expect(joueur.trouverLettre("Z")).to.equal(false);
        done();
    });
});
