import { Partie } from './partie';
import { expect } from 'chai';

const DEUX_JOUEURS = 2;
const TROIS_JOUEURS = 3;
const QUATRE_JOUEURS = 4;

describe("Tests partie Scrabble", function()
{
    let pinky = "PINKY";
    let brain = "BRAIN";
    let partieScrabble = Partie.ObtenirInstance(DEUX_JOUEURS);

    it ("Test creation d'une partie deux joueurs", done =>
    {
        let partieDeuxJoueurs : Partie;
        partieDeuxJoueurs = new Partie(DEUX_JOUEURS);
        expect(partieDeuxJoueurs).exist;
        expect(partieDeuxJoueurs.obtenirNbJoueurs()).to.equal(2);
        done();
    });
    it ("Test creation d'une partie trois joueurs", done =>
    {
        let partieTroisJoueurs : Partie;
        partieTroisJoueurs = new Partie(TROIS_JOUEURS);
        expect(partieTroisJoueurs).exist;
        expect(partieTroisJoueurs.obtenirNbJoueurs()).to.equal(3);
        done();
    });
    it ("Test creation d'une partie quatre joueurs", done =>
    {
        let partieQuatreJoueurs : Partie;
        partieQuatreJoueurs = new Partie(QUATRE_JOUEURS);
        expect(partieQuatreJoueurs).exist;
        expect(partieQuatreJoueurs.obtenirNbJoueurs()).to.equal(4);
        done();
    });

    it ("Test placement du premier mot sur la grille", done =>
    {
        partieScrabble.ajouterJoueur(pinky);
        partieScrabble.ajouterJoueur(brain);
        let mot = ["SARDES", "H"];
        let position = [7, 3];
        expect(partieScrabble.estPremierMot()).to.equal(false);
        partieScrabble.placerPremierMot(mot, position, pinky);
        expect(partieScrabble.estPremierMot()).to.equal(true);
        done();
    });

    it ("Test placer des mots sur la grille", done =>
    {
        let mot = ["ARE", "V"];
        let position = [8, 5];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [8, 4];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["SELON", "V"];
        position = [7, 8];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        position = [8, 4];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(false);
        mot = ["PETIT", "H"];
        position = [10, 0];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [11, 0];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["ATI", "V"];
        position = [11, 4];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["SIC", "H"];
        position = [13, 3];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [0, 6];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["OELE", "V"];
        position = [11, 0];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["F", "H"];
        position = [11, 3];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [10, 0];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["DE", "H"];
        position = [14, 2];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["YE", "H"];
        position = [11, 6];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [0, 10];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["H", "H"];
        position = [10, 7];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["NE", "V"];
        position = [8, 2];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["PI", "H"];
        position = [8, 0];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["RI", "V"];
        position = [7, 1];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["AVER", "H"];
        position = [9, 9];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["VOGU", "V"];
        position = [5, 11];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["BOA", "H"];
        position = [6, 10];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["IN", "V"];
        position = [10, 10];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["ATE", "V"];
        position = [10, 12];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["IL", "H"];
        position = [10, 13];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["WE", "H"];
        position = [12, 13];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["MU", "V"];
        position = [13, 14];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["N", "H"];
        position = [14, 13];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["GEMI", "V"];
        position = [3, 3];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["DIS", "H"];
        position = [6, 2];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["LEGO", "H"];
        position = [3, 1];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        position = [3, 6];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(false);
        mot = ["H", "H"];
        position = [4, 4];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["LUN", "V"];
        position = [0, 2];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["SURE", "H"];
        position = [1, 1];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        position = [1, 0];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(false);
        mot = ["SA", "H"];
        position = [0, 0];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["M", "H"];
        position = [0, 4];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        mot = ["OUI", "V"];
        position = [4, 7];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["BOXAT", "H"];
        position = [4, 6];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        position = [6, 4];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(false);
        mot = ["RINCAT", "V"];
        position = [0, 9];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["JOU", "H"];
        position = [0, 6];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        position = [6, 0];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(false);
        mot = ["KA", "V"];
        position = [1, 7];
        expect(partieScrabble.placerMot(mot, position, brain)).to.equal(true);
        mot = ["F", "H"];
        position = [1, 10];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(true);
        position = [6, 0];
        expect(partieScrabble.placerMot(mot, position, pinky)).to.equal(false);
        done();
    });

    it ("Test calcul de score pour chaque joueur a la fin de partie (Sans BINGO)", done =>
    {
        let joueurs = partieScrabble.obtenirListeJoueurs();
        expect(joueurs.length).to.equal(2);
        let joueurUn = joueurs[0];
        let joueurDeux = joueurs[1];
        expect(joueurUn.obtenirNom()).to.equal("PINKY");
        expect(joueurDeux.obtenirNom()).to.equal("BRAIN");
        expect(joueurUn.obtenirScore()).to.equal(211);
        expect(joueurDeux.obtenirScore()).to.equal(181);
        done();
    });
});
