export class Mot
{
    public unMot: string;
    public ligne?: number;
    public colonne?: number;
    public direction?: string;

    constructor()
    {
        this.unMot = "";
        this.ligne = undefined;
        this.colonne = undefined;
        this.direction = undefined;
    }

    modifierMot(mot: string, ligne: number, colonne: number, direction: string): void
    {
        this.unMot = mot;
        this.ligne = ligne;
        this.colonne = colonne;
        this.direction = direction;
    }
}
