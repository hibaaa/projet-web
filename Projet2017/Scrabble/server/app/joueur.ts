import { Lettre } from './lettre';

export class Joueur
{
    public nomJoueur : string;
    public lettres : Array<Lettre>;
    private actif : boolean;
    private score : number;

    constructor(nomJoueur_ : string)
    {
        this.score = 0;
        this.nomJoueur = nomJoueur_;
        this.lettres = [];
        this.actif = false;
    }

    public obtenirNom(): string
    {
        return this.nomJoueur;
    }

    public obtenirLettres(): Array<Lettre>
    {
        return this.lettres;
    }

    public obtenirScore(): number
    {
        return this.score;
    }

    public modifierScore(nouveauScore: number): void
    {
        this.score += nouveauScore;
    }

    public estActif(): boolean
    {
        return this.actif;
    }

    public rendreActif(): void
    {
        this.actif = true;
    }

    public rendreInactif(): void
    {
        this.actif = false;
    }

    public obtenirNombreLettres(): number
    {
        return this.lettres.length;
    }

    public trouverLettre(lettre : string): boolean
    {
        let estSurChevalet = false;
        this.lettres.find( element => {
            if (element.idLettre === lettre)
            {
                estSurChevalet = true;
                return estSurChevalet;
            }
        });
        return estSurChevalet;
    }
}
