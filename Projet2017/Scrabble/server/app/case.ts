import { Lettre } from './lettre';

const MAX_SCRABBLE = 15;
const CENTRE = 7;

export class Case
{
    public lettre: Lettre;
    public bonusLettre: number;
    public bonusMot: number;
    public estUtilisee: boolean;

    constructor()
    {
        this.lettre = new Lettre();
        this.bonusLettre = 0;
        this.bonusMot = 0;
        this.estUtilisee = false;
    }

    public verifierCaseMotTriple(ligne: number, colonne: number): boolean
    {
        return (((ligne === 0 || (ligne % CENTRE) === 0) && (colonne === 0 || (colonne % CENTRE) === 0))
        && (!(ligne === CENTRE && colonne === CENTRE)));
    }

    public verifierCaseMotDouble(ligne: number, colonne: number): boolean
    {
        return (((ligne > 0 && ligne <= Math.round(CENTRE / 2))
        && (ligne === colonne || colonne === (MAX_SCRABBLE - ligne - 1)))
        || ((ligne >= MAX_SCRABBLE - Math.round(CENTRE / 2) - 1)
        && (colonne === MAX_SCRABBLE - ligne - 1 || ligne === colonne)));
    }

    public verifierCaseLettreTriple(ligne: number, colonne: number): boolean
    {
        return ((ligne === CENTRE - 2 || ligne === CENTRE + 2)
        && (colonne === CENTRE - 2 || colonne === CENTRE + 2) || (colonne === CENTRE + 2 || colonne === CENTRE - 2)
        && (ligne === CENTRE + Math.round(CENTRE / 2) + 2 || ligne === CENTRE - Math.round(CENTRE / 2) - 2)
        || (ligne === CENTRE + 2 || ligne === CENTRE - 2)
        && (colonne === CENTRE + Math.round(CENTRE / 2) + 2 || colonne === CENTRE - Math.round(CENTRE / 2) - 2));
    }

    public verifierCaseLettreDouble(ligne: number, colonne: number): boolean
    {
        let lignesCentrales = (ligne === CENTRE - 1) || (ligne === CENTRE + 1);
        let colonnesCentrales = (colonne === CENTRE - 1) || (colonne === CENTRE + 1);
        let lignesExtremiteEtMilieu = (ligne === 0 || ligne === MAX_SCRABBLE - 1 || ligne === CENTRE);
        let colonnesExtremiteEtMilieu = (colonne === 0 || colonne === MAX_SCRABBLE - 1 || colonne === CENTRE);
        let lignesCentralesMilieu = (ligne === CENTRE + Math.floor(CENTRE / 2) + 1
        || ligne === CENTRE - Math.floor(CENTRE / 2) - 1);
        let colonnesCentralesMilieu = (colonne === CENTRE + Math.floor(CENTRE / 2) + 1
        || colonne === CENTRE - Math.floor(CENTRE / 2) - 1);
        let lignesEntreExtremites = (ligne === CENTRE + Math.floor(CENTRE / 2) + 2
        || ligne === CENTRE - Math.floor(CENTRE / 2) - 2);
        let colonnesEntreExtremites = (colonne === CENTRE + Math.floor(CENTRE / 2) + 2
        || colonne === CENTRE - Math.floor(CENTRE / 2) - 2);
        return (
            lignesCentrales && colonnesCentrales ||
            lignesExtremiteEtMilieu && colonnesCentralesMilieu ||
            colonnesExtremiteEtMilieu && lignesCentralesMilieu ||
            colonnesCentrales && lignesEntreExtremites ||
            lignesCentrales && colonnesEntreExtremites
        );
    }
}
