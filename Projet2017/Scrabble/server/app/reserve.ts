import * as mongoose from 'mongoose';

export interface Ireserve extends mongoose.Document
{
    lettre: string;
    valeur: number;
}

let reserveSchema = new mongoose.Schema(
{
    lettre: String,
    valeur: Number
});

export default mongoose.model<Ireserve>("reserve", reserveSchema);
