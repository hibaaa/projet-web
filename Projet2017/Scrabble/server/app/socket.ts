import * as socketIo from "socket.io";
import { GestionnaireDePartie } from './gestionnaireDePartie';
import { Partie } from './partie';
import { MongoService } from './mongoService';

export class Socket{

    private static instance : Socket;
    private io: any;
    private rooms_: Array<any>;

    public static obtenirInstance(): Socket
    {
        if (!Socket.instance)
        {
            Socket.instance = new Socket();
        }
        return Socket.instance;
    }

    constructor(){}

    public initialiser(server: any): void
    {
        this.io = socketIo(server);
        this.rooms_ = [];
        this.seConnecter();
    }

    private seConnecter(): void
    {
        this.io.on('connect', (socket: any) =>
        {
            console.log("Client connected");

            socket.on('chercherPartie', (data: any) =>
            {
                let partieTrouvee = this.trouverPartie(data.nbJoueurs, data.nomUtilisateur);
                let id = partieTrouvee.obtenirId();
                socket.emit('partietrouve', id );
                socket.join(id);
                if (partieTrouvee.estPartieComplete()){
                    setTimeout(() => {
                        this.io.in(id).emit('partieLancee', {taille: partieTrouvee.obtenirTailleReserve()});
                    }, 1000);
                }
            });

            socket.on('message', (data: any) =>
            {
                console.log('[server](message): ', JSON.stringify(data.message));
                this.io.in(data.idPartie).emit('message', data.message);
            });

            socket.on('InitialisationPartie', (data: any) =>
            {
                let partie = GestionnaireDePartie.ObtenirInstance().trouverPartieParId(data.id);
                let listeJoueurs = partie.obtenirListeJoueurs();
                listeJoueurs.forEach(element => {
                    if (element.obtenirNom() === data.nom)
                    {
                        let lettres = element.obtenirLettres();
                        socket.emit('ActualisationDuChevalet', {nouvelleLettres : lettres} );
                        socket.emit('JoueurActif', element.estActif());
                    }
                });
            });

            socket.on('placerMot', (data: any) =>
            {
                let partie = GestionnaireDePartie.ObtenirInstance().trouverPartieParId(data.idPartie);
                if (partie.estPremierMot())
                {
                    partie.placerMot(data.mot, data.position, data.nom);
                }
                else
                {
                    partie.placerPremierMot(data.mot, data.position, data.nom);
                }
                if (partie.estPartieFinie())
                {
                    let actif = partie.obtenirActif();
                    let nomActif = actif.obtenirNom();
                    let scoreActif = actif.obtenirScore();
                    let lettresActif = actif.obtenirLettres();
                    let gagnant = partie.trouverGagnant();
                    let nomGagnant = gagnant.obtenirNom();
                    let scoreGagnant = gagnant.obtenirScore();
                    this.io.in(partie.obtenirId()).emit('partieFini',
                            {nomGagnant: nomGagnant, scoreGagnant: scoreGagnant, grille : partie.obtenirGrille()
                            , score : scoreActif , nom : nomActif , lettresFinale: lettresActif
                            , tailleChevalet : lettresActif.length});
                }
                else
                {
                    socket.emit('prochainTour');
                }
            });

            socket.on('TourFini' , (data : any) =>
            {
                let partie = GestionnaireDePartie.ObtenirInstance().trouverPartieParId(data.id);
                // pour emit le score du joueur
                let ancienActif = partie.obtenirActif();
                let nomAncienActif = ancienActif.obtenirNom();
                partie.prochainTour();
                let nomProchainActif = partie.obtenirActif().obtenirNom();
                this.io.in(data.id).emit('EstCeMonTour', {ancienActif: nomAncienActif ,
                nouveauActif: nomProchainActif , grille : partie.obtenirGrille() ,
                score: ancienActif.obtenirScore(), taille: partie.obtenirTailleReserve() });
            });

            socket.on('ActualisationChevalet' , (data : any) =>
            {
                let partie = GestionnaireDePartie.ObtenirInstance().trouverPartieParId(data.id);
                let listeJoueurs = partie.obtenirListeJoueurs();
                listeJoueurs.forEach(element => {
                    if (element.obtenirNom() === data.nom)
                    {
                        let lettres = element.obtenirLettres();
                        socket.emit('ActualisationDuChevalet', {nouvelleLettres: lettres});
                    }
                });
            });

            socket.on('echangeDeLettres', (data: any) =>
            {
                let partie = GestionnaireDePartie.ObtenirInstance().trouverPartieParId(data.idPartie);
                let echange = partie.echangerLettre(data.nomUtilisateur, data.lettres.toUpperCase());
                let lettres = partie.trouverJoueur(data.nomUtilisateur).obtenirLettres();
                socket.emit('ActualisationDuChevalet', {nouvelleLettres: lettres, echange: echange});
                if (echange)
                {
                    socket.emit('prochainTour');
                }
            });

            socket.on('validerUtilisateur', (nomUtilisateur : string) =>
            {
                MongoService.obtenirInstance().ajouterUtilisateur(nomUtilisateur)
                .then(reponse =>
                {
                    if (reponse)
                    {
                        socket.emit('utilisateurAjoute');
                    }
                    else
                    {
                        socket.emit('utilisateurExistant');
                    }
                });
            });

            socket.on('disconnect', () =>
            {
                console.log('Client disconnected');
            });
        });
    }

    private trouverPartie(nbJoueurs: number , nomUtilsisateur: string) : Partie
    {
        let partie = GestionnaireDePartie.ObtenirInstance().trouverPartie(nbJoueurs);
        partie.ajouterJoueur(nomUtilsisateur);
        return partie;
    }
}
