import { dictionnaire } from './dictionnaireScrabble';
import { Case } from './case';
import { Mot } from './mot';

const MAX_SCRABBLE = 15;
//Tableau des points attribues a chaque lettre en ordre alphabetique
const POINTS = [1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 10, 1, 3, 1, 1, 3, 8, 1, 1, 1, 1, 4, 10, 10, 10, 10];

export class GestionnaireDeValidation
{
    private grille: Array<Array<Case>>;
    private score: number;

    constructor()
    {
        this.grille = [];
        for (let i = 0; i < MAX_SCRABBLE; i++)
        {
            this.grille[i] = [];
            for (let j = 0; j < MAX_SCRABBLE; j++)
            {
                this.grille[i][j] = new Case();
                if (this.grille[i][j].verifierCaseMotTriple(i, j))
                {
                    this.grille[i][j].bonusMot = 3;
                }
                else if (this.grille[i][j].verifierCaseMotDouble(i, j))
                {
                    this.grille[i][j].bonusMot = 2;
                }
                else if (this.grille[i][j].verifierCaseLettreDouble(i, j))
                {
                    this.grille[i][j].bonusLettre = 2;
                }
                else if (this.grille[i][j].verifierCaseLettreTriple(i, j))
                {
                    this.grille[i][j].bonusLettre = 3;
                }
                else
                {
                    this.grille[i][j].bonusLettre = 0;
                    this.grille[i][j].bonusMot = 0;
                }
            }
        }
        this.score = 0;
    }

    public obtenirGrille(): Array<Array<Case>>
    {
        return this.grille;
    }

    public obtenirScore(): number
    {
        return this.score;
    }

    public ajouterMotHorizontalValide(mot: string, position: number []): void
    {
        let ligne = position[0];
        let colonne = position[1];
        for (let i = 0; i < mot.length; i++)
        {
            this.grille[ligne][colonne + i].lettre.idLettre = mot[i];
            this.grille[ligne][colonne + i].lettre.valeur = POINTS[mot.charCodeAt(i) - "A".charCodeAt(0)];
            this.grille[ligne][colonne + i].estUtilisee = true;
        }
    }

    public ajouterMotVerticalValide(mot: string, position: number []): void
    {
        let ligne = position[0];
        let colonne = position[1];
        for (let j = 0; j < mot.length; j++)
        {
            this.grille[ligne + j][colonne].lettre.idLettre = mot[j];
            this.grille[ligne + j][colonne].lettre.valeur = POINTS[mot.charCodeAt(j) - "A".charCodeAt(0)];
            this.grille[ligne + j][colonne].estUtilisee = true;
        }
    }

    public verifierMotFrancais(mot: string): boolean
    {
        let debut = 0;
        let fin = dictionnaire.length - 1 ;
        let milieu: number;
        while (debut <= fin)
        {
            milieu = Math.floor((debut + fin) / 2);
            if (dictionnaire[milieu] === mot)
            {
                return true ;
            }
            else if (dictionnaire[milieu] < mot)
            {
                 debut = milieu + 1;
            }
            else
            {
                fin = milieu - 1;
            }
        }
        return false;
    }

    private verifierEnvironnementCoins(ligne: number, colonne: number): boolean
    {
        if (ligne === 0 && colonne === 0)
        {
            return ((this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
        }
        if (ligne === MAX_SCRABBLE - 1 && colonne === 0)
        {
            return ((this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne - 1][colonne].lettre.idLettre !== undefined));
        }
        if (colonne === MAX_SCRABBLE - 1 && ligne === 0)
        {
            return ((this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
        }
        if (colonne === MAX_SCRABBLE - 1 && ligne === colonne)
        {
            return ((this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne - 1][colonne].lettre.idLettre !== undefined));
        }
    }

    private verifierEnvironnementLigneSup(ligne: number, colonne: number): boolean
    {
        return ((this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                || (this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
    }

    private verifierEnvironnementLigneDroite(ligne: number, colonne: number): boolean
    {
        return ((this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                || (this.grille[ligne - 1][colonne].lettre.idLettre !== undefined)
                || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
    }

    private verifierEnvironnementLigneGauche(ligne: number, colonne: number): boolean
    {
        return ((this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                || (this.grille[ligne - 1][colonne].lettre.idLettre !== undefined)
                || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
    }

    private verifierEnvironnementLigneInf(ligne: number, colonne: number): boolean
    {
        return ((this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                || (this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                || (this.grille[ligne - 1][colonne].lettre.idLettre !== undefined));
    }

    private verifierEnvironnementLettre(ligne: number, colonne: number): boolean
    {
        //Verifier si une lettre donnee touche a une lettre deja placee sur la grille
        if (ligne > 0 && ligne < MAX_SCRABBLE - 1 && colonne > 0 && colonne < MAX_SCRABBLE - 1)
        {
            return ((this.grille[ligne - 1][colonne].lettre.idLettre !== undefined)
                    || (this.grille[ligne][colonne - 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne][colonne + 1].lettre.idLettre !== undefined)
                    || (this.grille[ligne + 1][colonne].lettre.idLettre !== undefined));
        }

        else if (ligne === 0 && colonne > 0 && colonne < MAX_SCRABBLE - 1)
        {
            return this.verifierEnvironnementLigneSup(ligne, colonne);
        }
        else if (ligne === MAX_SCRABBLE - 1 && colonne > 0 && colonne < MAX_SCRABBLE - 1)
        {
            return this.verifierEnvironnementLigneInf(ligne, colonne);
        }
        else if (colonne === 0 && ligne > 0 && ligne < MAX_SCRABBLE - 1)
        {
            return this.verifierEnvironnementLigneGauche(ligne, colonne);
        }
        else if (colonne === MAX_SCRABBLE - 1 && ligne > 0 && ligne < MAX_SCRABBLE - 1)
        {
            return this.verifierEnvironnementLigneDroite(ligne, colonne);
        }
        else
        {
            return (this.verifierEnvironnementCoins(ligne, colonne));
        }
    }

    public verifierEnvironnementMotVertical(mot: string, position: number[]): boolean
    {
        let ligneDebut = position[0];
        let colonneDebut = position[1];
        for (let ligne = ligneDebut; ligne < ligneDebut + mot.length; ligne++)
        {
            //Si la case correspondante a la lettre a placer est occupee,
            //on verifie si c'est la meme lettre
            if (this.grille[ligne][colonneDebut].lettre.idLettre !== undefined)
            {
                if (this.grille[ligne][colonneDebut].lettre.idLettre !== mot[ligne - ligneDebut])
                {
                        return false;
                }
            }
        }
        //Verifier si au moins une lettre du mot touche a une lettre deja placee sur la grille
        for (let ligne = ligneDebut; ligne < ligneDebut + mot.length; ligne++)
        {
            if (this.grille[ligne][colonneDebut].lettre.idLettre === undefined)
            {
                if (this.verifierEnvironnementLettre(ligne, colonneDebut))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public verifierEnvironnementMotHorizontal(mot: string, position: number[]): boolean
    {
        let ligneDebut = position[0];
        let colonneDebut = position[1];
         for (let colonne = colonneDebut; ((colonne < colonneDebut + mot.length)); colonne++)
         {
            //Si la case correspondante a la lettre a placer est occupee,
            //on verifie si c'est la meme lettre
            if (this.grille[ligneDebut][colonne].lettre.idLettre !== undefined)
            {
                if (this.grille[ligneDebut][colonne].lettre.idLettre !== mot[colonne - colonneDebut])
                {
                        return false;
                }
            }
        }
        //Verifier si au moins une lettre du mot touche a une lettre deja placee sur la grille
        for (let colonne = colonneDebut; colonne < colonneDebut + mot.length; colonne++)
        {
           if (this.grille[ligneDebut][colonne].lettre.idLettre === undefined)
            {
                if (this.verifierEnvironnementLettre(ligneDebut, colonne))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public calculerScoreMot(position: number[], motDirection: string[]): number
    {
        let score = 0;
        let bonusMot = 1;
        let ligne = position[0];
        let colonne = position[1];
        let mot = motDirection[0];
        let direction = motDirection[1];

        if (direction === "H"){
            for (let i = 0; i < mot.length; i++){
                let lettre = mot.charCodeAt(i) - "A".charCodeAt(0);
                if ((this.grille[ligne][colonne + i].bonusMot > 1) && !(this.grille[ligne][colonne + i].estUtilisee)){
                    bonusMot *= this.grille[ligne][colonne + i].bonusMot;
                    score += POINTS[lettre];
                }
                else if ((this.grille[ligne][colonne + i].bonusLettre > 1)
                && !(this.grille[ligne][colonne + i].estUtilisee)){
                    score += POINTS[lettre] * this.grille[ligne][colonne + i].bonusLettre;
                }
                else{
                    score += POINTS[lettre];
                }
            }
        }
        else if (direction === "V"){
            for (let i = 0; i < mot.length; i++){
                let lettre = mot.charCodeAt(i) - "A".charCodeAt(0);
                if ((this.grille[ligne + i][colonne].bonusMot > 1) && !(this.grille[ligne + i][colonne].estUtilisee)){
                    bonusMot *= this.grille[ligne + i][colonne].bonusMot;
                    score += POINTS[lettre];
                }
                else if ((this.grille[ligne + i][colonne].bonusLettre > 1)
                && !(this.grille[ligne + i][colonne].estUtilisee)){
                    score += POINTS[lettre] * this.grille[ligne + i][colonne].bonusLettre;
                }
                else{
                    score += POINTS[lettre];
                }
            }
        }
        return score * bonusMot;
    }

    private trouverMotGauche(ligne: number, colonne: number): Mot
    {
        let finMot = false;
        let mot = new Mot();
        for (let i = colonne; i >= 0 && !finMot ; i--)
        {
            if (this.grille[ligne][i].estUtilisee)
            {
                mot.unMot += this.grille[ligne][i].lettre.idLettre;
            }
            else
            {
                finMot = true;
            }
        }
        mot.modifierMot((mot.unMot).split("").reverse().join(""), ligne, colonne - mot.unMot.length + 1, "H");
        return mot;
    }

    private trouverMotDroite(ligne: number, colonne: number): Mot
    {
        let finMot = false;
        let mot = new Mot();
        for (let i = colonne; i < MAX_SCRABBLE && !finMot ; i++)
        {
            if (this.grille[ligne][i].estUtilisee)
            {
                mot.unMot += this.grille[ligne][i].lettre.idLettre;
            }
            else
            {
                finMot = true;
            }
        }
        return mot;
    }

    private trouverMotEnDessous(ligne: number, colonne: number): Mot
    {
        let finMot = false;
        let mot = new Mot();
        for (let i = ligne; i < MAX_SCRABBLE && !finMot ; i++)
        {
            if (this.grille[i][colonne].estUtilisee)
            {
                mot.unMot += this.grille[i][colonne].lettre.idLettre;
            }
            else
            {
                finMot = true;
            }
        }
        return mot;
    }

    private trouverMotAuDessus(ligne: number, colonne: number): Mot
    {
        let finMot = false;
        let mot = new Mot();
        for (let i = ligne; i >= 0 && !finMot ; i--)
        {
            if (this.grille[i][colonne].estUtilisee)
            {
                mot.unMot += this.grille[i][colonne].lettre.idLettre;
            }
            else
            {
                finMot = true;
            }
        }
        mot.modifierMot((mot.unMot).split("").reverse().join(""), ligne - mot.unMot.length + 1, colonne, "V");
        return mot;
    }

    private trouverNouveauxMotsVerti(mot: string, position: number[]): Array<Mot>
    {
        let ligne = position[0];
        let colonne = position[1];
        let nouveauxMots: Array <Mot>;
        let nouveauMot = "";
        nouveauxMots = [];
        //Trouver tous les nouveaux mots qui se sont formes automatiquement
        //apres l'ajout d'un mot par le joueur
        for (let i = 0; i < mot.length; i++)
        {
            let motDroite = new Mot();
            let motGauche = new Mot();

            if (!this.grille[i + ligne][colonne].estUtilisee)
            {
                if (colonne !== 14)
                {
                if (this.grille[i + ligne][colonne + 1].estUtilisee)
                    {
                        motDroite = this.trouverMotDroite(i + ligne, colonne + 1);
                    }
                }
                if ( colonne !== 0)
                {
                    if (this.grille[i + ligne][colonne - 1].estUtilisee)
                    {
                        motGauche = this.trouverMotGauche(i + ligne, colonne - 1);
                    }
                }
            }
            nouveauMot = motGauche.unMot + mot[i] + motDroite.unMot;
            let motForme = new Mot();
            if (motGauche.unMot === "")
            {
                motForme.modifierMot(nouveauMot, i + ligne, colonne, "H");
            }
            else
            {
                motForme.modifierMot(nouveauMot, i + ligne, motGauche.colonne, "H");
            }
            if (nouveauMot.length > 1)
            {
                nouveauxMots.push(motForme);
            }
        }
        return nouveauxMots;
    }

    private trouverNouveauxMotsHoriz(mot: string, position: number[]): Array<Mot>
    {
        let ligne = position[0];
        let colonne = position[1];
        let nouveauxMots: Array <Mot>;
        let nouveauMot = "";
        nouveauxMots = [];
        //Trouver tous les nouveaux mots qui se sont formes automatiquement
        //apres l'ajout d'un mot par le joueur
        for (let i = 0; i < mot.length; i++)
        {
            let motDessus = new Mot();
            let motDessous = new Mot();
            if (!this.grille[ligne][colonne + i].estUtilisee)
            {
                if (ligne !== 0)
                {
                    if (this.grille[ligne - 1][colonne + i].estUtilisee)
                    {
                        motDessus = this.trouverMotAuDessus(ligne - 1, colonne + i);
                    }
                }
                if (ligne !== 14)
                {
                    if (this.grille[ligne + 1][colonne + i].estUtilisee)
                    {
                        motDessous = this.trouverMotEnDessous(ligne + 1, colonne + i);
                    }
                }
            }
            nouveauMot = motDessus.unMot + mot[i] + motDessous.unMot;
            let motForme = new Mot();
            if (motDessus.unMot === "")
            {
                motForme.modifierMot(nouveauMot, ligne, colonne + i, "V");
            }
            else
            {
                motForme.modifierMot(nouveauMot, motDessus.ligne, colonne + i, "V");
            }
            if (nouveauMot.length > 1){
                nouveauxMots.push(motForme);
            }
        }
        return nouveauxMots;
    }

    public validerPlacementMotVerticalSurGrille(mot: string, position: number[]): boolean
    {
        let ligne = position[0];
        let colonne = position[1];
        let nouveauxMots: Array<Mot>;
        let motAjoute = new Mot();
        this.score = 0;
        //trouver le mot vertical forme apres le placement
        motAjoute.modifierMot(this.trouverMotAuDessus(ligne - 1, colonne).unMot
        + mot + this.trouverMotEnDessous(ligne + mot.length, colonne).unMot,
        this.trouverMotAuDessus(ligne - 1, colonne).ligne, colonne, "V");
        nouveauxMots = [];
        //Si le mot est valide, on construit un tableau contenant le mot et les nouveaux mots formes
        if ((this.verifierMotFrancais((motAjoute.unMot).toUpperCase()) || ((motAjoute.unMot).length === 1))
        && this.verifierEnvironnementMotVertical(mot.toUpperCase(), position))
        {
            if (motAjoute.unMot.length > 1)
            {
                nouveauxMots.push(motAjoute);
            }
            nouveauxMots = nouveauxMots.concat(this.trouverNouveauxMotsVerti(mot, position));
            //on verifie si tous les nouveaux mots formes sont des mots francais valides
            let i: number;
            for (i = 0; i < nouveauxMots.length; i++)
            {
                if (!this.verifierMotFrancais(nouveauxMots[i].unMot))
                {
                    break;
                }
            }
            //Si le placement est legal, calculer le score du joueur et ajouter le mot
            if (i === nouveauxMots.length)
            {
                for (let itMot of nouveauxMots)
                {
                    let positionMot = [itMot.ligne, itMot.colonne];
                    let motDirection = [itMot.unMot, itMot.direction];
                    this.score += this.calculerScoreMot(positionMot, motDirection);
                }
                this.ajouterMotVerticalValide(mot, position);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public validerPlacementMotHorizontalSurGrille(mot: string, position: number []): boolean
    {
        let ligne = position[0];
        let colonne = position[1];
        let nouveauxMots: Array<Mot>;
        let motAjoute = new Mot();
        this.score = 0;
        //trouver le mot horizontal forme apres le placement
        motAjoute.modifierMot(this.trouverMotGauche(ligne, colonne - 1).unMot
        + mot + this.trouverMotDroite(ligne, colonne + mot.length).unMot,
        ligne, this.trouverMotGauche(ligne, colonne - 1).colonne, "H");
        //Si le mot est valide, on construit un tableau contenant le mot et les nouveaux mots formes
        if ((this.verifierMotFrancais(motAjoute.unMot.toUpperCase()) || (motAjoute.unMot.length === 1))
        && this.verifierEnvironnementMotHorizontal(mot.toUpperCase(), position))
        {
            nouveauxMots = [];
            if (motAjoute.unMot.length > 1)
            {
                nouveauxMots.push(motAjoute);
            }
            nouveauxMots = nouveauxMots.concat(this.trouverNouveauxMotsHoriz(mot, position));
            //on verifie si tous les nouveaux mots formes sont des mots francais valides
            let i: number;
            for (i = 0; i < nouveauxMots.length; i++)
            {
                if (!this.verifierMotFrancais(nouveauxMots[i].unMot))
                {
                    break;
                }
            }
            //Si le placement est legal, calculer le score du joueur et ajouter le mot
            if (i === nouveauxMots.length)
            {
                for (let itMot of nouveauxMots)
                {
                    let positionMot = [itMot.ligne, itMot.colonne];
                    let motDirection = [itMot.unMot, itMot.direction];
                    this.score += this.calculerScoreMot(positionMot, motDirection);
                }
                this.ajouterMotHorizontalValide(mot, position);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}

