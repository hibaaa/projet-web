import { Partie } from './partie';

export class GestionnaireDePartie
{
    private static instance: GestionnaireDePartie;
    private tableDePartie : Array<Partie>;

    public static ObtenirInstance() : GestionnaireDePartie
    {
        if (!GestionnaireDePartie.instance)
        {
            GestionnaireDePartie.instance = new GestionnaireDePartie();
        }
        return GestionnaireDePartie.instance;
    }

    constructor(){
        this.tableDePartie = [];
    }

    private creerPartie(nbJoueurs: number) : Partie
    {
        let thisGameId = ( Math.random() * 100000 );
        let nouvellePartie = new Partie(nbJoueurs);
        nouvellePartie.modifierId(thisGameId);
        this.tableDePartie.push(nouvellePartie);
        return nouvellePartie;
    }

    public trouverPartie(nbJoueurs: number) : Partie
    {
        let partie = this.tableDePartie.find(element =>
        {
            return (element.obtenirNbJoueurs() === nbJoueurs && !element.estPartieComplete());
        });
        if (partie)
        {
            return partie;
        }
        else
        {
            return this.creerPartie(nbJoueurs);
        }
    }

    public trouverPartieParId(id: number) : Partie
    {
        let partie = this.tableDePartie.find(element =>
        {
            return (element.obtenirId() === id);
        });
        if (partie)
        {
            return partie;
        }
        else
        {
            console.log("error");
        }
    }
}

