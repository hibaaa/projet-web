import { GestionnaireTour } from './gestionnaireDeTour';
import { GestionnaireDeValidation} from './GestionnaireDeValidation';
import { Joueur } from './joueur';
import { Lettre } from './lettre';
import { Case } from './case';
import { MongoService } from './mongoService';

const MAX_LETTRE = 7;
const BINGO = 50;
const CENTRE = 7;

export class Partie
{
    private static instance : Partie;
    private id: number;
    private nbJoueurs : number;
    private reserve : Array<Lettre>;
    private joueurs : Array<Joueur>;
    private gestionnaireDeValidation : GestionnaireDeValidation;
    private gestionnaireTour : GestionnaireTour;
    private premierMotPlace : boolean;
    private estComplete : boolean;
    private estFinie : boolean;

    public static ObtenirInstance(nbJoueurs: number) : Partie
    {
        if (!Partie.instance)
        {
            Partie.instance = new Partie(nbJoueurs);
        }
        return Partie.instance;
    }

    constructor(nbJoueurs : number)
    {
        this.id = 0;
        this.nbJoueurs = nbJoueurs;
        this.reserve = [];
        this.gestionnaireTour = new GestionnaireTour();
        this.gestionnaireDeValidation = new GestionnaireDeValidation();
        this.joueurs = [] ;
        this.estComplete = false;
        this.premierMotPlace = false;
        this.estFinie = false;
    }

    public obtenirGrille() : Array<Array<Case>>
    {
        return this.gestionnaireDeValidation.obtenirGrille();
    }

    public obtenirTailleJoueurs(): number
    {
        return this.joueurs.length;
    }

    public obtenirListeJoueurs(): Array<Joueur>
    {
        return this.joueurs;
    }

    public obtenirNbJoueurs() : number
    {
        return this.nbJoueurs;
    }

    public estPartieComplete() : boolean
    {
        return this.estComplete;
    }

    public estPartieFinie() : boolean
    {
        return this.estFinie;
    }

    public obtenirTailleReserve() : number
    {
        return this.reserve.length;
    }

    public obtenirId(): number
    {
        return this.id;
    }

    public estPremierMot(): boolean
    {
        return this.premierMotPlace;
    }

    public modifierId(id_: number): void
    {
        this.id = id_;
    }

    public trouverJoueur(nomUtilisateur: string) : Joueur
    {
        return this.joueurs.find(element =>
        {
            return (element.obtenirNom() === nomUtilisateur);
        });
    }

    private initialiserPartie(): void
    {
        // charger reserve de la bd et tirer 7 lettres au hasard pour chaque joueur
        MongoService.obtenirInstance().copierReserve()
        .then(reponse =>
        {
            reponse.forEach(element =>
            {
                let nouvelleLettre: Lettre = new Lettre();
                nouvelleLettre.idLettre = element.lettre;
                nouvelleLettre.valeur = element.valeur;
                this.reserve.push(nouvelleLettre);
            });
            for (let i = 0; i < this.nbJoueurs; i++)
            {
                for (let j = 0; j < MAX_LETTRE; j++)
                {
                    let pige: number = Math.floor(Math.random() * this.reserve.length);
                    let lettrePige: Lettre = new Lettre();
                    lettrePige.idLettre = this.reserve[pige].idLettre;
                    lettrePige.valeur = this.reserve[pige].valeur;
                    this.joueurs[i].lettres.push(lettrePige);
                    this.reserve.splice(pige, 1);
                }
            }
            this.gestionnaireTour.determinerOrdre(this.nbJoueurs);
            this.joueurs[this.gestionnaireTour.prochainJoueur()].rendreActif();
        }).catch(err => console.log("ERROR"));
        this.estComplete = true;
    }

    public ajouterJoueur(nomUtilisateur : string): void
    {
        if (this.joueurs.length < this.nbJoueurs)
        {
            let nouveauJoueur = new Joueur(nomUtilisateur);
            this.joueurs.push(nouveauJoueur);
            if (this.joueurs.length === this.nbJoueurs)
            {
                this.initialiserPartie();
            }
        }
    }

    public echangerLettre(nomUtilisateur : string , lettres: string) : boolean
    {
        let echange = false;
        if (lettres.length <= this.reserve.length)
        {
            this.joueurs.forEach(joueur =>
            {
                if (joueur.nomJoueur === nomUtilisateur )
                {
                    for (let j = 0; j < lettres.length; j++)
                    {
                        for (let i = 0; i < joueur.lettres.length; i++)
                        {
                            if (joueur.lettres[i].idLettre === lettres[j])
                            {
                                let pige : number = Math.floor(Math.random() * this.reserve.length);
                                this.reserve.push(joueur.lettres[i]);
                                joueur.lettres.splice(i , 1);
                                joueur.lettres.push(this.reserve[pige]);
                                this.reserve.splice(pige , 1);
                                break;
                            }
                        }
                    }
                    echange = true;
                }
            });
        }
        return echange;
    }

    private verifierGrille(mot: string[], position: number[]) : number[]
    {
        //Si la lettre est deja presente sur la grille et que le joueur detient cette lettre sur son chevalet
        //on recupere la position de la lettre pour ne pas l'enlever du chevalet
        let indexALaisser : number[] = [];
        for (let j = 0; j < mot[0].length; j++)
        {
            let lettreSurGrilleH = (mot[1] === "H" &&
            this.gestionnaireDeValidation.obtenirGrille()[position[0]][position[1] + j].estUtilisee);
            let lettreSurGrilleV = (mot[1] === "V" &&
            this.gestionnaireDeValidation.obtenirGrille()[position[0] + j][position[1]].estUtilisee);
            if (lettreSurGrilleH || lettreSurGrilleV)
            {
                indexALaisser.push(j);
            }
        }
        return indexALaisser;
    }

    public placerMot(mot: string[], position: number[], nomJoueur: string): boolean
    {
        let indexALaisser = this.verifierGrille(mot, position);
        if (((mot[1] === "H")
        && (this.gestionnaireDeValidation.validerPlacementMotHorizontalSurGrille(mot[0], position)))
        || ((mot[1] === "V")
        && this.gestionnaireDeValidation.validerPlacementMotVerticalSurGrille(mot[0], position)))
        {
            let score = this.gestionnaireDeValidation.obtenirScore();
            this.actualiserInfoJoueur(mot[0], nomJoueur, score , indexALaisser);
            return true;
        }
        return false;
    }

    private actualiserInfoJoueur(mot : string , nomJoueur : string, score: number, index: number[]): void
    {
        let compteur = 0;
        this.joueurs.forEach(joueur =>
        {
            if (joueur.obtenirNom() === nomJoueur)
            {
                for (let j = 0; j < mot.length; j++)
                {
                    for (let i = 0; i < joueur.lettres.length; i++)
                    {
                        let aLaisser = index.find( element => {return element === j; });
                        let lettre = joueur.trouverLettre(mot[j]);
                        if (this.reserve.length === 0)
                        {
                            this.estFinie = true;
                            if ( lettre !== false)
                            {
                                if (joueur.lettres[i].idLettre === mot[j] && aLaisser === undefined)
                                {
                                    joueur.lettres.splice(i , 1);
                                    compteur ++;
                                    break;
                                }
                            }
                            else if (joueur.lettres[i].idLettre === "*" && aLaisser === undefined)
                            {
                                joueur.lettres.splice(i , 1);
                                compteur ++;
                                break;
                            }
                        }
                        else if ( lettre !== false)
                        {
                            if (joueur.lettres[i].idLettre === mot[j] && aLaisser === undefined)
                            {
                                joueur.lettres.splice(i , 1);
                                let pige : number = Math.floor(Math.random() * this.reserve.length);
                                joueur.lettres.push(this.reserve[pige]);
                                this.reserve.splice(pige , 1);
                                compteur ++;
                                break;
                            }
                        }
                        else if (joueur.lettres[i].idLettre === "*" && aLaisser === undefined)
                        {
                            joueur.lettres.splice(i , 1);
                            let pige : number = Math.floor(Math.random() * this.reserve.length);
                            joueur.lettres.push(this.reserve[pige]);
                            this.reserve.splice(pige , 1);
                            compteur ++;
                            break;
                        }
                    }
                }
                joueur.modifierScore(score);
                if (compteur === CENTRE)
                {
                    this.trouverJoueur(nomJoueur).modifierScore(BINGO);
                }
            }
        });
        if (this.estFinie)
        {
            this.finirPartie();
        }
    }

    public obtenirActif() : Joueur
    {
        return this.joueurs[this.gestionnaireTour.obtenirJoueurActif()];
    }

    private finirPartie(): void
    {
        let score = 0;
        this.joueurs.forEach(joueur =>
        {
            for ( let i = 0; i < joueur.lettres.length ; i++)
            {
                score += joueur.lettres[i].valeur;
            }
        });
        this.obtenirActif().modifierScore(score);
    }

    public trouverGagnant(): Joueur
    {
        let gagnant : Joueur;
        let max = 0;
        this.joueurs.forEach(joueur =>
        {
            if ( joueur.obtenirScore() > max)
            {
                max = joueur.obtenirScore();
                gagnant = joueur ;
            }
        });
        return gagnant;
    }

    public prochainTour(): void
    {
        if (this.gestionnaireTour.obtenirJoueurActif() !== undefined)
        {
            this.joueurs[this.gestionnaireTour.obtenirJoueurActif()].rendreInactif();
        }
        this.joueurs[ this.gestionnaireTour.prochainJoueur()].rendreActif();
    }

    public placerPremierMot(mot: string[], position: number[], nomJoueur: string): void
    {
        let indexALaisser = this.verifierGrille(mot, position);
        if (this.gestionnaireDeValidation.verifierMotFrancais(mot[0]))
        {
            if (mot[1] === "H")
            {
                this.gestionnaireDeValidation.ajouterMotHorizontalValide(mot[0], position);
            }
            else if (mot[1] === "V")
            {
                this.gestionnaireDeValidation.ajouterMotVerticalValide(mot[0], position);
            }
            this.premierMotPlace = true;
            let score = this.gestionnaireDeValidation.calculerScoreMot(position, mot);
            this.actualiserInfoJoueur(mot[0], nomJoueur, score, indexALaisser);
            return;
        }
        this.premierMotPlace = false;
    }
}

