import { GestionnaireDeValidation } from './GestionnaireDeValidation';
import { expect } from 'chai';

const MAX_SCRABBLE = 15;

describe("Tests GestionnaireDeValidation", function()
{
    let validation: GestionnaireDeValidation;

    beforeEach(() => {
         validation = new GestionnaireDeValidation();
    });

    it ("Test de construction de la grille de jeu", done =>
    {
        let casesMotDouble = 0;
        let casesMotTriple = 0;
        let casesLettreDouble = 0;
        let casesLettreTriple = 0;

        expect(validation).exist;
        for (let i = 0; i < MAX_SCRABBLE; i++)
        {
            for (let j = 0; j < MAX_SCRABBLE; j++)
            {
                if (validation.obtenirGrille()[i][j].bonusMot === 3)
                {
                    casesMotTriple++;
                    expect(validation.obtenirGrille()[i][j].bonusLettre).to.equal(0);
                }
                else if (validation.obtenirGrille()[i][j].bonusMot === 2)
                {
                    casesMotDouble++;
                    expect(validation.obtenirGrille()[i][j].bonusLettre).to.equal(0);
                }
                else if (validation.obtenirGrille()[i][j].bonusLettre === 3)
                {
                    casesLettreTriple++;
                    expect(validation.obtenirGrille()[i][j].bonusMot).to.equal(0);
                }
                else if (validation.obtenirGrille()[i][j].bonusLettre === 2)
                {
                    casesLettreDouble++;
                    expect(validation.obtenirGrille()[i][j].bonusMot).to.equal(0);
                }
                expect(validation.obtenirGrille()[i][j].estUtilisee).to.equal(false);
            }
        }
        expect(casesMotDouble).to.equal(16);
        expect(casesMotTriple).to.equal(8);
        expect(casesLettreDouble).to.equal(24);
        expect(casesLettreTriple).to.equal(12);
        done();
    });

   it("Test validation mot francais", done =>
   {
        expect(validation.verifierMotFrancais("ZYTHUMS")).to.equal(true);
        expect(validation.verifierMotFrancais("ABAISSASSE")).to.equal(true);
        expect(validation.verifierMotFrancais("GAMBILLES")).to.equal(true);
        expect(validation.verifierMotFrancais("POLYMTL")).to.equal(false);
        expect(validation.verifierMotFrancais("TAS")).to.equal(true);
        expect(validation.verifierMotFrancais("L")).to.equal(false);
        expect(validation.verifierMotFrancais("SEAU")).to.equal(true);
        done();
    });

    it("Test ajout d'un mot vertical sur la grille sans validation", done =>
    {
        let position = [8, 7];
        validation.ajouterMotVerticalValide("DEROBE", position);
        expect(validation.obtenirGrille()[8][7].lettre.idLettre).to.equal("D");
        expect(validation.obtenirGrille()[8][7].lettre.valeur).to.equal(2);
        expect(validation.obtenirGrille()[9][7].lettre.idLettre).to.equal("E");
        expect(validation.obtenirGrille()[9][7].lettre.valeur).to.equal(1);
        expect(validation.obtenirGrille()[10][7].lettre.idLettre).to.equal("R");
        expect(validation.obtenirGrille()[10][7].lettre.valeur).to.equal(1);
        expect(validation.obtenirGrille()[11][7].lettre.idLettre).to.equal("O");
        expect(validation.obtenirGrille()[11][7].lettre.valeur).to.equal(1);
        expect(validation.obtenirGrille()[12][7].lettre.idLettre).to.equal("B");
        expect(validation.obtenirGrille()[12][7].lettre.valeur).to.equal(3);
        expect(validation.obtenirGrille()[13][7].lettre.idLettre).to.equal("E");
        expect(validation.obtenirGrille()[13][7].lettre.valeur).to.equal(1);
        done();
    });

    it("Test ajout d'un mot horizontal sur la grille sans validation", done =>
    {
        let position = [11, 7];
        validation.ajouterMotHorizontalValide("OSAIT", position);
        expect(validation.obtenirGrille()[11][7].lettre.idLettre).to.equal("O");
        expect(validation.obtenirGrille()[11][8].lettre.idLettre).to.equal("S");
        expect(validation.obtenirGrille()[11][9].lettre.idLettre).to.equal("A");
        expect(validation.obtenirGrille()[11][10].lettre.idLettre).to.equal("I");
        expect(validation.obtenirGrille()[11][11].lettre.idLettre).to.equal("T");
        done();
    });

    it("Test calcul de score", done =>
    {
        let position = [2, 5];
        let mot = ["GAMET", "H"];
        expect(validation.calculerScoreMot(position, mot)).to.equal(10);
        position = [11, 2];
        mot = ["BEC", "H"];
        expect(validation.calculerScoreMot(position, mot)).to.equal(14);
        position = [7, 0];
        mot = ["EXEMPLE", "H"];
        expect(validation.calculerScoreMot(position, mot)).to.equal(69);
        position = [4, 7];
        mot = ["DANS", "V"];
        expect(validation.calculerScoreMot(position, mot)).to.equal(5);
        position = [1, 5];
        mot = ["BEAUCOUP", "V"];
        expect((validation.calculerScoreMot(position, mot))).to.equal(26);
        position = [0, 0];
        mot = ["EXECUTION", "V"];
        expect((validation.calculerScoreMot(position, mot))).to.equal(207);
        done();
    });

    it("Test placement legal d'un mot sur la grille", done =>
    {
        let position = [3, 8];
        validation.ajouterMotHorizontalValide("ENTRE", position);
        position = [5, 4];
        validation.ajouterMotHorizontalValide("TOUS", position);
        position = [4, 7];
        expect(validation.verifierEnvironnementMotHorizontal("A", position)).to.equal(true);
        validation.ajouterMotHorizontalValide("A", position);
        position = [3, 7];
        expect(validation.verifierEnvironnementMotHorizontal("V", position)).to.equal(true);
        position = [5, 8];
        expect(validation.verifierEnvironnementMotHorizontal("VERT", position)).to.equal(true);
        position = [3, 3];
        expect(validation.verifierEnvironnementMotHorizontal("VERT", position)).to.equal(false);
        position = [3, 4];
        expect(validation.verifierEnvironnementMotHorizontal("VERT", position)).to.equal(true);
        position = [8, 7];
        validation.ajouterMotVerticalValide("DEROBE", position);
        position = [10, 9];
        validation.ajouterMotVerticalValide("TAC", position);
        position = [11, 7];
        expect(validation.verifierEnvironnementMotHorizontal("OSAIT", position)).to.equal(true);
        validation.ajouterMotHorizontalValide("OSAIT", position);
        position = [7, 8];
        expect(validation.verifierEnvironnementMotVertical("BETISES", position)).to.equal(true);
        position = [0, 9];
        validation.ajouterMotHorizontalValide("CARTE", position);
        position = [0, 14];
        expect(validation.verifierEnvironnementMotVertical("SE", position)).to.equal(true);
        position = [1, 14];
        expect(validation.verifierEnvironnementMotVertical("SE", position)).to.equal(false);
        position = [7, 5];
        validation.ajouterMotHorizontalValide("ENTRE", position);
        position = [6, 5];
        expect(validation.verifierEnvironnementMotVertical("TOUS", position)).to.equal(false);
        done();
    });

    it("Test validation de la grille apres l'ajout d'un mot vertical", done =>
    {
        let position = [8, 7];
        validation.ajouterMotVerticalValide("DEROBE", position);
        position = [10, 9];
        validation.ajouterMotVerticalValide("TAC", position);
        position = [11, 7];
        validation.ajouterMotHorizontalValide("OSAIT", position);
        position = [7, 8];
        expect(validation.validerPlacementMotVerticalSurGrille("BETISES", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(30);
        position = [2, 6];
        validation.ajouterMotVerticalValide("ALLO", position);
        position = [5, 4];
        validation.ajouterMotHorizontalValide("ADONNERAI", position);
        position = [3, 6];
        validation.ajouterMotHorizontalValide("LUIRE", position);
        position = [1, 8];
        validation.ajouterMotHorizontalValide("EGO", position);
        position = [1, 9];
        expect(validation.validerPlacementMotVerticalSurGrille("GARE", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(6);
        position = [12, 13];
        validation.ajouterMotVerticalValide("RUE", position);
        position = [12, 12];
        validation.ajouterMotHorizontalValide("ARC", position);
        position = [11, 12];
        expect(validation.validerPlacementMotVerticalSurGrille("EAU", position)).to.equal(false);
        expect(validation.obtenirScore()).to.equal(0);
        position = [1, 2];
        validation.ajouterMotVerticalValide("DENT", position);
        position = [2, 3];
        validation.ajouterMotHorizontalValide("AU", position);
        position = [2, 1];
        expect(validation.validerPlacementMotVerticalSurGrille("S", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(4);
        position = [7, 11];
        validation.ajouterMotHorizontalValide("IL", position);
        position = [8, 12];
        expect(validation.validerPlacementMotVerticalSurGrille("OT", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(4);
        position = [7, 3];
        expect(validation.validerPlacementMotVerticalSurGrille("MOT", position)).to.equal(false);
        expect(validation.obtenirScore()).to.equal(0);
        done();
    });

    it("Test validation de la grille apres l'ajout d'un mot horizontal", done =>
    {
        let position = [6, 4];
        validation.ajouterMotHorizontalValide("DON", position);
        position = [2, 8];
        validation.ajouterMotVerticalValide("GARE", position);
        position = [4, 5];
        validation.ajouterMotHorizontalValide("LUIRE", position);
        position = [6, 7];
        expect(validation.validerPlacementMotHorizontalSurGrille("NERAI", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(17);
        position = [9, 4];
        validation.ajouterMotHorizontalValide("ENTRE", position);
        position = [11, 1];
        validation.ajouterMotHorizontalValide("ROSE", position);
        position = [10, 3];
        expect(validation.validerPlacementMotHorizontalSurGrille("A", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(2);
        validation.ajouterMotHorizontalValide("A", position);
        position = [9, 3];
        expect(validation.validerPlacementMotHorizontalSurGrille("V", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(15);
        position = [12, 3];
        expect(validation.validerPlacementMotHorizontalSurGrille("ET", position)).to.equal(true);
        expect(validation.obtenirScore()).to.equal(11);
        position = [9, 3];
        expect(validation.validerPlacementMotHorizontalSurGrille("T", position)).to.equal(false);
        expect(validation.obtenirScore()).to.equal(0);
        position = [10, 2];
        expect(validation.validerPlacementMotHorizontalSurGrille("CARTE", position)).to.equal(false);
        expect(validation.obtenirScore()).to.equal(0);
        done();
    });
});
