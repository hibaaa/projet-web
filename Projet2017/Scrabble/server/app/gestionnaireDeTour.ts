export class GestionnaireTour
{
    private ordre : Array<number>;
    private joueurActif : number;
    private index: number;

    constructor()
    {}

    public obtenirOrdre(): Array<number>
    {
        return this.ordre;
    }
    public obtenirJoueurActif(): number
    {
        return this.joueurActif;
    }

    public determinerOrdre (nbJoueurs : number) : void
    {
        this.ordre = new Array<number>(nbJoueurs);
        for (let i = 0; i < this.ordre.length; i++)
        {
            let position = Math.floor(Math.random() * nbJoueurs);
            while (this.ordre.find(element => {return element === position; }) !== undefined)
            {
                position = Math.floor(Math.random() * nbJoueurs);
            }
            this.ordre[i] = position;
        }
    }

    public prochainJoueur () : number
    {
        if (this.ordre === undefined)
        {
            return -1;
        }
        else if (this.joueurActif === undefined)
        {
            this.index = 0;
            this.joueurActif = this.ordre[0];
        }
        else
        {
            this.index = (this.index + 1) % this.ordre.length;
            this.joueurActif = this.ordre[this.index];
        }
        return this.joueurActif;
    }
}
