
import * as mongoose from 'mongoose';
import Utilisateur from './utilisateur';
import { IUtilisateur } from './utilisateur';
import Reserve from './reserve';
import { Ireserve } from './reserve';

export class MongoService
{
    private static instance : MongoService;

    public static obtenirInstance(): MongoService
    {
        if (!MongoService.instance) {
            MongoService.instance = new MongoService();
        }
        return MongoService.instance;
    }

    public connecter(): void
    {
        mongoose.connect("mongodb://equipe18:nouvelleequipe@ds143330.mlab.com:43330/db_inf2990", () => {
        console.log('connected to Mongo...');
        });
    }

    public copierReserve() : Promise<Ireserve[]>
    {
        return Reserve.find().exec();
    }


    public ajouterUtilisateur(nomUtilisateur : string) : Promise<boolean>
    {
        return this.trouverUtilisateur(nomUtilisateur)
        .then(reponse =>
        {
            let estAjoute = false;
            if (reponse === null)
            {
                let nouvelUtilisateur = new Utilisateur({nom : nomUtilisateur });
                nouvelUtilisateur.save();
                estAjoute = true;
            }
            else if ( reponse.nom === nomUtilisateur)
            {
                reponse = null;
            }
            return estAjoute;
        });
    }

    private trouverUtilisateur(nomUtilisateur : string) : Promise<IUtilisateur>
    {
        return Utilisateur.findOne({ 'nom': nomUtilisateur }, function(erreur, utilisateurs)
        {
            if (erreur)
            {
                return console.error(erreur);
            }
        }).exec();
    }

}
