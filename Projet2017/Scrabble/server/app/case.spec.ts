import { Case } from './case';
import { expect } from 'chai';

describe("Tests cases bonus", function()
{
    let caseBonus: Case;

    beforeEach(() => {
         caseBonus = new Case();
    });

    it ("Cases Mot Triple", done =>
    {
        expect(caseBonus.verifierCaseMotTriple(0, 0)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(0, 7)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(0, 14)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(7, 0)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(7, 14)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(14, 0)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(14, 7)).to.equal(true);
        expect(caseBonus.verifierCaseMotTriple(14, 14)).to.equal(true);
        done();
     });

    it ("Cases Mot Double", done =>
    {
        expect(caseBonus.verifierCaseMotDouble(1, 1)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(2, 2)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(3, 3)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(4, 4)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(10, 4)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(11, 3)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(12, 2)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(13, 1)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(1, 13)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(2, 12)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(3, 11)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(4, 10)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(10, 10)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(11, 11)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(12, 12)).to.equal(true);
        expect(caseBonus.verifierCaseMotDouble(13, 13)).to.equal(true);
        done();
     });

    it ("Cases Lettre Triple", done =>
    {
        expect(caseBonus.verifierCaseLettreTriple(1, 5)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(1, 9)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(5, 1)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(5, 5)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(5, 9)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(5, 13)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(9, 1)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(9, 5)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(9, 9)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(9, 13)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(13, 5)).to.equal(true);
        expect(caseBonus.verifierCaseLettreTriple(13, 9)).to.equal(true);
        done();
     });

    it ("Cases Lettre Double", done =>
    {
        expect(caseBonus.verifierCaseLettreDouble(0, 3)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(0, 11)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(2, 6)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(2, 8)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(3, 0)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(3, 7)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(3, 14)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(6, 2)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(6, 6)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(6, 8)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(6, 12)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(7, 3)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(7, 11)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(14, 3)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(14, 11)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(8, 2)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(8, 6)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(8, 8)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(8, 12)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(11, 0)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(11, 7)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(11, 14)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(12, 6)).to.equal(true);
        expect(caseBonus.verifierCaseLettreDouble(12, 8)).to.equal(true);
        done();
     });
    });
