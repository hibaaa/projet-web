import * as mongoose from 'mongoose';
import Utilisateur from './utilisateur';
import { IUtilisateur } from './utilisateur';
import MeilleurScore from './meilleurScore';
import { IMeilleurScore } from './meilleurScore';

export class MongoService
{
    private static instance : MongoService;

    static obtenirInstance()
    {
        if (!MongoService.instance) {
            MongoService.instance = new MongoService();
        }
        return MongoService.instance;
    }

    connecter()
    {
        mongoose.connect("mongodb://equipe18:nouvelleequipe@ds143330.mlab.com:43330/db_inf2990", () => {
        console.log('connected to Mongo...');
        });
    }

    ajouterUtilisateur(nomUtilisateur : string) : Promise<IUtilisateur>
    {
        return this.trouverUtilisateur(nomUtilisateur)
        .then(reponse =>
        {
            if (reponse === null)
            {
                let nouvelUtilisateur = new Utilisateur({nom : nomUtilisateur});
                nouvelUtilisateur.save();
            }
            return reponse;
        });
    }

    trouverUtilisateur(nomUtilisateur : string) : Promise<IUtilisateur>
    {
        return Utilisateur.findOne({ 'nom': nomUtilisateur }, function(erreur, utilisateurs)
        {
            if (erreur)
            {
                return console.error(erreur);
            }
        }).exec();
    }

    ajouterMeilleurScore(nomUtilisateur : string, score_ : number) : Promise<IMeilleurScore[]>
    {
        return this.trouverMeilleurScore()
        .then(reponse =>
        {
            let compteur = 0;
            reponse.forEach(element => {
                if (element.score < score_)
                {
                    compteur++;
                }
            });
            if (compteur <= 3)
            {
                reponse.forEach(element =>
                {
                    if (element.position <= compteur)
                    {
                        element.position--;
                        element.save();
                    }
                    if (element.position === -1)
                    {
                        element.remove();
                    }
                });
                let nouveauMeilleurScore = new MeilleurScore(
                    {nom : nomUtilisateur, score : score_, position : ((compteur === 3 ) ? 2 : compteur)});
                nouveauMeilleurScore.save();
            }
            return reponse;
        });
    }

    trouverMeilleurScore() : Promise<IMeilleurScore[]>
    {
        return MeilleurScore.find(function(erreur, meilleursScores)
        {
            if (erreur)
            {
                return console.error(erreur);
            }
        }).sort({position : -1}).exec();
    }
}
