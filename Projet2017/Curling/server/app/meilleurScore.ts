import * as mongoose from 'mongoose';

export interface IMeilleurScore extends mongoose.Document
{
    nom : string;
    score : number;
    position : number;
}

let meilleurScoreSchema = new mongoose.Schema(
{
    nom : String,
    score : Number,
    position : Number,
});

export default mongoose.model<IMeilleurScore>("MeilleurScore", meilleurScoreSchema);
