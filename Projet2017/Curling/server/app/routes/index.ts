import * as express from 'express';
import { MongoService } from '../mongoService';

module Route {

  export class Index {

    mongoURL = "mongodb://Argon:Noble@ds157559.mlab.com:57559/projet_2_db";

    public index(req: express.Request, res: express.Response, next: express.NextFunction)
    {
      res.send('Hello world');
    }

    public glComponent(req: express.Request, res: express.Response, next: express.NextFunction)
    {
      res.redirect('/glcomp');
    }

    public validerUtilisateur(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let utilisateur = req.body.nomUtilisateur;
        MongoService.obtenirInstance().ajouterUtilisateur(utilisateur)
        .then(reponse =>
        {
            if (reponse !== null)
            {
                utilisateur = "";
            }
            res.json({nomUtilisateur : utilisateur });
        });
    }

    public verifierScore(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let estDifficile : boolean = req.params.estDifficile === "true";

        MongoService.obtenirInstance().ajouterMeilleurScore(req.body.nomUtilisateur, req.body.score);
    }

    public envoyerMeilleursScores(req: express.Request, res: express.Response, next: express.NextFunction)
    {
        let estDifficile : boolean = req.params.estDifficile === "true";

        MongoService.obtenirInstance().trouverMeilleurScore()
        .then(reponse =>
        {
            let donnes : any[] = [];
            let compteur = 0;
            reponse.forEach(element =>
            {
                donnes[compteur] = { nomUtilisateur : element.nom, score : element.score };
                compteur++;
            });
            res.json({ meilleursScores : donnes });
        });
    }
  }
}

export = Route;

