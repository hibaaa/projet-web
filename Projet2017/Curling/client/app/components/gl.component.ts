import { Component } from '@angular/core';
import { RenderService } from '../services/render.service';
import { LoginService } from '../services/login.service';
import { MdSnackBar } from '@angular/material';
import { PartieService } from '../services/partie.service';

export class MeilleurScore
{
    nomUtilisateur : string;
    score : number;
}

@Component({
    selector: 'mon-glcomp',
    templateUrl: "/assets/templates/gl.component.html"
})
export class GlComponent {
    public aideEstAfficher = false;

    constructor(private partieService : PartieService,
                private renderService : RenderService,
                private snackbar: MdSnackBar,
                private loginService: LoginService
                ){}

    public choisirPerspective() : void
    {
        this.renderService.choixPerspective();
    }

    public choisirOrthogonale() : void
    {
        this.renderService.choixOrthogonale();
    }

    public afficherAide() : void
    {
        this.aideEstAfficher = !this.aideEstAfficher;
    }

    public choisirHoraire() : void
    {
        this.renderService.choixHoraire();
    }

    public choisirAntihoraire() : void
    {
        this.renderService.choixAntihoraire();
    }
}
