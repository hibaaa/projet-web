import { LoginComponent } from './login.component';
import { LoginService } from '../services/login.service';
import { ObjetCreateurService } from '../services/objetCreateur.service';
import { expect } from 'chai';
import { Router } from '@angular/router';

describe('test LoginComponent', function () {
  let login : LoginComponent;
  let loginService : LoginService;
  let objetCreateurService : ObjetCreateurService;
  let router: Router;

  beforeEach(() => {
    login = new LoginComponent(loginService, objetCreateurService, router);
  });

  it('test creation du component', done =>
  {
    expect(login).to.not.be.undefined;
    done();
  });

});
