
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { ObjetCreateurService } from '../services/objetCreateur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'mon-login',
  templateUrl: "/assets/templates/login.component.html"
})

export class LoginComponent implements OnInit{
  constructor(private loginService : LoginService, private objetCreateurService : ObjetCreateurService,
              private router : Router){}

  ngOnInit() : void
  {
    this.objetCreateurService.init();
  }

  public validerJoueur(nomUtilisateur : string) : void
  {
    if (this.loginService.sontChampsRemplie())
    {
        this.loginService.validerUtilisateur();
    }
    else
    {
      alert("Entrer un nom de joueur pour se connecter...");
    }
    /*if (this.loginService.obtenirEstConnecte())
    {
      this.router.navigate(['/glcomp']);
    }*/
  }

  /*public choisirDifficulte(estDifficile : boolean) : void
  {
    if (estDifficile === this.loginService.obtenirDifficulte())
    {
      return;
    }
    this.loginService.modifierDifficulte(estDifficile);
    this.loginService.obtenirMeilleursScores(estDifficile)
                     .then(reponse => {return reponse; },
                           error => console.log(error));
  }*/
}
