import { DashboardComponent } from './dashboard.component';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { expect } from 'chai';

describe('test dashBoardComponent', function () {
  let dashboardComponent : DashboardComponent;
  let login : LoginService;
  let router : Router;

  beforeEach(() => {
      dashboardComponent = new DashboardComponent(router, login);
  });

  it('test creation du component', done =>
  {
    expect(dashboardComponent).to.not.be.undefined;
    done();
  });
});
