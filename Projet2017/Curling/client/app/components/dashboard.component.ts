import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

import { Router } from '@angular/router';

@Component({
  selector: 'mon-dashboard',
  templateUrl: "/assets/templates/dashboard.component.html"
})
export class DashboardComponent implements OnInit{
    private peutJouer : boolean;

    constructor(private _router: Router, private loginService : LoginService){}

    ngOnInit() : void
    {
        this.peutJouer = false;
    }

    public peutJouerAuCurling() : boolean
    {
        this.peutJouer = this.loginService.obtenirEstConnecte();
        return this.peutJouer;
    }
}
