import { AppComponent } from './app.component';
import { expect } from 'chai';

class RouterStub {
  navigateByUrl(url: string) { return url; }
}

describe('AppComponent', function () {
  let comp: AppComponent;

  beforeEach(() => {
    comp = new AppComponent();
  });

  it('should create component', () => expect(comp).to.not.be.undefined );

});
