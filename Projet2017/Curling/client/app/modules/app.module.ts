import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from '../components/app.component';
import { GlComponent } from '../components/gl.component';
import { DashboardComponent } from '../components/dashboard.component';
import { LoginComponent } from '../components/login.component';

import { ModifierDirective } from '../directives/modifier.directive';

import { RenderService } from '../services/render.service';
import { HudService } from '../services/hud.service';
import { LoginService } from '../services/login.service';
import { DecorService } from '../services/decor.service';
import { CameraService } from '../services/camera.service';
import { PartieService } from '../services/partie.service';
import { ObjetCreateurService } from '../services/objetCreateur.service';

import { MaterialModule } from '@angular/material';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CanActivateGuard } from '../services/loginGuard.service';

@NgModule({
  imports: [ BrowserModule, FormsModule, AppRoutingModule, MaterialModule.forRoot()],
  declarations: [ AppComponent, GlComponent, DashboardComponent, ModifierDirective, LoginComponent ],
  providers: [ HudService, DecorService, CameraService, LoginService, PartieService,
               RenderService, CanActivateGuard, ObjetCreateurService ],
  bootstrap: [ AppComponent ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
