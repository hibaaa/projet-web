import { Directive, Input } from '@angular/core';
import { PartieService } from '../services/partie.service';

@Directive({
    selector: 'modifier'
})
export class ModifierDirective {
    public scale = 1;
    constructor(private _partieService : PartieService) {}

    @Input()
    public set container(value: HTMLElement) {
        if ( value )
        {
            this._partieService.init(value);
        }
    }
}
