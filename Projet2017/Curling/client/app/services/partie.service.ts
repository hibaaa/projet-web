import { Injectable } from '@angular/core';
import { RenderService } from './render.service';
import { LoginService } from './login.service';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { Observable, Subscription } from 'rxjs/Rx';
import { ObjetCreateurService } from './objetCreateur.service';

@Injectable()
export class PartieService {
    private estClicke : boolean;            //L'utilisateur a clické pour lancer la pierre
    private peutLancer : boolean;           //L'initialisation du tir est finie
    private indexPierre : number;           //Index pour nommer la pierre

    private nombrePierresUtilisateur = 8;
    private nombrePierresCPU = 8;
    private numeroManche = 1;
    private nombreLancers = 0;
    private score = 7;           //Score fictif à envoyer au tableau des meilleurs scores à la fin d'une partie
    private estFini = false;

    private serveurUrl : string;
    private souris : THREE.Vector3;
    private ligne : THREE.Line;
    private directionLancer : THREE.Vector3;
    private compteurTimer : number;
    private timer : Observable<number>;
    private sub : Subscription;
    private container : HTMLElement;
    private ligneGeometry : THREE.Geometry;
    private opacite = 1;
    private estEnTrainDeDisparaitre = false;
    private spin = 0;
    private angle : number;
    private pierres : THREE.Object3D[];
    private meshHaloPierres : THREE.Mesh[];

    constructor(private renderService : RenderService,
                private http : Http,
                private loginService : LoginService,
                private objetCreateurService : ObjetCreateurService)
    {
        this.serveurUrl = 'http://localhost:3002/';
    }

    public init(container: HTMLElement)
    {
        this.container = container;
        this.estClicke = false;
        this.peutLancer = true;
        this.indexPierre = 0;
        this.angle = 0;
        this.pierres = [];
        this.meshHaloPierres = [];
        this.renderService.init(container);
        this.directionLancer = new THREE.Vector3(0, 0, -1);
        this.souris = new THREE.Vector3();
        this.ligneGeometry = new THREE.Geometry();
        window.addEventListener('keydown', _ => this.onkeydown(event));
        this.container.addEventListener('mousedown', event => this.onDocumentMouseDown(event), false);
        this.container.addEventListener('mouseup', event => this.onDocumentMouseUp(event), false);
        this.container.addEventListener('mousemove', event => this.onDocumentMouseMove(event), false);
        this.creerLigneLancer();
        this.pierres = this.objetCreateurService.obtenirPierres();
        this.renderService.obtenirScene().add(this.pierres[this.indexPierre]);
        this.jouer();
    }

    private modifierSpin() : void
    {
        this.spin = this.renderService.obtenirSpin();
    }

    private creerHalo(position : THREE.Vector3) : void
    {
        let customMaterial = new THREE.ShaderMaterial({
            uniforms: {},
            vertexShader: document.getElementById( 'vertexShader' ).textContent,
            fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
            side: THREE.DoubleSide,
            blending: THREE.AdditiveBlending,
            transparent: true
            });
        let ballGeometry = new THREE.SphereGeometry( 60, 60, 60, Math.PI, Math.PI, 3 * Math.PI / 2 );
        let ball = new THREE.Mesh( ballGeometry, customMaterial );
        ball.position.set(position.x, position.y, position.z);
        this.meshHaloPierres.push(ball);
    }

    private onkeydown(event: any) : void
    {
        if (event.keyCode === 32) //Barre espace
        {
            if (this.numeroManche < 4)
            {
                if (this.nombreLancers < 15)
                {
                    this.preparerLancer();
                }
                else
                {
                    this.preparerManche();
                }
            }
            //Envoyer le score au tableau des meilleurs scores
            else if (this.numeroManche === 4 && !this.estFini)
            {
                this.score++;
                this.verifierMeilleurScore();
                this.estFini = true;
            }
        }
    }

    private onDocumentMouseDown(event: MouseEvent) : void
    {
        if (!this.estClicke && this.pierres[this.indexPierre].userData.estPierreUtilisateur && this.peutLancer)
        {
            this.modifierSpin();
            this.estClicke = true;
            this.commencerTimer();
        }
    }

    private onDocumentMouseUp(event : MouseEvent) : void
    {

        if (this.compteurTimer !== 0)
        {
            this.envoyerTemps();
        }
        if (this.pierres[this.indexPierre].userData.force < 15 && this.peutLancer && !this.pierreEnMouvement())
        {
            this.pierres[this.indexPierre].userData.force = 0;
            this.estClicke = false;
            this.peutLancer = true;
        }
        else
        {
            if (this.pierres[this.indexPierre].userData.estPierreUtilisateur && this.angle === 0)
            {
                this.peutLancer = false;
                let point = this.calculerPositionSouris();
                this.angle = this.calculerAngle(point.x, point.y);
                this.nombrePierresUtilisateur--;
                this.renderService.obtenirHUD().miseAJourPierres(this.nombrePierresUtilisateur, this.nombrePierresCPU);
                this.pierres[this.indexPierre].userData.estEnMouvement = true;
            }
        }
    }

    public pierreEnMouvement() : boolean
    {
        for (let i = 0; i <= this.indexPierre; i++)
        {
            if (this.pierres[i].userData.estEnMouvement)
            {
                return true;
            }
        }
        return false;
    }

    private commencerTimer() : void
    {
        this.timer = Observable.timer(0, 25);
        this.sub = this.timer.subscribe(t => this.compteurTimer = t);
    }

    private envoyerTemps() : void
    {
        this.modifierForce(this.compteurTimer);
        this.compteurTimer = 0;
        this.sub.unsubscribe();
    }

    private modifierForce(compteur : number) : void
    {
        this.pierres[this.indexPierre].userData.force = compteur;
    }

    private onDocumentMouseMove(event : MouseEvent) : void
    {
        this.souris.x = (event.clientX / (<HTMLElement>event.target).clientWidth) * 2 - 1;
        this.souris.y = 0;
        this.souris.z = (event.clientY / (<HTMLElement>event.target).clientHeight) * 2 + 1;
        this.miseAjourLigne();
    }

    private miseAjourLigne() : void
    {
        if (this.pierres[this.indexPierre].userData.estPierreUtilisateur && !this.estClicke)
        {
            let pointB = this.calculerPositionSouris();
            this.ligneGeometry.vertices[1].set(pointB.x, 1, -2000);
            let angle = this.calculerAngle(pointB.x, 4500 - this.ligneGeometry.vertices[1].z);
            if (angle < 120 && angle > 60)
            {
                this.ligneGeometry.computeLineDistances();
                this.ligneGeometry.verticesNeedUpdate = true;
            }
        }
        else
        {
            this.ligneGeometry.vertices[1].set(0, 0, 4500);
            this.ligneGeometry.computeLineDistances();
            this.ligneGeometry.verticesNeedUpdate = true;
        }
    }

    private jouer() : void
    {
        window.requestAnimationFrame(_ => this.jouer());
        this.avancer();
        this.renderService.render();
    }

    avancer(): void
    {
        let compteur = 0;
        for (let i = 0; i <= this.indexPierre; i++)
        {
            let nomPierre = "Pierre_Curling" + i.toString();
            let tmp = this.renderService.obtenirScene().getObjectByName(nomPierre);
            if (this.pierres[i] !== undefined && tmp !== undefined)
            {
                if (this.pierres[i].userData.force > 0)
                {
                    this.pierres[i].userData.estEnMouvement = true;
                    this.peutLancer = false;
                    this.renderService.positionnerCameras(this.pierres[this.indexPierre]);
                    this.miseÀJourPositionPierre(i);
                    this.verifierCollision(this.pierres[i], i);
                    if (this.estHorsJeu(this.pierres[i].position))
                    {
                        if (!this.estEnTrainDeDisparaitre)
                        {
                            this.pierres[i].userData.force = 2;
                            this.estEnTrainDeDisparaitre = true;
                        }
                        this.diminuerOpacitePierre(i);
                    }
                }
                else
                {
                    this.pierres[i].userData.estEnMouvement = false;
                    compteur++;
                }
            }
            else
            {
                compteur++;
            }
        }
        //Toutes les pierres sont immobilisées après un lancer
        if (compteur > this.indexPierre && this.angle !== 0)
        {
            if (!this.peutLancer)
            {
                this.illuminerPierres();
                this.peutLancer = true;
            }
        }
    }

    private miseÀJourPositionPierre(indexPierre : number) : void
    {
        this.pierres[indexPierre].userData.force -= 0.07;
        this.pierres[indexPierre].rotateZ(this.spin * Math.PI / 40);
        let angleSpin = this.angle + this.pierres[indexPierre].userData.spin;
        this.directionLancer.x = this.pierres[indexPierre].userData.force * Math.cos(THREE.Math.degToRad(angleSpin));
        this.directionLancer.z = this.pierres[indexPierre].userData.force * Math.sin(THREE.Math.degToRad(this.angle));
        if (this.directionLancer.z === 0)
        {
            this.directionLancer.x = 0;
            this.directionLancer.z = this.pierres[indexPierre].userData.force;
        }
        this.pierres[indexPierre].position.x += this.directionLancer.x;
        this.pierres[indexPierre].position.y += 0;
        this.pierres[indexPierre].position.z -= this.directionLancer.z;
        this.pierres[indexPierre].userData.spin += this.spin * 0.035;
    }

    private diminuerOpacitePierre(index : number) : void
    {
        this.opacite -= 0.1;
        let opacite = this.opacite;
        this.pierres[index].traverse(function (node)
        {
            if (node instanceof THREE.Mesh)
            {
                node.material = new THREE.MeshBasicMaterial(
                { color: 0x000000, transparent: true, opacity: opacite});
            }
        });
        if (opacite < 0.1 && opacite > -0.1)
        {
            this.pierres[index].userData.force = 0;
            this.pierres[index].userData.estEnMouvement = false;
            this.renderService.obtenirScene().remove(this.pierres[index]);
        }
    }

    public estHorsJeu(position : THREE.Vector3) : boolean
    {
        return (position.x < -600 || position.x > 600 || position.z < -6075);
    }

    private illuminerPierres() : void
    {
        this.enleverMeshIllumination();
        this.meshHaloPierres = [];
        for (let i = 0; i <= this.indexPierre; i++)
        {
            if (this.estDansLaMaison(this.pierres[i].position))
            {
                this.creerHalo(this.pierres[i].position);
            }
        }
        for (let i = 0; i < this.meshHaloPierres.length; i++)
        {
            this.renderService.obtenirScene().add(this.meshHaloPierres[i]);
        }
    }

    public estDansLaMaison(position : THREE.Vector3) : boolean
    {
        let centreMaison = new THREE.Vector3(0, 0, -4500);
        let distance = Math.sqrt(Math.pow(position.x - centreMaison.x, 2)
                                + Math.pow(position.y - centreMaison.y, 2)
                                + Math.pow(position.z - centreMaison.z, 2));
        let rayonMaison = 360;
        return (distance < rayonMaison);
    }

    private preparerLancer() : void
    {
        if (this.peutLancer && this.angle !== 0)
        {
            this.indexPierre++;
            if (!this.pierres[this.indexPierre].userData.estPierreUtilisateur)
            {
                this.nombrePierresCPU--;
                this.angle = this.calculerAngleCPU();
                this.calculerSpinCPU();
                this.pierres[this.indexPierre].userData.force = Math.floor(Math.random() * 3) + 36;
            }
            else
            {
                this.angle = 0;
            }
            this.nombreLancers++;
        }
        this.initialiserLancer();

        if (this.nombreLancers === 15)
        {
            this.numeroManche++;
        }
    }

    private preparerManche() : void
    {
        this.renderService.obtenirHUD().miseAJourManches(this.numeroManche);
        this.enleverPierres();
        this.reinitialiserPierres();
        this.nombrePierresUtilisateur = 8;
        this.nombrePierresCPU = 8;
        this.nombreLancers = 0;
        this.indexPierre = 0;
        this.angle = 0;
        this.initialiserLancer();
    }

    private initialiserLancer() : void
    {
        this.enleverMeshIllumination();
        this.renderService.reinitialiserSpin();
        this.renderService.reinitialiserCameraPerspective();
        this.renderService.obtenirHUD().miseAJourPierres(this.nombrePierresUtilisateur, this.nombrePierresCPU);
        this.renderService.obtenirScene().add(this.pierres[this.indexPierre]);
        this.miseAjourLigne();
        this.peutLancer = true;
        this.estClicke = false;
        this.opacite = 1;
        this.estEnTrainDeDisparaitre = false;
    }

    private calculerAngleCPU() : number
    {
        return Math.floor(Math.random() * 10) + 85;
    }

    private calculerSpinCPU() : void
    {
        if (this.angle >= 85 && this.angle <= 90)
        {
            this.spin = 1;
        }
        else
        {
            this.spin = -1;
        }
    }

    private verifierCollision(pierre : THREE.Object3D, index : number) : void
    {
        for (let i = 0; i < this.indexPierre; i++)
        {
            let nomPierre = "Pierre_Curling" + i.toString();
            let tmp = this.renderService.obtenirScene().getObjectByName(nomPierre);
            if (i === index || tmp === undefined)
            {
                continue;
            }
            //this.pierre[i] est frappée par pierre
            if (this.sontEnCollision(pierre, this.pierres[i]))
            {
                this.transfererForces(pierre, this.pierres[i]);
            }
        }
    }

    public transfererForces(pierre1 : THREE.Object3D, pierre2 : THREE.Object3D) : void
    {
        pierre2.position.z -= 30;
        pierre2.userData.force = pierre1.userData.force;
        pierre1.userData.force = 0;
        this.spin = 0;
        pierre1.userData.spin = 0;
        pierre2.userData.spin = 0;
        this.angle = 90;

    }

    private enleverMeshIllumination() : void
    {
        for (let i = 0; i <= this.meshHaloPierres.length; i++)
        {
            this.renderService.obtenirScene().remove(this.meshHaloPierres[i]);
        }
    }

    public reinitialiserPierres() : void
    {
        for (let i = 0; i <= 15; i++)
        {
            this.pierres[i].position.set(0, 0, 4500);
            this.pierres[i].userData.force = 0;
            this.pierres[i].userData.spin = 0;
            this.pierres[i].userData.estEnMouvement = false;
            this.objetCreateurService.chargerMateriel(this.pierres[i], this.pierres[i].userData.estPierreUtilisateur);
        }
    }

    public sontEnCollision(pierre1 : THREE.Object3D, pierre2 : THREE.Object3D) : boolean
    {
        return ((Math.abs(pierre1.position.z - pierre2.position.z)) <= 50
            && (Math.abs(pierre1.position.x - pierre2.position.x)) <= 50);
    }

    private enleverPierres() : void
    {
        for (let i = this.indexPierre; i >= 0; i--)
        {
            this.renderService.obtenirScene().remove(this.pierres[i]);
        }
    }

    private verifierMeilleurScore () : Promise<string>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serveurUrl + 'verifierScore/' + (this.loginService.obtenirDifficulte()),
                       { nomUtilisateur : this.loginService.obtenirNomUtilisateur(),
                         score          : this.score }, options)
               .toPromise()
               .then(reponse =>
               {
                   return this.loginService.obtenirMeilleursScores();
               })
               .catch(this.loginService.gererErreur);
    }

    private creerLigneLancer(): void
    {
        let pointA = new THREE.Vector3(0, 1, 4500);
        let pointB = new THREE.Vector3(0, 1, 900);
        this.ligneGeometry.vertices.push(pointA, pointB);
        this.ligneGeometry.computeLineDistances();
        this.ligneGeometry.verticesNeedUpdate = true;
        let lineMaterial = new THREE.LineDashedMaterial( { color: 0x00cc00, dashSize: 4, gapSize: 2 } );
        this.ligne = new THREE.Line( this.ligneGeometry, lineMaterial );
        this.renderService.obtenirScene().add(this.ligne);
    }

    public calculerAngle(x : number, y : number) : number
    {
        let angle : number;
        angle = Math.atan(Math.abs(x) / Math.abs(y));
        //Quadrant 1
        if (x > 0) {
            angle = Math.atan(Math.abs(y) / Math.abs(x));
            angle = angle * 57.2958;
            if (angle < 60)
            {
                angle = 60;
            }
        }
        //Quadrant 2
        if (x < 0) {
            angle = Math.atan(Math.abs(x) / Math.abs(y));
            angle = angle * 57.2958;
            angle += 90;
            if (angle > 120)
            {
                angle = 120;
            }
        }
        return angle;
    }

    private calculerPositionSouris() : THREE.Vector3
    {
        let vector = new THREE.Vector3(this.souris.x, this.souris.y, this.souris.z);
        vector.unproject( this.renderService.obtenirCamera() );
        let direction = vector.sub( this.renderService.obtenirCamera().position ).normalize();
        let distance = - this.renderService.obtenirCamera().position.z / direction.z;
        let position = this.renderService.obtenirCamera().position.clone().add( direction.multiplyScalar( distance ) );
        return position;
    }
}
