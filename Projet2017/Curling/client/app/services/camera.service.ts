import { Injectable } from '@angular/core';

@Injectable()
export class CameraService {
    private cameraActive: THREE.Camera;
    private cameraPerspective: THREE.PerspectiveCamera;
    private cameraPersPositionInitialeX : number;
    private cameraPersPositionInitialeY : number;
    private cameraPersPositionInitialeZ : number;
    private cameraOrthogonale: THREE.OrthographicCamera;
    private cameraOrthoPositionInitialeX : number;
    private cameraOrthoPositionInitialeY : number;
    private cameraOrthoPositionInitialeZ : number;
    constructor() {}

    public initialiser() : void
    {
        this.cameraPerspective = new THREE.PerspectiveCamera(120, window.innerWidth / window.innerHeight,
                                                             1, 20000);
        this.cameraPersPositionInitialeX = 0;
        this.cameraPersPositionInitialeY = 500;
        this.cameraPersPositionInitialeZ = 4600;
        this.cameraPerspective.rotateX(-Math.PI / 3);
        this.cameraPerspective.position.set(this.cameraPersPositionInitialeX,
                                            this.cameraPersPositionInitialeY,
                                            this.cameraPersPositionInitialeZ);

        this.cameraOrthogonale = new THREE.OrthographicCamera(-window.innerWidth * 5, window.innerWidth * 5,
                                                              window.innerHeight * 3, window.innerHeight * -3,
                                                              100, 20000 );
        this.cameraOrthoPositionInitialeX = 0;
        this.cameraOrthoPositionInitialeY = 6000;
        this.cameraOrthoPositionInitialeZ = 0;
        this.cameraOrthogonale.position.set(this.cameraOrthoPositionInitialeX,
                                            this.cameraOrthoPositionInitialeY,
                                            this.cameraOrthoPositionInitialeZ);
        this.cameraOrthogonale.up = new THREE.Vector3(0, 0, -1);
        this.cameraOrthogonale.rotateX(-Math.PI / 2);
        this.cameraOrthogonale.rotateZ(Math.PI / 2);

        this.cameraActive = new THREE.Camera();
        this.cameraActive = this.cameraPerspective;
        this.cameraActive.up = new THREE.Vector3(0, 0, -1);
    }

    public obtenirCameraActive() : THREE.Camera
    {
        return this.cameraActive;
    }

    public obtenirCameraPerspective() : THREE.PerspectiveCamera
    {
        return this.cameraPerspective;
    }

    public obtenirCameraOrthogonale() : THREE.OrthographicCamera
    {
        return this.cameraOrthogonale;
    }
}
