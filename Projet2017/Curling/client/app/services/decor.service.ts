import { Injectable } from '@angular/core';

const METRE = 300;

@Injectable()
export class DecorService {
    private scene : THREE.Scene;

    constructor(){}

    public initialiser(scene : THREE.Scene) : void
    {
        this.scene = scene;
        this.creerSkybox();

        this.creerPatinoire();

        this.scene.add(new THREE.AmbientLight(0xbbbbbb));
        let lumiereDirectionnelle = new THREE.DirectionalLight(0xeeeeee);
        lumiereDirectionnelle.position.set(1, 0, 1);
        lumiereDirectionnelle.position.normalize();
        this.scene.add(lumiereDirectionnelle);

        this.creerLumiereDirectionnelle(500, 300, -4500, 1);
        this.creerLumiereDirectionnelle(-500, 300, -4500, -1);
        this.creerLumiereDirectionnelle(500, 300, 4500, 1);
        this.creerLumiereDirectionnelle(-500, 300, 4500, -1);
    }

    private creerSkybox() : void
    {
        let imagePrefix = '../assets/images/';
        let directions = ['devant', 'derriere', 'haut', 'bas', 'droite', 'gauche'];
        let imageSuffix = '.jpg';
        let geometrieSkybox = new THREE.CubeGeometry( 20000, 20000, 20000 );
        let materiaux = [];
        for (let i = 0; i < 6; i++)
        {
            materiaux.push( new THREE.MeshBasicMaterial({
                map: new THREE.TextureLoader().load( imagePrefix + directions[i] + imageSuffix ),
                side: THREE.BackSide }));
        }
        let skyMaterial = new THREE.MeshFaceMaterial( materiaux );
        let skyBox = new THREE.Mesh( geometrieSkybox, skyMaterial );
        this.scene.add( skyBox );
    }

    private creerPatinoire() : void
    {
        let image = new THREE.MeshBasicMaterial({
            map: new THREE.TextureLoader().load('../assets/images/patinoire.jpg')
            });
        let mesh : THREE.Mesh;
        //Patinoire de 42*METRE x 4*METRE
        let indice = 4 * METRE;
        //Application de plusieurs textures identiques sur la patinoire
        for (let i = 0; i < 1; i++) {
            for (let j = 0; j < 10; j++)
            {
                mesh = new THREE.Mesh( new THREE.PlaneGeometry(4 * METRE, 4 * METRE), image) ;
                mesh.position.set( 0 + indice * i, 0, -5500 + indice * j);
                mesh.rotateY(-Math.PI / 2);
                mesh.rotateX(-Math.PI / 2);
                mesh.receiveShadow = true;
                this.scene.add( mesh );
            }
        }

        this.creerMaison(1);    //Maison de départ
        this.creerMaison(-1);   //Maison d'arrivée

        this.creerLigne(-3000);
        this.creerLigne(3000);
    }

    private creerLigne(z : number) : void
    {
        let longeur = 10, largeur = 600;
        let rectangle = new THREE.Shape();
        rectangle.moveTo( 0, 0 );
        rectangle.lineTo( 0, largeur );
        rectangle.lineTo( longeur, largeur );
        rectangle.lineTo( longeur, 0 );
        rectangle.lineTo( 0, 0 );

        let geometrie = new THREE.ShapeGeometry(rectangle);
        let meshLigne = new THREE.Mesh( geometrie, new THREE.MeshPhongMaterial({ color : 0x000000 })) ;
        meshLigne.position.set( -600, 1, z );
        meshLigne.rotateY(-Math.PI / 2);
        meshLigne.rotateX(-Math.PI / 2);
        meshLigne.scale.set( 2, 2, 2 );
        meshLigne.receiveShadow = true;
        this.scene.add( meshLigne );
    }

    private creerMaison(x : number) : void
    {
        this.creerCercle(x, 180, 0xff0000, 0.1);
        this.creerCercle(x, 120, 0xffffff, 0.2);
        this.creerCercle(x, 60, 0x0000ff, 0.3);
        this.creerCercle(x, 10, 0xffffff, 0.4);
    }

    private creerCercle(x : number, rayon: number, color: number, index: number) : void
    {
        let cercle = new THREE.Shape();
        cercle.moveTo( 0, rayon );
        cercle.quadraticCurveTo( rayon, rayon, rayon, 0 );
        cercle.quadraticCurveTo( rayon, -rayon, 0, -rayon );
        cercle.quadraticCurveTo( -rayon, -rayon, -rayon, 0 );
        cercle.quadraticCurveTo( -rayon, rayon, 0, rayon );
        let geometrie = new THREE.ShapeGeometry(cercle);
        let meshCercle = new THREE.Mesh( geometrie, new THREE.MeshPhongMaterial({ color : color }));
        meshCercle.position.set( 0, index, x * 4500 );
        meshCercle.rotateY(-Math.PI / 2);
        meshCercle.rotateX(-Math.PI / 2);
        meshCercle.scale.set( 2, 2, 2 );
        meshCercle.receiveShadow = true;
        this.scene.add(meshCercle);
    }

    private creerLumiereDirectionnelle(x: number, y: number, z: number, signeAngle: number) : void
    {
        this.creerSpotlight(x, y, z, signeAngle);
        let spotLight = new THREE.SpotLight( 0xeeeeee);
        spotLight.target.position.set(0, 0, z);
        spotLight.target.updateMatrixWorld(true);
        spotLight.position.set( x, y, z );
        spotLight.angle = -Math.PI / 4;
        spotLight.penumbra = 0;
        spotLight.decay = 1;
        spotLight.distance = 600;
        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;
        (<THREE.PerspectiveCamera>spotLight.shadow.camera).near = 1;
        (<THREE.PerspectiveCamera>spotLight.shadow.camera).far = 600;
        (<THREE.PerspectiveCamera>spotLight.shadow.camera).fov = 30;
        this.scene.add(spotLight);
    }

    private creerSpotlight(x: number, y : number, z : number, signeAngle: number): void
    {
        let materials = [
            new THREE.MeshBasicMaterial({color: 0x000000}),
            new THREE.MeshBasicMaterial({color: 0xffffff}),
            new THREE.MeshBasicMaterial({color: 0x000000}),
            new THREE.MeshBasicMaterial({color: 0x000000}),
            new THREE.MeshBasicMaterial({color: 0x000000}),
            new THREE.MeshBasicMaterial({color: 0x000000})
        ];
        let geometry = new THREE.BoxBufferGeometry(30, 30, 30);
        let cube = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));
        cube.position.set(x, y, z);
        if (x > 0)
        {
            cube.rotateZ(signeAngle * Math.PI / 4);
        }
        else
        {
            cube.rotateZ(signeAngle * 5 * Math.PI / 4);
        }
        this.scene.add( cube );
    }
}
