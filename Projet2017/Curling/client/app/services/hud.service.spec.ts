import { expect } from 'chai';
import { HudService } from './hud.service';

describe("tests HudService", function()
{
    let meshTest : THREE.Mesh[];
    let hud : HudService;
    let scene = new THREE.Scene();
    let camera = new THREE.OrthographicCamera(-window.innerWidth / 2, window.innerWidth / 2,
                                                      window.innerHeight / 2, -window.innerHeight / 2, 0, 50 );

    beforeEach(() => {
        hud = new HudService();
    });

    it("test construction du HUD", done =>
    {
        expect(hud).exist;
        done();
    });

    it("test initialisation HUD", done =>
    {
        hud.initialiser(camera, scene, "John");
        expect(hud.obtenirNomUtilisateur()).equal("John");
        expect(hud.obtenirScene()).equal(scene);
        expect(hud.obtenirCamera()).equal(camera);
        done();
    });

    it("test chargement pierres", done =>
    {
        hud.initialiser(camera, scene, "John");
        meshTest = [];
        hud.chargerPierres(8, 1, meshTest);
        expect (meshTest.length).equal(8);
        done();
    });

    it("test chargement manches", done =>
    {
        hud.initialiser(camera, scene, "John");
        meshTest = [];
        hud.chargerManches(meshTest);
        expect (meshTest.length).equal(3);
        done();
    });
});
