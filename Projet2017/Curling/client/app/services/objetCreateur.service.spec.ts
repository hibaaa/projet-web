import { expect } from 'chai';
import { ObjetCreateurService } from './objetCreateur.service';

describe("tests ObjetCreateurService", function()
{
    let objetCreateurService : ObjetCreateurService;

    beforeEach(() => {
        objetCreateurService = new ObjetCreateurService();
    });

    it("test construction", done =>
    {
        expect(objetCreateurService).exist;
        done();
    });

    it('set correctement les nouveaux attributs d une pierre', () => {
        let obj = new THREE.Object3D;
        objetCreateurService.ajouterAttributs(obj, true);
        expect(obj.userData).to.have.property('force');
        expect(obj.userData).to.have.property('estPierreUtilisateur');
        expect(obj.userData.force as number).to.deep.equal(0);
        expect(obj.userData.estPierreUtilisateur as boolean).to.deep.equal(true);
    });

    it('set correctement tous les attributs d une pierre', () => {
        let obj = new THREE.Object3D;
        objetCreateurService.initAttributs(obj, 0);
        expect(obj.name).equal("Pierre_Curling0");
        let scale = new THREE.Vector3(15, 15, 15);
        expect(obj.scale.clone()).to.deep.equal(scale);
        let position = new THREE.Vector3(0, 0, 4500);
        expect(obj.position.clone()).to.deep.equal(position);
    });
});




