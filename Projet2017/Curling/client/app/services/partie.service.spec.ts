import { expect } from 'chai';
import { RenderService } from './render.service';
import { PartieService } from './partie.service';
import { LoginService } from './login.service';
import { ObjetCreateurService } from './objetCreateur.service';
import { HudService } from './hud.service';
import { DecorService } from './decor.service';
import { CameraService } from './camera.service';
import { Http } from '@angular/http';

let http : Http;
let loginService : LoginService = new LoginService(http);
let hudService : HudService = new HudService();
let cameraService : CameraService = new CameraService();
let decorService : DecorService = new DecorService();
let renderService : RenderService = new RenderService(loginService, cameraService, decorService, hudService);
let objetCreateur : ObjetCreateurService = new ObjetCreateurService();
let partieService : PartieService = new PartieService(renderService, http, loginService, objetCreateur);
let pierres : THREE.Object3D[] = [];
for (let i = 0; i <= 15; i++)
{
    pierres.push(new THREE.Object3D());
}
objetCreateur.modifierPierres(pierres);
let container = document.createElement('div');
partieService.init(container);
setTimeout(10000);

describe("tests PartieService", function()
{
    it("test construction", done =>
    {
        expect(partieService).exist;
        done();
    });

    it("devrait detecter s'il y a des pierres en mouvement", done =>
    {
        expect(partieService.pierreEnMouvement()).to.be.false;
        pierres[0].userData.estEnMouvement = true;
        expect(partieService.pierreEnMouvement()).to.be.true;
        pierres[0].userData.estEnMouvement = false;
        done();
    });

    it("devrait bien detecter les collisions", done =>
    {
        pierres[0].position.set(0, 0, 3000);
        pierres[1].position.set(0, 0, 1000);
        expect(partieService.sontEnCollision(pierres[0], pierres[1])).to.be.false;
        pierres[0].position.set(0, 0, 3000);
        pierres[1].position.set(0, 0, 2950);
        expect(partieService.sontEnCollision(pierres[0], pierres[1])).to.be.true;
        done();
    });

    it("devrait bien transferer les forces", done =>
    {
        pierres[0].userData.force = 5;
        pierres[1].userData.force = 0;
        pierres[0].userData.spin = 1;
        pierres[1].userData.spin = -1;
        partieService.transfererForces(pierres[0], pierres[1]);
        expect(pierres[0].userData.force).equal(0);
        expect(pierres[1].userData.force).equal(5);
        expect(pierres[0].userData.spin).equal(0);
        expect(pierres[1].userData.spin).equal(0);
        done();
    });

       it("devrait reconnaitre hors jeu", done =>
    {
        pierres[0].position.set(-601, 0, -6000);
        expect (partieService.estHorsJeu(pierres[0].position)).to.be.true;
        pierres[0].position.set(-500, 0, -6000);
        expect (partieService.estHorsJeu(pierres[0].position)).to.be.false;
        done();
    });

    it("devrait savoir si dans la maison", done =>
    {
        pierres[0].position.set(10, 0, -4200);
        expect (partieService.estDansLaMaison(pierres[0].position)).to.be.true;
        pierres[0].position.set(395, 0, -4000);
        expect (partieService.estDansLaMaison(pierres[0].position)).to.be.false;
        done();
    });

     it("devrait reinitialiser les pierres", done =>
    {
        partieService.reinitialiserPierres();
        expect (pierres[5].userData.force).to.equal(0);
        expect (pierres[6].userData.spin).to.equal(0);
        expect (pierres[7].userData.estEnMouvement).to.be.false;
        done();
    });

    it("devrait être dans le premier quadrant", done =>
    {
        let x = 4000;
        let z = 4000;
        let angle = partieService.calculerAngle(x, z);
        expect(angle).to.be.at.least(0).and.to.be.at.most(90);

        x = 1;
        z = 4500;
        angle = partieService.calculerAngle(x, z);
        expect(angle).to.be.at.least(0).and.to.be.at.most(90);
        done();
    });

    it("devrait être dans le deuxieme quadrant", done =>
    {
        let x = -4000;
        let z = 4000;
        let angle = partieService.calculerAngle(x, z);
        expect(angle).to.be.at.least(90).and.to.be.at.most(180);

        x = -1;
        z = 4000;
        angle = partieService.calculerAngle(x, z);
        expect(angle).to.be.at.least(90).and.to.be.at.most(180);
        done();
    });

    it("devrait faire plafonner a 60 degres", done =>
    {
        let x = 6000;
        let z = 4000;
        let angle = partieService.calculerAngle(x, z);
        expect(angle).to.equal(60);
        done();
    });

    it("devrait faire plafonner a 120 degres", done =>
    {
        let x = -6000;
        let z = 4000;
        let angle = partieService.calculerAngle(x, z);
        expect(angle).to.equal(120);
        done();
    });

    it("devrait reconnaitre hors jeu", done =>
    {
        pierres[0].position.set(-601, 0, -6000);
        expect (partieService.estHorsJeu(pierres[0].position)).to.be.true;
        pierres[0].position.set(-500, 0, -6000);
        expect (partieService.estHorsJeu(pierres[0].position)).to.be.false;
        done();
    });
});




