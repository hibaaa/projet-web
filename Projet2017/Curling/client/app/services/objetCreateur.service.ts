import { Injectable } from '@angular/core';

@Injectable()
export class ObjetCreateurService {
    private colladaLoader : THREE.ColladaLoader;
    private pierres : THREE.Object3D[];

    constructor(){}

    public init() : void
    {
        this.pierres = [];
        this.chargerPierres();
    }

    private chargerPierres() : void
    {
        let estPierreUtilisateur = true;
        for (let i = 0; i <= 15; i++)
        {
            this.chargerPierre(estPierreUtilisateur, i);
            estPierreUtilisateur = !estPierreUtilisateur;
        }
    }

    private chargerPierre(estPierreUtilisateur : boolean, index : number) : Promise<THREE.Object3D>
    {
        this.colladaLoader = new THREE.ColladaLoader();

        return new Promise<THREE.Mesh>((resolve, erreur) => {
            this.colladaLoader.load('/assets/models/pierre_curling/Curling_Stone.dae', (objet) => {
                if (objet === undefined)
                {
                    erreur("Incapable de charger la pierre");
                }
                else
                {
                    let pierre = objet.scene as THREE.Object3D;
                    this.ajouterAttributs(pierre, estPierreUtilisateur);
                    this.initAttributs(pierre, index);
                    resolve(pierre as THREE.Mesh);
                    this.pierres.push(pierre);
                }
            });
        });
    }

    public ajouterAttributs(objet: THREE.Object3D, estPierreUtilisateur : boolean) : void
    {
        objet.userData.force = 0;
        objet.userData.estPierreUtilisateur = estPierreUtilisateur;
        objet.userData.spin = 0;
        objet.userData.estEnMouvement = false;
    }

    public initAttributs(objet: THREE.Object3D, index : number): void
    {
        objet.name = "Pierre_Curling" + index.toString();
        objet.scale.set(15, 15, 15);
        objet.position.x = 0;
        objet.position.y = 0;
        objet.position.z = 4500;
        this.chargerMateriel(objet, objet.userData.estPierreUtilisateur);
    }

    public chargerMateriel(objet : THREE.Object3D, estPierreUtilisateur : boolean) : void
    {
        objet.traverse(function (node) {
            if (node instanceof THREE.Mesh)
            {
                if (estPierreUtilisateur)
                {
                    node.material = new THREE.MeshBasicMaterial(
                    { map: new THREE.TextureLoader().load('/assets/images/texture_pierre_utilisateur.png')});
                }
                else
                {
                    node.material = new THREE.MeshBasicMaterial(
                    { map: new THREE.TextureLoader().load('/assets/images/texture_pierre_cpu.png')});
                }
            }
        });
    }

    public obtenirPierres() : THREE.Object3D[]
    {
        return this.pierres;
    }

    public modifierPierres(pierres : THREE.Object3D[]) : void
    {
        this.pierres = pierres;
    }
}

