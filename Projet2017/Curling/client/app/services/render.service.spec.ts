import { RenderService } from './render.service';
import { HudService } from './hud.service';
import { DecorService } from './decor.service';
import { CameraService } from './camera.service';
import { LoginService } from './login.service';
import { Http } from '@angular/http';
import { expect } from 'chai';

let renderService : RenderService;
let http : Http;
let loginService : LoginService = new LoginService(http);
let hudService : HudService = new HudService();
let cameraService : CameraService = new CameraService();
let decorService : DecorService = new DecorService();
renderService = new RenderService(loginService, cameraService, decorService, hudService);
let container = document.createElement('div');
renderService.init(container);


describe('tests renderService', function()
{
    setTimeout(10000);
    it("test construction", done =>
    {
        expect(renderService).exist;
        done();
    });

    it("devrait bien initialiser la scene", done =>
    {
        expect(renderService.obtenirScene()).to.not.be.undefined;
        done();
    });

    it("devrait bien initialiser les cameras", done =>
    {
        renderService.initialiserCameras();
        expect(renderService.obtenirCamera()).to.not.be.undefined.and.to.be.a('THREE.PerspectiveCamera');
        let position = new THREE.Vector3(0, 500, 4600);
        expect(renderService.obtenirCamera().position).to.deep.equal(position);
        done();
    });

    it("devrait bien modifier les cameras", done =>
    {
        renderService.choixPerspective();
        expect(renderService.obtenirCamera()).to.not.be.undefined.and.to.be.a('THREE.PerspectiveCamera');
        renderService.choixOrthogonale();
        expect(renderService.obtenirCamera()).to.not.be.undefined.and.to.be.a('THREE.OrthographicCamera');
        done();
    });

    it("devrait bien modifier le spin", done =>
    {
        expect(renderService.obtenirSpin()).to.equal(0);
        renderService.choixHoraire();
        expect(renderService.obtenirSpin()).to.equal(-1);
        renderService.choixAntihoraire();
        expect(renderService.obtenirSpin()).to.equal(1);
        done();
    });

    it("devrait bien suivre la pierre", done =>
    {
        renderService.choixPerspective();
        let pierre = new THREE.Object3D();
        pierre.position.set(0, 0, 3000);
        renderService.positionnerCameras(pierre);
        let position = new THREE.Vector3(0, 400, 3200);
        expect(renderService.obtenirCamera().position).to.deep.equal(position);
        done();
    });

    it("devrait bien reinitialiser la camera perspective", done =>
    {
        renderService.choixPerspective();
        renderService.reinitialiserCameraPerspective();
        let position = new THREE.Vector3(0, 500, 4600);
        expect(renderService.obtenirCamera().position).to.deep.equal(position);
        done();
    });
});






