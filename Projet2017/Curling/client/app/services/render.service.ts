import { Injectable } from '@angular/core';
import { HudService } from './hud.service';
import { DecorService } from './decor.service';
import { CameraService } from './camera.service';
import { LoginService } from './login.service';

@Injectable()
export class RenderService {
    private scene: THREE.Scene;
    private renderer: THREE.WebGLRenderer;

    private cameraActive: THREE.Camera;
    private cameraPerspective: THREE.PerspectiveCamera;
    private cameraOrthogonale: THREE.OrthographicCamera;

    private sceneHUD : THREE.Scene;
    private cameraHUD : THREE.OrthographicCamera;

    private estPierreUtilisateur : boolean;

    private spin : number;

    constructor(private loginService : LoginService, private cameraService : CameraService,
                private decorService : DecorService, private hudService : HudService) {}

    public init(container: HTMLElement) : void
    {
        this.estPierreUtilisateur = true;
        this.spin = 0;
        this.scene = new THREE.Scene();

        this.renderer = new THREE.WebGLRenderer({antialias: true, devicePixelRatio: window.devicePixelRatio});
        this.renderer.setSize(window.innerWidth * 0.8, window.innerHeight * 0.8);
        this.renderer.domElement.style.position = "relative";
        container.appendChild( this.renderer.domElement );
        this.renderer.autoClear = false;

        this.initialiserCameras();

        this.decorService.initialiser(this.scene);

        this.cameraHUD = new THREE.OrthographicCamera(-window.innerWidth / 2, window.innerWidth / 2,
                                                      window.innerHeight / 2, -window.innerHeight / 2, 0, 50 );
        this.cameraHUD.position.y = 300;
        this.sceneHUD = new THREE.Scene();
        this.hudService.initialiser(this.cameraHUD, this.sceneHUD, this.loginService.obtenirNomUtilisateur());

        // Insérer le canvas dans le DOM
        if (container.getElementsByTagName('canvas').length === 0)
        {
            container.appendChild(this.renderer.domElement);
        }

        // bind to window resizes
        window.addEventListener('windowResize', _ => this.onWindowResize(event));
        document.addEventListener('keydown', _ => this.onkeydown(event));
    }

    public obtenirScene() : THREE.Scene
    {
        return this.scene;
    }

    public obtenirHUD() : HudService
    {
        return this.hudService;
    }

    public obtenirCamera() : THREE.Camera
    {
        return this.cameraActive;
    }

    public initialiserCameras() : void
    {
        this.cameraService.initialiser();
        this.cameraPerspective = this.cameraService.obtenirCameraPerspective();
        this.cameraOrthogonale = this.cameraService.obtenirCameraOrthogonale();
        this.cameraActive = this.cameraService.obtenirCameraActive();
    }

    public reinitialiserCameraPerspective() : void
    {
        this.cameraPerspective.position.set(0, 500, 4600);
    }

    private onkeydown(event: any) : void
    {
        switch (event.keyCode)
        {
            case 79: //"O"
                this.cameraActive = this.cameraOrthogonale;
                break;
            case 80: //"P"
                this.cameraActive = this.cameraPerspective;
                break;
        }
    }

    public choixPerspective() : void
    {
        this.cameraActive = this.cameraPerspective;
    }

    public choixOrthogonale() : void
    {
        this.cameraActive = this.cameraOrthogonale;
    }

    public choixHoraire() : void
    {
        this.spin = -1;
    }

    public choixAntihoraire() : void
    {
        this.spin = 1;
    }

    public reinitialiserSpin() : void
    {
        this.spin = 0;
    }

    public obtenirSpin(): number
    {
        return this.spin;
    }

    private onWindowResize(event: any) : void
    {
        let SCREEN_WIDTH = window.innerWidth;
        let SCREEN_HEIGHT = window.innerHeight;

        this.renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

        this.cameraPerspective.aspect = 0.5 * SCREEN_WIDTH / SCREEN_HEIGHT;
        this.cameraPerspective.updateProjectionMatrix();

        this.cameraOrthogonale.left = -0.5 * SCREEN_WIDTH / 2;
        this.cameraOrthogonale.right = 0.5 * SCREEN_WIDTH / 2;

        this.cameraOrthogonale.top = SCREEN_HEIGHT / 2;
        this.cameraOrthogonale.bottom = -SCREEN_HEIGHT / 2;
        this.cameraOrthogonale.updateProjectionMatrix();
    }

    public render(): void
    {
        this.renderer.render(this.scene, this.cameraActive);
        this.renderer.render(this.hudService.obtenirScene(), this.cameraHUD);
    }

    public positionnerCameras(pierre : THREE.Object3D) : void
    {
        this.cameraPerspective.position.set(pierre.position.x,
                                            pierre.position.y + 400,
                                            pierre.position.z + 200);
        this.cameraPerspective.lookAt(pierre.position);
    }
}
