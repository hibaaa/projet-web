import { Injectable } from '@angular/core';

@Injectable()
export class HudService {
    private camera: THREE.OrthographicCamera;
    private scene: THREE.Scene;
    private mesh: THREE.Mesh;
    private image : THREE.MeshBasicMaterial;
    private textGeo : THREE.TextGeometry;
    private f : any;

    private fontLoader: THREE.FontLoader;
    private font: THREE.Font;

    private textMeshHud : THREE.Group;
    private meshPoints : THREE.Group;
    private textMaterialHud : THREE.MultiMaterial;

    private meshPierres : THREE.Group;
    private meshPierresUtilisateur : THREE.Mesh[];
    private meshPierresCPU : THREE.Mesh[];

    private meshManches : THREE.Group;
    private meshManchesCompletes : THREE.Mesh[];
    private meshManchesIncompletes : THREE.Mesh[];

    private nomUtilisateur = "";
    private pointsUtilisateur = 0;
    private pointsCPU = 0;
    private nombrePierresUtilisateur = 8;
    private nombrePierresCPU = 8;
    private nombrePierresMax = 8;
    private nombreManchesCompletes = 1;
    private nombreMancheMax = 3;

    constructor(){}

    public initialiser(camera : THREE.OrthographicCamera, scene : THREE.Scene, nomUtilisateur : string) : void
    {
        this.camera = camera;
        this.scene = scene;
        this.nomUtilisateur = nomUtilisateur;
        this.fontLoader = new THREE.FontLoader();
        this.meshPierresUtilisateur = [];
        this.meshPierres = new THREE.Group();
        this.meshManches = new THREE.Group();
        this.meshPoints = new THREE.Group();
        this.meshPierresCPU = [];
        this.meshManchesCompletes = [];
        this.meshManchesIncompletes = [];

        this.textMaterialHud = new THREE.MultiMaterial([
            new THREE.MeshPhongMaterial({shading: THREE.FlatShading}), // front
            new THREE.MeshPhongMaterial({shading: THREE.SmoothShading})
            ]
        );

        this.fontLoader.load('/assets/fonts/helvetiker_regular.typeface.json', r => {
            this.font = new THREE.Font(r);
            this.f = Object(r);

            this.textMeshHud = new THREE.Group();

            this.textGeo = new THREE.TextGeometry( this.nomUtilisateur, {
                font: this.f as THREE.Font,
                size: 20,
                height: 0,
                curveSegments: 4,
                bevelThickness: 0,
                bevelSize: 0,
                bevelEnabled: false
            });

            this.textGeo.computeBoundingBox();
            this.textGeo.computeVertexNormals();

            this.mesh = new THREE.Mesh( this.textGeo, this.textMaterialHud );
            this.mesh.position.x = this.camera.position.x - window.innerWidth / 2 + 25;
            this.mesh.position.y = this.camera.position.y + window.innerHeight / 2 - 50;
            this.mesh.position.z = 0;

            this.textMeshHud.add(this.mesh);

            this.textGeo = new THREE.TextGeometry( "CPU", {
                font: this.f as THREE.Font,
                size: 20,
                height: 0,
                curveSegments: 4,
                bevelThickness: 0,
                bevelSize: 0,
                bevelEnabled: false
            });
            this.textGeo.computeBoundingBox();
            this.textGeo.computeVertexNormals();

            this.mesh = new THREE.Mesh( this.textGeo, this.textMaterialHud );
            this.mesh.position.x = this.camera.position.x + window.innerWidth / 2 - 75;
            this.mesh.position.y = this.camera.position.y + window.innerHeight / 2 - 50;
            this.mesh.position.z = 0;

            this.textMeshHud.add(this.mesh);


            this.miseAJourPoints(this.pointsUtilisateur, this.pointsCPU);

            this.image = new THREE.MeshBasicMaterial({
            map: new THREE.TextureLoader().load('../../assets/images/curling.png'),
            transparent : true
            });

            this.chargerPierres(this.nombrePierresMax, 1, this.meshPierresUtilisateur);
            this.chargerPierres(this.nombrePierresMax, -1, this.meshPierresCPU);

            this.image = new THREE.MeshLambertMaterial({
            map: new THREE.TextureLoader().load('../../assets/images/manche_completee.png'),
            transparent : true
            });

            this.chargerManches(this.meshManchesCompletes);

            this.image = new THREE.MeshLambertMaterial({
            map: new THREE.TextureLoader().load('../../assets/images/manche_incomplete.png'),
            transparent : true
            });

            this.chargerManches(this.meshManchesIncompletes);

            this.scene.add(this.textMeshHud);

            for (let i = 0; i < this.meshPierresUtilisateur.length; i++)
            {
                this.meshPierres.add(this.meshPierresUtilisateur[i]);
            }
            for (let i = 0; i < this.meshPierresCPU.length; i++)
            {
                this.meshPierres.add(this.meshPierresCPU[i]);
            }
            this.scene.add(this.meshPierres);

            for (let i = 0; i < this.nombreManchesCompletes; i++)
            {
                this.meshManches.add(this.meshManchesCompletes[i]);
            }
            for (let i = this.nombreManchesCompletes; i < this.nombreMancheMax; i++)
            {
                this.meshManches.add(this.meshManchesIncompletes[i]);
            }
            this.scene.add(this.meshManches);
        });
    }

    public chargerPierres(nombrePierres: number, signe: number, meshPierres: THREE.Mesh[]) : void
    {
        let indice = 50;

        for (let i = 0; i < nombrePierres; i++)
        {
            this.mesh = new THREE.Mesh(new THREE.PlaneGeometry(200, 200), this.image);
            this.mesh.position.x = this.camera.position.x - signe * window.innerWidth / 2 + signe * 50;
            this.mesh.position.y = this.camera.position.y - window.innerHeight / 2 + indice;
            this.mesh.position.z = 0;
            this.mesh.scale.x = 0.40;
            this.mesh.scale.y = 0.40;

            indice += 75;

            meshPierres.push(this.mesh);
        }
    }

    public chargerManches(meshManches: THREE.Mesh[]) : void
    {
        let indice = -50;

        for (let i = 0; i < this.nombreMancheMax; i++)
        {
            this.mesh = new THREE.Mesh(new THREE.PlaneGeometry(200, 200), this.image);
            this.mesh.position.x = this.camera.position.x + 25 + indice;
            this.mesh.position.y = this.camera.position.y + window.innerHeight / 2 - 75;
            this.mesh.position.z = 0;
            this.mesh.scale.x = 0.15;
            this.mesh.scale.y = 0.15;

            indice += 50;

            meshManches.push(this.mesh);
        }
    }

    public miseAJourPierres(nombrePierresUtilisateur : number, nombrePierresCPU : number) : void
    {
        this.scene.remove(this.meshPierres);
        this.meshPierres = new THREE.Group();
        this.nombrePierresUtilisateur = nombrePierresUtilisateur;
        this.nombrePierresCPU = nombrePierresCPU;
        for (let i = 0; i < this.nombrePierresUtilisateur; i++)
        {
            this.meshPierres.add(this.meshPierresUtilisateur[i]);
        }
        for (let i = 0; i < this.nombrePierresCPU; i++)
        {
            this.meshPierres.add(this.meshPierresCPU[i]);
        }
        this.scene.add(this.meshPierres);
    }

    public miseAJourManches(nombreManchesCompletes : number) : void
    {
        this.nombreManchesCompletes = nombreManchesCompletes;
        this.scene.remove(this.meshManches);
        this.meshManches = new THREE.Group();
        for (let i = 0; i < this.nombreManchesCompletes; i++)
        {
            this.meshManches.add(this.meshManchesCompletes[i]);
        }
        for (let i = this.nombreManchesCompletes; i < this.nombreMancheMax; i++)
        {
            this.meshManches.add(this.meshManchesIncompletes[i]);
        }
        this.scene.add(this.meshManches);
    }

    private miseAJourPoints(pointsUtilisateur : number, pointsCPU : number)
    {
        this.scene.remove(this.meshPoints);
        this.pointsUtilisateur = pointsUtilisateur;
        this.pointsCPU = pointsCPU;
        this.meshPoints = new THREE.Group();
        this.textGeo = new THREE.TextGeometry( (this.pointsUtilisateur + " - " + this.pointsCPU), {
                font: this.f as THREE.Font,
                size: 20,
                height: 0,
                curveSegments: 4,
                bevelThickness: 0,
                bevelSize: 0,
                bevelEnabled: false
            });
            this.textGeo.computeBoundingBox();
            this.textGeo.computeVertexNormals();

            this.mesh = new THREE.Mesh( this.textGeo, this.textMaterialHud );
            this.mesh.position.x = this.camera.position.x;
            this.mesh.position.y = this.camera.position.y + window.innerHeight / 2 - 50;
            this.mesh.position.z = 0;

            this.meshPoints.add(this.mesh);
            this.scene.add(this.meshPoints);
    }

    public obtenirScene() : THREE.Scene
    {
        return this.scene;
    }

    public obtenirNomUtilisateur()
    {
        return this.nomUtilisateur;
    }

    public obtenirCamera()
    {
        return this.camera;
    }
}
