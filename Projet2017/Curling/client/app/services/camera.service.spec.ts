import { CameraService } from './camera.service';
import { expect } from 'chai';

describe('tests CameraService', function()
{
    let cameraService : CameraService;

    beforeEach(() =>
    {
        cameraService = new CameraService();
    });

    it("test construction", done =>
    {
        expect(cameraService).exist;
        done();
    });

    it('devrait creer les cameras correctement', done =>
    {
        cameraService.initialiser();
        let position = new THREE.Vector3(0, 500, 4600);
        expect(cameraService.obtenirCameraPerspective()).to.not.be.undefined.and.to.be.a('THREE.PerspectiveCamera');
        expect(cameraService.obtenirCameraPerspective().position).to.deep.equal(position);
        position.set(0, 6000, 0);
        expect(cameraService.obtenirCameraOrthogonale()).to.not.be.undefined.and.to.be.a('THREE.OrthographicCamera');
        expect(cameraService.obtenirCameraOrthogonale().position).to.deep.equal(position);
        expect(cameraService.obtenirCameraActive()).to.not.be.undefined;
        done();
    });
});

