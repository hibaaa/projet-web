import { LoginService, MeilleurScore } from './login.service';
import { Http, Response, ResponseOptions } from '@angular/http';
import { expect } from 'chai';

describe("tests LoginService", function()
{
    let login : LoginService;
    let http : Http;

    beforeEach(() => {
        login = new LoginService(http);
    });

    it("test construction", done =>
    {
        expect(login).exist;
        done();
    });

    it("test modifier la difficulte", done =>
    {
        login.modifierDifficulte(true);
        expect(login.obtenirDifficulte()).equal(true);
        done();
    });

    it("test sont champs remplies", done =>
    {
        expect(login.sontChampsRemplie()).equal(false);
        done();
    });

    it("test gérer Utilisateur", done =>
    {
        let reponse = new Response(new ResponseOptions({body : { nomUtilisateur : "" }}));
        login.gererUtilisateur(reponse);
        expect(login.obtenirNomUtilisateur()).equal("");
        reponse = new Response(new ResponseOptions({body : { nomUtilisateur : "bob" }}));
        login.gererUtilisateur(reponse);
        expect(login.obtenirNomUtilisateur()).equal("bob");
        done();
    });

    it("test extraire json meilleur score", done =>
    {
        let tableau: MeilleurScore[] = [{nomUtilisateur : "McFly-kun", score : 100},
                                        {nomUtilisateur : "McFly-kun", score : 200},
                                        {nomUtilisateur : "McFly-kun", score : 300}];
        let reponse = new Response(new ResponseOptions
                        ({body : {meilleursScores : tableau }}));
        login.modifierDifficulte(false);
        login.extraireJsonMeilleursScore(reponse);
        expect(login.meilleursScores[0].nomUtilisateur).equal(tableau[0].nomUtilisateur);
        expect(login.meilleursScores[0].score).equal(tableau[0].score);
        done();
    });
});
