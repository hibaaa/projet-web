import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

export class MeilleurScore
{
    nomUtilisateur : string;
    score : number;
}

@Injectable()

export class LoginService
{
    private estDifficile : boolean;
    private nomUtilisateur : string;
    private serveurUrl : string;
    private estConnecte : boolean;
    meilleursScores : MeilleurScore[];

    constructor(private http : Http)
    {
        this.estDifficile = undefined;
        this.nomUtilisateur = "";
        this.serveurUrl = 'http://localhost:3002/';
        this.estConnecte = false;
        this.meilleursScores = undefined;
    }

    public obtenirNomUtilisateur() : string
    {
        return this.nomUtilisateur;
    }

    public obtenirEstConnecte() : boolean
    {
        return this.estConnecte;
    }

    public obtenirDifficulte() : boolean
    {
        return this.estDifficile;
    }

    public modifierDifficulte(estDifficile : boolean)
    {
        this.estDifficile = estDifficile;
    }

    public sontChampsRemplie () : Boolean
    {
        return this.nomUtilisateur !== "";
    }

    public validerUtilisateur () : Promise<string>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.serveurUrl + 'validerUtilisateur',
                              {nomUtilisateur : this.nomUtilisateur},
                              options)
                        .toPromise()
                        .then(reponse => this.gererUtilisateur(reponse))
                        .catch(this.gererErreur);
    }

    public obtenirMeilleursScores() : Promise<string>
    {
        return this.http.get(this.serveurUrl + 'meilleursScores')
               .toPromise()
               .then(reponse => this.extraireJsonMeilleursScore(reponse))
               .catch(this.gererErreur);
    }

    public gererUtilisateur (reponse : Response) : void
    {
        let donnes = reponse.json();
        this.nomUtilisateur = donnes.nomUtilisateur.toString();
        if (this.nomUtilisateur === "")
        {
            alert("login Fail");
        }
        else
        {
            this.estConnecte = true;
        }
    }

    public extraireJsonMeilleursScore (res: Response) : void
    {
        let donnes = res.json();
        this.meilleursScores = donnes.meilleursScores;
    }

    // handleError du tutorial de angular renommé en français pour les normes du projet
    public gererErreur (erreur: Response | any) : Promise<string>
    {
        console.log("ERROR");
        let errMsg: string;
        if (erreur instanceof Response)
        {
            const body = erreur.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${erreur.status} - ${erreur.statusText || ''} ${err}`;
        }
        else
        {
            errMsg = erreur.message ? erreur.message : erreur.toString();
        }
        console.log(errMsg);
        return Promise.reject(errMsg);
    }
}
